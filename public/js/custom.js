$(document).ready(function(){
    setMenuHover();

    $("#profile-picture-input").change(function(){
        readURL(this);
        $('#remove-profile-picture').removeClass('hide');
    });

    $('#remove-profile-picture').on('click', function(){
        $('#file-preview').addClass('hide');
        $('#profile-picture-input').val("");
        $('#change').addClass('hide');
        $('#select').removeClass('hide');
        $('#hasPicture').val('false');
        $(this).addClass('hide');

    });

    $('#sidebar-menu > li > a').on('click', function(){
        $(this).parent().toggleClass('sfHover');
        $(this).parent().find('.sidebar-submenu').toggleClass('hide');
    });

    $('#cms-a').on('click', function(){
        $(this).find('h4').toggleClass('active');
       $(this).find('span').toggleClass('ui-icon-triangle-1-e ui-icon-triangle-1-s');
        if($('#ftp').hasClass('in')){
            $('#ftp-h4').toggleClass('active');
            $('#ftp-span').toggleClass('ui-icon-triangle-1-e ui-icon-triangle-1-s');
        }
    });
    $('#ftp-a').on('click', function(){
        $(this).find('h4').toggleClass('active');
        $(this).find('span').toggleClass('ui-icon-triangle-1-e ui-icon-triangle-1-s');
        if($('#cms').hasClass('in')){
            $('#cms-h4').toggleClass('active');
            $('#cms-span').toggleClass('ui-icon-triangle-1-e ui-icon-triangle-1-s');
        }
    });

    $('a.toggle').on('click', function(){
        $(this).find('span').toggleClass('ui-icon-triangle-1-e ui-icon-triangle-1-s');
    });

    $('#ftp-box').on('click', function () {
        $('#ftp-host').text('ftp://');
    });
    $('#sftp-box').on('click', function () {
        $('#ftp-host').text('sftp://');
    });
    
    var indexCMS = 0;
    $('#add-cms-user').on('click', function (e) {
        e.preventDefault();
        indexCMS++;
        var $newRowCMS = $('#cms-row-user').clone();
        $newRowCMS.addClass('mrg10T clone').appendTo('.cms-col-user').find("input[type='text']").val("");
        $newRowCMS.find("input[type='text'].input-user").attr('name', 'cms_login['+indexCMS+'][user]');
        $newRowCMS.find("input[type='text'].input-password").attr('name', 'cms_login['+indexCMS+'][password]');

        $('.clone #add-cms-user').hide();
        $('.clone .remove-cms-user').removeClass('hide').on('click', function (e) {
            e.preventDefault();
            indexCMS--;
            $(this).parent().parent().remove();
        });;
    });
    $('.clone .remove-cms-user').on('click', function (e) {
        e.preventDefault();
        indexCMS--;
        $(this).parent().parent().remove();
    });

    var indexFTP = 0;
    $('#add-ftp-user').on('click', function (e) {
        e.preventDefault();
        indexFTP++;
        var $newRowFTP = $('#ftp-row-user').clone();
        $newRowFTP.addClass('mrg10T clone').appendTo('.ftp-col-user').find("input[type='text']").val("");
        $newRowFTP.find("input[type='text'].input-user").attr('name', 'ftp_login['+indexFTP+'][user]');
        $newRowFTP.find("input[type='text'].input-password").attr('name', 'ftp_login['+indexFTP+'][password]');
        $('.clone #add-ftp-user').hide();
        $('.clone .remove-ftp-user').removeClass('hide').on('click', function (e) {
            e.preventDefault();
            indexFTP--;
            $(this).parent().parent().remove();
        });;
    });
    $('.clone .remove-ftp-user').on('click', function (e) {
        e.preventDefault();
        indexFTP--;
        $(this).parent().parent().remove();
    });
    

    $('#add-company').click(function () {
        $('#col-list-company').hide();
        $('#col-add-company').hide();
        $('#col-company').show();
        $('#col-submit-company').show();
    });

    $('#cancel-company').click(function () {
        $('#col-company').hide();
        $('#col-submit-company').hide();
        $('#col-list-company').show();
        $('#col-add-company').show();
    });

    $('#btn-add-client').click(function (e) {
        var firstname = $('#client-firstname').val();
        var lastname = $('#client-lastname').val();
        var email = $('#client-email').val();
        var isValid = true;
        if(firstname == '')
        {
            isValid = false;
            $('#client-firstname').addClass('parsley-error');
        }
        else
        {
            $('#client-firstname').removeClass('parsley-error');
        }

        if(lastname == '')
        {
            isValid = false;
            $('#client-lastname').addClass('parsley-error');
        }
        else
        {
            $('#client-lastname').removeClass('parsley-error');
        }
        if( !validateEmail(email))
        {
            isValid = false;
            $('#client-email').addClass('parsley-error');
        }
        else
        {
            $('#client-email').removeClass('parsley-error');
        }


        if(isValid) addClient(firstname, lastname, email);

    });

    $('#add-submit-company').click(function () {
        var companyName = $('#company-name').val();
        if(companyName != '')
        {
            addCompany(companyName);
            $('#company-name').val('');
            $('#company-name').removeClass('parsley-error');
        }
        else {
            $('#company-name').addClass('parsley-error');
        }

    });
    
    

    imagePreview();
});

function loadPreview($box) {
    var preview = $box.find('.imgPreview');
    var reader  = new FileReader();

    reader.addEventListener("load", function () {
        preview.src = reader.result;
    }, false);

    if (link) {
        reader.readAsArrayBuffer(file);
    }
}

function readURL(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#file-preview').removeClass('hide');
            $('#profile-picture-img').attr('src', e.target.result);
            $('#change').removeClass('hide');
            $('#select').addClass('hide');
        };

        reader.readAsDataURL(input.files[0]);
    }
}

function imagePreview()
{
    $('.img-prev').each(function () {
        // get download link
        var $link = $(this).find('a.file-download').attr('href');

        // load preview
        //loadPreview($(this));
        var $preview = $(this).find('.imgPreview');
        var reader  = new FileReader();

        var blob = null;
        var xhr = new XMLHttpRequest();
        xhr.open("GET", $link);
        xhr.responseType = "blob";//force the HTTP response, response-type header to be blob
        xhr.onload = function()
        {
            blob = xhr.response;//xhr.response is now a blob object
            reader.readAsDataURL(blob);
            if(blob.type == 'application/pdf')
            {
                $preview.attr('src', '/images/pdf-icon.png');
            }else if(blob.type == 'application/msword' || blob.type.match(/.*?.document/g)){
                $preview.attr('src', '/images/doc-icon.png');
            }else if(blob.type == 'image/jpeg' || blob.type == 'image/png' || blob.type == 'image/gif'){
                $preview.attr('src', URL.createObjectURL(blob));
            }else{
                $preview.attr('src', '/images/file-icon.png');
            }

        }
        xhr.send();

    });
}


function setMenuHover(){
    var pathArray = window.location.pathname.split( '/' );
    if(pathArray[2] == undefined) pathArray[2] = '';
    $('#sidebar-menu > li').each(function () {
        $li = $(this);
       if($(this).attr('id') == window.location.pathname)
       {
           $li.addClass('sfHover hovered');
           $li.find('.sidebar-submenu').removeClass('hide');

       }else if($(this).attr('id') == pathArray[1])
       {
           $(this).find('.sidebar-submenu li').each(function () {
               if($(this).attr('id') == pathArray[2])
               {
                   $li.addClass('sfHover hovered');
                   $li.find('.sidebar-submenu').removeClass('hide');
                   $(this).addClass('hovered');
               }
           });
       }
    });

}

function validateEmail(email)
{
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function addClient(firstname, lastname, email) {
    var data = JSON.stringify({
        'firstname': firstname,
        'lastname' : lastname,
        'email' : email
    });

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "POST",
        url: "/ajax/client/new",
        data: data,
        dataType: "json",
        contentType: "application/json; charset=UTF-8"
    }).done(function (res) {
        var option = "<option value='"+res.client.id+"' selected>"+res.client.firstname+" "+res.client.lastname+"</option>";
        $('#client-select').append(option);
        $('#addClient').modal('hide');
    }).error(function (err) {
        var span = "<span class='help-block'>"+JSON.parse(err.responseText).email[0]+"</span>";
        $('#client-email').addClass('parsley-error').after(span);

    });
}