var createFolder = function(slug, name, path = '')
{
    var data = JSON.stringify({
        'path' : path,
        'name' : name
    });
    $.ajax({
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "POST",
        url: "/baboons/"+slug+"/folder/create",
        data: data,
        dataType: "json",
        contentType: "application/json; charset=UTF-8"
    })
    .done(function() {
        $('#modal-folder').modal('hide');
        $('#folder-name').val('');
        setFileStructure(slug, path);
    });
}

var deleteFolder = function(slug, path)
{
    var data = JSON.stringify({
        'path' : path,
    });
    $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: "POST",
            url: "/baboons/"+slug+"/folder/delete",
            data: data,
            dataType: "json",
            contentType: "application/json; charset=UTF-8"
        })
        .done(function() {
            $('.modal').modal('hide');
            var path = $('#dropzone-path').val();
            setFileStructure(slug, path);
        });
}

var deleteFile = function(slug, id)
{
    $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: "GET",
            url: "/baboons/"+slug+"/file/"+id+"/delete",
            contentType: "application/json; charset=UTF-8"
        })
        .done(function() {
            $('.modal').modal('hide');
            var path = $('#dropzone-path').val();
            setFileStructure(slug, path);
        });
}

var setFileStructure = function(slug, path = '')
{

    var data = JSON.stringify({
        'path' : path,
    });

    $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: "POST",
            url: "/baboons/"+slug+"/files",
            data: data,
            dataType: "json",
            contentType: "application/json; charset=UTF-8"
        })
        .done(function( data ) {
            displayFolder(slug, data);
            displayBreadCrumb(slug, path);
            setDropzonePath(path);

            $('.editable-file').editable({
                type: 'text',
                pk: 1,
                params: function(params) {
                    //originally params contain pk, name and value
                    params.id = $(this).attr('id');
                    return params;
                },
                validate: function(value) {
                    if($.trim(value) == '') return 'This field is required';
                },
                name: 'name',
                mode: 'inline',
                showbuttons: false,
                onblur: 'submit',
                url: '/file/rename',
                ajaxOptions: {
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                }
            });

            $('.editable-folder').editable({
                type: 'text',
                pk: 1,
                params: function(params) {
                    //originally params contain pk, name and value
                    params.old_path = $(this).attr('data-path');
                    return params;
                },
                validate: function(value) {
                    if($.trim(value) == '') return 'This field is required';
                },
                name: 'name',
                mode: 'inline',
                showbuttons: false,
                onblur: 'submit',
                url: '/folder/rename',
                ajaxOptions: {
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                },
                success: function(){
                    setFileStructure(slug)
                }
            });

            $('.thumbnail-box').hover(function() {
                $( this ).find('.remove-folder').removeClass('hide');
            }, function() {
                $( this ).find('.remove-folder').addClass('hide');
            });
        });
}

function setDropzonePath(path)
{
    $('#dropzone-path').val(path);
}

function displayBreadCrumb(slug, path = '')
{
    $('#breadcrumb').empty();
    var icon = "<i class='glyph-icon icon-file'></i>";
    $('#breadcrumb').append(icon);
    var tab = path.split('/');

    for(var i = 0; i < tab.length; i++)
    {
        // Last
        if(i == tab.length-1)
        {
            var title;
            tab[i] == '' ?  title = 'Files' :  title = tab[i];
            var link = title;
            $('#breadcrumb').append(link);
        }else{
            var goTo = "setFileStructure('"+slug+"','/"+tab[i]+"')";
            var title;
            tab[i] == '' ?  title = 'Files' :  title = tab[i];
            var link = "<a href='#' onclick="+goTo+">"+title+"</a>";
            $('#breadcrumb').append(link);
        }
    }
}


function displayFolder(slug, folder)
{
    $('#file-cols').empty();

    for(var i = 0; i < folder.items.length; i++)
    {
        var string = folder.items[i].path;
        var string2 = '.*'+slug+'(.*)';
        var regex = new RegExp( string2);
        var path = regex.exec(string)[1];

        if(folder.items[i].type == 'folder')
        {
            var goTo = "setFileStructure('"+slug+"','"+path+"')";
            var deleteFolder = "deleteFolder('"+slug+"','"+folder.items[i].path+"')";

            var col = "<div class='col-md-2'> " +
                            "<div class='thumbnail-box-wrapper'> " +
                                "<div class='thumbnail-box text-center'> " +
                                    "<button data-toggle='modal' data-target='#modal-folder-"+folder.items[i].name+"' class='btn btn-lg btn-round btn-danger remove-folder hide' title='Remove'><i class='glyph-icon icon-remove'></i></button> " +
                                    "<a href='#' onclick="+goTo+"><i class='glyph-icon icon-folder big'></i></a>" +
                                    "<div class='thumb-pane'> " +
                                        "<h3 class='text-transform-none text-center font-size-15'> " +
                                            "<a href='#' data-type='text' data-pk='1' data-title='"+folder.items[i].name+"' data-path='"+folder.items[i].path+"' data-placeholder='Required' class='editable editable-click editable-folder' style='display: inline-block;'>"+folder.items[i].name+"</a>" +
                                        "</h3> " +
                                    "</div> " +
                                "</div> " +
                            "</div> " +
                            "<div class='modal fade' id='modal-folder-"+folder.items[i].name+"' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true' style='display: none;'> " +
                                "<div class='modal-dialog'> " +
                                    "<div class='modal-content'> " +
                                        "<div class='modal-header'> " +
                                            "<button type='button' class='close' data-dismiss='modal' aria-hidden='true'>×</button> " +
                                            "<h4 class='modal-title'>Delete folder</h4> " +
                                        "</div> " +
                                        "<div class='modal-body'> " +
                                            "<p>Are you sure that you want to delete the folder "+folder.items[i].name+" and all his content?</p> " +
                                        "</div> " +
                                        "<div class='modal-footer'> " +
                                            "<button type='button' class='btn btn-default' data-dismiss='modal'>Close</button> " +
                                            "<button class='btn btn-danger' onclick="+deleteFolder+" onsubmit="+deleteFolder+">Delete</button> " +
                                        "</div> " +
                                    "</div> " +
                                "</div> " +
                            "</div>";
                        "</div> ";
        }else{
            var deleteFile = "deleteFile('"+slug+"','"+folder.items[i].id+"')";
            var col = "<div class='col-md-2'> " +
                            "<div class='thumbnail-box-wrapper'> " +
                                "<div class='thumbnail-box text-center img-prev'> " +
                                    "<div class='thumb-content'> " +
                                        "<div class='center-vertical'> " +
                                            "<div class='center-content'> " +
                                                "<div class='thumb-btn animated zoomIn'> " +
                                                    "<a href='"+folder.items[i].src+"' class='btn btn-lg btn-round btn-info file-download' title='Downdload'><i class='glyph-icon icon-elusive-download-alt'></i></a> " +
                                                    "<button data-toggle='modal' data-target='#modal"+folder.items[i].id+"' class='btn btn-lg btn-round btn-danger' title='Remove'><i class='glyph-icon icon-remove'></i></button> " +
                                                "</div> " +
                                            "</div> " +
                                        "</div> " +
                                    "</div> " +
                                    "<div class='thumb-overlay bg-gray'></div> " +
                                        "<img class='imgPreview' src='' alt='"+folder.items[i].name+"'> " +
                                    "</div> " +
                                    "<div class='thumb-pane'> " +
                                        "<h3 class='text-transform-none text-center font-size-15'> " +
                                            "<a href='#' id='"+folder.items[i].id+"' data-type='text' data-pk='1' data-title='"+folder.items[i].name+"' data-placeholder='Required' class='editable editable-click editable-file' style='display: inline-block;'>"+folder.items[i].name+"</a>" +
                                        "</h3> " +
                                    "</div> " +
                                "</div> " +
                            "</div> " +
                            "<div class='modal fade' id='modal"+folder.items[i].id+"' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true' style='display: none;'> " +
                                "<div class='modal-dialog'> " +
                                    "<div class='modal-content'> " +
                                        "<div class='modal-header'> " +
                                            "<button type='button' class='close' data-dismiss='modal' aria-hidden='true'>×</button> " +
                                            "<h4 class='modal-title'>Delete file</h4> " +
                                        "</div> " +
                                        "<div class='modal-body'> " +
                                            "<p>Are you sure that you want to delete the file "+folder.items[i].name+"?</p> " +
                                        "</div> " +
                                        "<div class='modal-footer'> " +
                                            "<button type='button' class='btn btn-default' data-dismiss='modal'>Close</button> " +
                                            "<button class='btn btn-danger' onclick="+deleteFile+" onsubmit="+deleteFile+">Delete</button> " +
                                        "</div> " +
                                    "</div> " +
                                "</div> " +
                            "</div>";
        }

        $('#file-cols').append(col);
        imagePreview();
    }
}
