$(document).ready(function () {
    var slider;
    var projectId = parseInt($('#project-id').val());

    calculate();
    // Create sliders
    $('.slider').each(function () {
        slider = $(this)[0];
        var inputFormat = $(this).parent().parent().find('input.rate');

        noUiSlider.create(slider, {
            start: [ inputFormat.val() ],
            step: 0.1,
            range: {
                'min': [ 0 ],
                'max': [ 50 ]
            }
        });


        slider.noUiSlider.on('update', function( values, handle ) {
            inputFormat.val(values[handle]);
            calculate();
        });

        slider.noUiSlider.on('change', function( values, handle ) {
            updateRate(projectId, parseInt(inputFormat.attr('data-user-id')), parseFloat(values[handle]));
        });

        inputFormat.on('change', function(){
            slider.noUiSlider.set($(this).val());
            calculate();
            updateRate(projectId, parseInt($(this).attr('data-user-id')), parseFloat($(this).val()));
        });
    });

});

function calculate() {
    var rate,
        time,
        cost,
        totalCost = 0,
        budget
        ;

    // Calculate user costs
    $('span.cost').each(function () {
        rate = parseFloat($(this).parents('.row-user').find('input.rate').val());
        time = parseFloat($(this).parents('.row-user').find('span.time').html());
        cost = rate * time;
        totalCost += cost;
        $(this).html(numberWithSpaces(cost.toFixed(2)));
    }).promise().done(function () {
        $('span.total-cost').html(numberWithSpaces(totalCost.toFixed(2)));

        // Calculate profit
        budget = parseFloat($('#budget').val());
        profit = budget - totalCost;
        $('span.profit').html(numberWithSpaces(profit.toFixed(2)));

        if(profit > 0)
        {
            $('td.td-profit').addClass('bg-green').removeClass('bg-red');
        }
        else
        {
            $('td.td-profit').removeClass('bg-green').addClass('bg-red');
        }

    });
}

function numberWithSpaces(x) {
    var parts = x.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, " ");
    return parts.join(".");
}

function updateRate(projectId, userId, $rate)
{
    var data = JSON.stringify({
        'projectId': projectId,
        'userId': userId,
        'rate': $rate
    });

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "POST",
        url: "/project/updateUserRate",
        data: data,
        dataType: "json",
        contentType: "application/json; charset=UTF-8"
    });
}