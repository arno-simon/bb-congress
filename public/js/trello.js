
/*
Display project lists and cards
 */
var displayProject = function (boardId) {
    $('#form-wizard-3 ul').empty();
    $('#form-wizard-3 .tab-content').empty();

    var authenticationFailure = function() { console.log('Failed authentication'); };
    var authenticationSuccess = function() {

        var error = function(errorMsg) { console.log(errorMsg); };

        var successGetProjectLists = function(lists) {
            function displayList(list, indexList, lists){
                var doneList;
                for( var j = 0; j<lists.length; j++)
                {
                    if(lists[j].name == "Done")
                    {
                        doneList = lists[j];
                    }
                }

                // Add list li

                var i = indexList+1;
                var li = "<li"; indexList == 0 ? li+=" class='active'>" : li+= ">";
                li += "<a href='#step-"+i+"' data-toggle='tab'> " +
                    "<label class='wizard-step'>"+i+"</label> " +
                    "<span class='wizard-description'>"+list.name+"</span> " +
                    "</a> " +
                    "</li>";

                $('#form-wizard-3 > ul').append(li);

                var pane = "<div class='tab-pane "; indexList == 0 ? pane+="active'" : pane+= "'";
                pane += " id='step-"+i+"'>" +
                "<div class='content-box'> " +
                "<h3 class='content-box-header bg-blue'> " +
                "<i class='glyph-icon icon-elusive-ok'></i>" +
                    list.name+
                "</h3> " +
                "<div class='content-box-wrapper'> " +
                "<ul class='todo-box'>";

                // Add cards
                function displayCard(card, indexCard, cards)
                {
                    var cardDoneFunction = "cardDone('"+card.id+"','"+doneList.id+"','"+boardId+"');return false;";
                    var cardRemoveFunction = "cardRemove('"+card.id+"','"+boardId+"');return false;";
                    pane += "<li id='li-"+card.id+"' class='border-primary'> " +
                        "<a href='"+card.url+"' target='_blank' class='float-left'>"+card.name+"</a> " +
                        "<button data-toggle='modal' data-target='#modal"+card.id+"' class='btn btn-xs btn-danger float-right mrg5L' title='Remove card'><i class='glyph-icon icon-remove'></i></button>" +
                        "<div class='modal fade' id='modal"+card.id+"' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true' style='display: none;'> " +
                            "<div class='modal-dialog'> " +
                                "<div class='modal-content'> " +
                                    "<div class='modal-header'> " +
                                        "<button type='button' class='close' data-dismiss='modal' aria-hidden='true'>×</button> " +
                                        "<h4 class='modal-title'>Delete card</h4> " +
                                    "</div> " +
                                    "<div class='modal-body'> " +
                                        "<p>Are you sure that you want to delete the card "+card.name+"?</p> " +
                                    "</div> " +
                                    "<div class='modal-footer'> " +
                                        "<button type='button' class='btn btn-default' data-dismiss='modal'>Close</button> " +
                                        "<button class='btn btn-danger' onclick="+cardRemoveFunction+">Delete</button> " +
                                    "</div> " +
                                "</div> " +
                            "</div> " +
                        "</div>";
                    if(list.name !== 'Done')
                    {
                        pane +="<button id='btn-done-"+card.id+"' class='btn btn-xs btn-success float-right' title='Done' onclick="+cardDoneFunction+"> " +
                            "<i class='glyph-icon icon-check'></i> " +
                            "</button> ";
                    }
                    pane += "</li>";
                }
                list.cards.forEach(displayCard);

                pane += "</ul></div></div>";


                $('#form-wizard-3 .tab-content').append(pane);

            }

            lists.forEach(displayList);

        };

        // Get lists
        Trello.get('/boards/'+boardId+'/lists/?cards=all', successGetProjectLists, error);
    }

    Trello.authorize({
        type: 'popup',
        name: 'BB-Congress',
        scope: {
            read: true,
            write: true },
        expiration: 'never',
        persist: true,
        success: authenticationSuccess,
        error: authenticationFailure
    });
};
/*
Set card to done
 */
function cardDone(idCard, idList, idBoard)
{
    var error = function(errorMsg) { console.log(errorMsg); };
    var successSetCardDone = function() {
        // $('#li-'+idCard).detach().prependTo($('#done-todo-box'));
        // $('#btn-done-'+idCard).remove();
        $('#refresh-button').click();
    };

    Trello.put('/cards/'+idCard+'/idList?value='+idList, successSetCardDone, error);

}

/*
Remove card
 */
function cardRemove(idCard, idBoard)
{
    var error = function(errorMsg) { console.log(errorMsg); };
    var successRemove = function() {
        // $('#li-'+idCard).remove();
        // $('.modal-backdrop').remove();
        $('#refresh-button').click();
    };

    Trello.delete('/cards/'+idCard, successRemove, error);

}

/*
 Set template list
 */
var setTemplateList = function(organization)
{
    var authenticationFailure = function() { console.log('Failed authentication'); };
    var authenticationSuccess = function() {
        var error = function(errorMsg) { console.log(errorMsg); };

        var successGetBoards = function(boards) {
            // Add boards to list
            function addToList(board, index, boards)
            {
                var option = "<option value='"+board.id+"'>"+board.name+"</option>";
                $('#select-template').append(option);
            }

            boards.forEach(addToList);
        };
        // Get boards
        Trello.get('/organizations/'+organization+'/boards', successGetBoards, error);

    }

    Trello.authorize({
        type: 'popup',
        name: 'BB-Congress',
        scope: {
            read: true,
            write: true },
        expiration: 'never',
        persist: true,
        success: authenticationSuccess,
        error: authenticationFailure
    });
}

/*
 Set template name
 */
var setTemplateName = function(boardId)
{
    var authenticationFailure = function() { console.log('Failed authentication'); };
    var authenticationSuccess = function() {
        var error = function(errorMsg) { console.log(errorMsg); };

        var successGetTemplate = function(board) {
            $('#template-name').html(board.name);
        };
        // Get boards
        Trello.get('/boards/'+boardId, successGetTemplate, error);

    }

    Trello.authorize({
        type: 'popup',
        name: 'BB-Congress',
        scope: {
            read: true,
            write: true },
        expiration: 'never',
        persist: true,
        success: authenticationSuccess,
        error: authenticationFailure
    });
}

/*
 Set company list
 */
var setCompanyList = function(company = '')
{
    var authenticationFailure = function() { console.log('Failed authentication'); };
    var authenticationSuccess = function(user) {
        var error = function(errorMsg) { console.log(errorMsg); };
        $('#select-company').find('option').remove()


        var successGetMe = function(user){
            var successGetOrganizations = function(organizations) {
                // Add companies to list
                function addToList(organization)
                {
                    if(company == organization.name)
                    {
                        var option = "<option value='"+organization.id+"' selected>"+organization.displayName+"</option>";
                    }else{
                        var option = "<option value='"+organization.id+"'>"+organization.displayName+"</option>";
                    }

                    $('#select-company').append(option);
                }

                // Add only companies that the user is admin
                for(var i=0; i < organizations.length; i++)
                {
                    for(var j=0; j < organizations[i].memberships.length; j++)
                    {
                        if(user.id == organizations[i].memberships[j].idMember && organizations[i].memberships[j].memberType == 'admin')
                        {

                            addToList(organizations[i]);
                        }
                    }
                }
                // First option
                if(company != '')
                {
                    var option = "<option>Select company</option>";

                }else{
                    var option = "<option selected>Select company</option>";
                }
                $('#select-company').prepend(option);
            };
            // Get boards
            Trello.get('/members/me/organizations', successGetOrganizations, error);
        }

        // Get me
        Trello.get('/members/me', successGetMe, error);

    }

    Trello.authorize({
        type: 'popup',
        name: 'BB-Congress',
        scope: {
            read: true,
            write: true },
        expiration: 'never',
        persist: true,
        success: authenticationSuccess,
        error: authenticationFailure
    });
}

/*
 Add company
 */
var addCompany = function(name)
{
    var authenticationFailure = function() { console.log('Failed authentication'); };
    var authenticationSuccess = function() {
        var error = function(errorMsg) { console.log(errorMsg); };

        var successAddOrganization = function(organizations) {
            setCompanyList(organizations.name);
            $('#col-company').hide();
            $('#col-submit-company').hide();
            $('#col-list-company').show();
            $('#col-add-company').show();
        };
        // Create organization
        Trello.post('/organizations?displayName='+name, successAddOrganization, error);

    }

    Trello.authorize({
        type: 'popup',
        name: 'BB-Congress',
        scope: {
            read: true,
            write: true },
        expiration: 'never',
        persist: true,
        success: authenticationSuccess,
        error: authenticationFailure
    });
}

/*
    Add member to an organization
 */
var addMemberToOrganization = function(orgId, memberId, type)
{
    var authenticationFailure = function() { console.log('Failed authentication'); };
    var authenticationSuccess = function() {
        var error = function(errorMsg) { console.log(errorMsg); };

        var successAddMember = function(successMsg) {
            console.log(successMsg);
        };
        // Add member
        Trello.put('/organizations/'+orgId+'/members/'+memberId+'?type='+type, successAddMember, error);

    }

    Trello.authorize({
        type: 'popup',
        name: 'BB-Congress',
        scope: {
            read: true,
            write: true },
        expiration: 'never',
        persist: true,
        success: authenticationSuccess,
        error: authenticationFailure
    });
}

/*
 Add member to a board
 */
var addMemberToBoard = function(boardId, memberId, type)
{
    var authenticationFailure = function() { console.log('Failed authentication'); };
    var authenticationSuccess = function() {
        var error = function(errorMsg) { console.log(errorMsg); };

        var successAddMember = function(successMsg) {
            console.log(successMsg);
        };
        // Add member
        Trello.put('/boards/'+boardId+'/members/'+memberId+'?type='+type, successAddMember, error);

    }

    Trello.authorize({
        type: 'popup',
        name: 'BB-Congress',
        scope: {
            read: true,
            write: true },
        expiration: 'never',
        persist: true,
        success: authenticationSuccess,
        error: authenticationFailure
    });
}
