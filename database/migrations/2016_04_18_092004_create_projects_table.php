<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('slug')->unique();
            $table->string('company');
            $table->string('website_url');
            $table->string('description');
            $table->integer('budget');
            $table->string('status')->default('ongoing');
            $table->string('cms_url');
            $table->string('ftp_type');
            $table->integer('ftp_port');
            $table->string('ftp_host');
            $table->string('domain_url');
            $table->string('domain_user');
            $table->string('domain_password');
            $table->string('cpanel_url');
            $table->string('invision_url');
            $table->string('sitemap_url');
            $table->string('trello_template_id');
            $table->string('trello_board_id');
            $table->string('trello_organization_id');
            $table->integer('toggle_project_id');
            $table->integer('toggle_support_project_id');
            $table->integer('toggle_client_id');
            $table->boolean('is_supported')->default(false);
            $table->string('trello_support_board_id');
            $table->integer('retainer_type_id')->unsigned()->nullable();
            $table->foreign('retainer_type_id')->references('id')->on('retainer_types');
            $table->integer('client_id')->unsigned();
            $table->foreign('client_id')->references('id')->on('clients');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('projects');
    }
}
