<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProjectAsanaIntegration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('projects', function($table){
            $table->renameColumn('trello_template_id', 'asana_template_id');
            $table->renameColumn('trello_board_id', 'asana_project_id');
            $table->renameColumn('trello_support_board_id', 'asana_support_project_id');
            $table->renameColumn('trello_organization_id', 'asana_team_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('projects', function($table){
            $table->renameColumn('asana_template_id', 'trello_template_id');
            $table->renameColumn('asana_project_id', 'trello_board_id');
            $table->renameColumn('asana_support_project_id', 'trello_support_board_id');
            $table->renameColumn('asana_team_id', 'trello_organization_id');
        });
    }
}
