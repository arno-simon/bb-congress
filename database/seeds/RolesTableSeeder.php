<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert(['title' => 'Admin', 'slug' => 'admin']);
        DB::table('roles')->insert(['title' => 'Marketing Manager', 'slug' => 'marketing-manager']);
        DB::table('roles')->insert(['title' => 'Project Manager', 'slug' => 'project-manager']);
        DB::table('roles')->insert(['title' => 'Account Manager', 'slug' => 'account-manager']);
        DB::table('roles')->insert(['title' => 'Designer', 'slug' => 'designer']);
        DB::table('roles')->insert(['title' => 'Developer', 'slug' => 'developer']);
    }
}
