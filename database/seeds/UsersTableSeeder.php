<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * Roles ids:
         * 1: Admin
         * 2: Marketing manager
         * 3: Project Manager
         * 4: Account Manager
         * 5: Designer
         * 6: Developer
         */
        // Create Baboons
        // Arno (admin)
        DB::table('users')->insert([
            'firstname' => 'Arno',
            'lastname' => 'Simon',
            'slug' => 'arno-simon',
            'picture' => '/images/users/arno-200x246.png',
            'email' => 'arno@bluebaboondigital.com',
            'password' => bcrypt('pass'),
            'trello_username' => 'arno146',
        ]);
        $user = App\User::where('firstname', 'Arno')->first();
        $user->roles()->attach(1);
        // Create storage folder
        $path = '/users/'.$user->slug.'/';
        Storage::makeDirectory($path);


        // Joe (Admin)
        DB::table('users')->insert([
            'firstname' => 'Joseph',
            'lastname' => 'Conlon',
            'slug' => 'joseph-conlon',
            'email' => 'joseph@bluebaboondigital.com',
            'password' => bcrypt('pass'),
            'trello_username' => 'josephcconlon',
        ]);
        $user = App\User::where('firstname', 'Joseph')->first();
        $user->roles()->attach(1);
        // Create storage folder
        $path = '/users/'.$user->slug.'/';
        Storage::makeDirectory($path);

        // Kevin (admin)
        DB::table('users')->insert([
            'firstname' => 'Kevin',
            'lastname' => 'Cavilla',
            'slug' => 'kevin-cavilla',
            'email' => 'kevin@bluebaboondigital.com',
            'password' => bcrypt('pass'),
            'trello_username' => 'kevincavilla1',
        ]);
        $user = App\User::where('firstname', 'Kevin')->first();
        $user->roles()->attach(1);
        // Create storage folder
        $path = '/users/'.$user->slug.'/';
        Storage::makeDirectory($path);
        
        // Joao (designer)
        DB::table('users')->insert([
            'firstname' => 'Joao',
            'lastname' => 'Neves',
            'slug' => 'joao-neves',
            'email' => 'joao@bluebaboondigital.com',
            'password' => bcrypt('pass'),
            'trello_username' => 'joaoneves15',
        ]);
        $user = App\User::where('firstname', 'Joao')->first();
        $user->roles()->attach(5);
        // Create storage folder
        $path = '/users/'.$user->slug.'/';
        Storage::makeDirectory($path);

        // Virginia (designer)
        DB::table('users')->insert([
            'firstname' => 'Virginia',
            'lastname' => 'Peláez',
            'slug' => 'virginia-pelaez',
            'email' => 'virginia@bluebaboondigital.com',
            'password' => bcrypt('pass'),
            'trello_username' => 'virginiapelaez',
        ]);
        $user = App\User::where('firstname', 'Virginia')->first();
        $user->roles()->attach(5);
        // Create storage folder
        $path = '/users/'.$user->slug.'/';
        Storage::makeDirectory($path);

        // Lea (designer)
        DB::table('users')->insert([
            'firstname' => 'Lea',
            'lastname' => 'Munzer',
            'slug' => 'lea-munzer',
            'email' => 'lea@bluebaboondigital.com',
            'password' => bcrypt('pass'),
            'trello_username' => 'leamunzer',
        ]);
        $user = App\User::where('firstname', 'Lea')->first();
        $user->roles()->attach(5);
        // Create storage folder
        $path = '/users/'.$user->slug.'/';
        Storage::makeDirectory($path);

        // Bindu (project manager)
        DB::table('users')->insert([
            'firstname' => 'Bindu',
            'lastname' => 'Budidha',
            'slug' => 'bindu-budidha',
            'email' => 'bindu@bluebaboondigital.com',
            'password' => bcrypt('pass'),
            'trello_username' => 'bindubudidha',
        ]);
        $user = App\User::where('firstname', 'Bindu')->first();
        $user->roles()->attach(3);
        // Create storage folder
        $path = '/users/'.$user->slug.'/';
        Storage::makeDirectory($path);

        // Samy (marketing manager)
        DB::table('users')->insert([
            'firstname' => 'Samy',
            'lastname' => 'Mansour',
            'slug' => 'samy-mansour',
            'email' => 'samy@bluebaboondigital.com',
            'password' => bcrypt('pass'),
            'trello_username' => 'samy64',
        ]);
        $user = App\User::where('firstname', 'Samy')->first();
        $user->roles()->attach(2);
        // Create storage folder
        $path = '/users/'.$user->slug.'/';
        Storage::makeDirectory($path);

        // Serena (Account manager, Marketing manager)
        DB::table('users')->insert([
            'firstname' => 'Serena',
            'lastname' => 'Reynell',
            'slug' => 'serena-reynell',
            'email' => 'serena@bluebaboondigital.com',
            'password' => bcrypt('pass'),
            'trello_username' => 'serenareynell',
        ]);
        $user = App\User::where('firstname', 'Serena')->first();
        $user->roles()->attach(array(2,4));
        // Create storage folder
        $path = '/users/'.$user->slug.'/';
        Storage::makeDirectory($path);

        // Gian (designer)
        DB::table('users')->insert([
            'firstname' => 'Gian',
            'lastname' => 'Coppola',
            'slug' => 'gian-coppola',
            'email' => 'gian@bluebaboondigital.com',
            'password' => bcrypt('pass'),
            'trello_username' => 'giancoppola1',
        ]);
        $user = App\User::where('firstname', 'Gian')->first();
        $user->roles()->attach(5);
        // Create storage folder
        $path = '/users/'.$user->slug.'/';
        Storage::makeDirectory($path);

    }
}
