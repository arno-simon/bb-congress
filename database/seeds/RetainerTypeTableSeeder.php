<?php

use Illuminate\Database\Seeder;

class RetainerTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('retainer_types')->insert(['name' => 'Retainer', 'slug' => 'retainer']);
        DB::table('retainer_types')->insert(['name' => 'Monthly support (2H)', 'slug' => 'monthly-support']);
        DB::table('retainer_types')->insert(['name' => 'Pay to go support', 'slug' => 'paytogo-support']);
    }
}
