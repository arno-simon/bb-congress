<?php

use Illuminate\Database\Seeder;

class TypeProjectTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('type_project')->insert(['slug' => 'strategy', 'name' => 'Strategy']);
        DB::table('type_project')->insert(['slug' => 'design', 'name' => 'Design']);
        DB::table('type_project')->insert(['slug' => 'development', 'name' => 'Development']);
        DB::table('type_project')->insert(['slug' => 'full-web-build', 'name' => 'Full Web Build']);
        DB::table('type_project')->insert(['slug' => 'marketing', 'name' => 'Marketing']);
    }
}
