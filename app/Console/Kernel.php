<?php

namespace App\Console;

use App\Console\Commands\UpdateToggleIds;
use App\Events\PeriodUpdate;
use App\Models\Notification;
use App\Models\Project;
use App\Models\RetainerPeriod;
use App\Repositories\ProjectRepository;
use App\Services\ToggleService;
use App\Services\TrelloService;
use Carbon\Carbon;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\Mail;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\Inspire::class,
        UpdateToggleIds::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // Check and update retainers and monthly support periods
        $schedule->call(function (ToggleService $toggleService) {

            $projectRepository = new ProjectRepository($toggleService);
            $projects = \App\Models\Project::where('is_supported', true)->get();

            foreach($projects as $project)
            {
                $activePeriod = $projectRepository->getActivePeriod($project->id);

                $projectRepository->updateActivePeriod($project->id);


                if(($project->retainerType->slug == 'retainer' ||  $project->retainerType->slug == 'monthly-support') && $activePeriod->to == \Carbon\Carbon::now()->toDateString())
                {
                    // New period
                    $period = new \App\Models\RetainerPeriod();
                    $period->project_id = $project->id;
                    $period->from = \Carbon\Carbon::now()->toDateString();
                    $period->to = \Carbon\Carbon::now()->addWeeks(4)->toDateString();
                    //$period->to = \Carbon\Carbon::now()->toDateString();
                    $period->time_due = $activePeriod->monthly_time + $activePeriod->time_left;
                    $period->monthly_time = $activePeriod->monthly_time;
                    $period->time_spent = 0;
                    $period->time_left = $period->time_due;
                    $period->status = 'active';
                    $period->save();


                    $activePeriod->status = 'past';
                    $activePeriod->time_left = 0;
                    $activePeriod->save();


                    \Illuminate\Support\Facades\Mail::queue('emails.retainers', array('last_period' => $activePeriod, 'new_period' => $period), function($message)
                    {
                        $message->to(['arno@bluebaboondigital.com', 'joseph@bluebaboondigital.com', 'kevin@bluebaboondigital.com'], 'BB-Congress')->subject('Project retainers updates');
                    });

                    $notification = new \App\Models\Notification();
                    $notification->content = "The current retainer period of the project " . $project->name . " has been updated";
                    // Only for admin and director
                    $notification->save();
                    $notification->roles()->attach(array(1,2));

                    event(new \App\Events\PeriodUpdate($notification));
                }
            }
        })->daily();
    }
}
