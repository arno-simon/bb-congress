<?php

namespace App\Console\Commands;

use App\Services\ToggleService;
use Illuminate\Support\Facades\DB;
use Illuminate\Console\Command;

class UpdateToggleIds extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:toggle';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get users toggles ids from the configured workspace and store them in the database to matching users (compare users emails)';

    protected $toggleService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(ToggleService $toggleService)
    {
        parent::__construct();
        $this->toggleService = $toggleService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Get toggle users
        $users = $this->toggleService->getWorkspaceUsers();

        foreach ($users as $user)
        {
            DB::table('users')
            ->where('email', $user['email'])
            ->update(['toggle_user_id' => $user['id']]);
        }

        $this->info('Users table updated successfully');
    }
}
