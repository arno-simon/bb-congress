<?php
namespace App\Services;

use App\Models\Project;
use Trello\Client;
use Trello\Manager;

class TrelloService
{
    private $api;
    private $manager;

    public function __construct()
    {
        $token = \Auth::user()->trello_token;
        $this->api = new Client();
        $this->api->authenticate(config('trello.api_key'), $token, Client::AUTH_URL_CLIENT_ID);

        $this->manager = new Manager($this->api);
    }


    /**
     * Create a trello board
     * @param $name
     * @param $companyId
     * @param null $templateId
     * @return mixed
     */
    public function createBoard($name, $companyId, $templateId = null)
    {
        if(isset($templateId))
        {
            // Create board
            $boardId = $this->api->boards()->create(['name' => $name, 'idOrganization' => $companyId, 'defaultLists' => false])['id'];
            $board =  $this->manager->getBoard($boardId);

            // Set labels
            $board->setLabelNames($this->manager->getBoard($templateId)->getLabelNames());
            $board->save();

            // Get lists from template
            $lists = $this->manager->getBoard($templateId)->getLists();

            // Create opened lists
            foreach ($lists as $list)
            {
                $data = $list->getData();
                if( ! $data['closed'] )
                {
                    $listId = $this->api->lists()->create(['name' => $data['name'], 'idBoard' => $boardId, 'pos' => $data['pos']])['id'];

                    // Create cards
                    foreach ($data['cards'] as $card)
                    {
                        if( ! $card['closed'] )
                        {
                            $cardId = $this->api->cards()->create(['name' => $card['name'], 'idList' => $listId, 'pos' => $card['pos']])['id'];
                            // Add label
                            if(isset(head($card['labels'])['color']))
                            {
                                $labelColor = head($card['labels'])['color'];
                                foreach ($this->api->boards()->labels()->all($boardId) as $label)
                                {
                                    if($label['color'] == $labelColor)
                                    {
                                        $this->api->cards()->labels()->set($cardId, [$label['color']]);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        else
        {
            $boardId = $this->api->boards()->create(['name' => $name, 'idOrganization' => $companyId, 'defaultLists' => true])['id'];
        }
        
        return $boardId;
       

    }

    /**
     * Add project's members to a trello board
     * @param Project $project
     * @param $boardId
     */
    public function setMemberships(Project $project, $boardId)
    {
        $board =  $this->manager->getBoard($boardId);
        $team = $this->manager->getOrganization($project->trello_organization_id);

        // Add members to the board and team
        foreach ($project->users() as $baboon)
        {
            \Auth::user()->trello_user_id == $baboon->trello_user_id ? $role = 'admin' : $role = 'normal';

            $this->manager->getMember($baboon->trello_user_id)->setOrganizationIds(['idMember' => $team->getData()['id']]);
            $board
                ->setMemberships([['idMembership' => $baboon->trello_username, 'type' => $role]])
                ->save();

        }

    }

    /**
     * Delete board from Trello
     * @param $boardId
     */
    public function deleteBoard($boardId)
    {
        $this->api->board()->setClosed($boardId);
    }

    /**
     * Update board name
     * @param $boardId
     * @param $name
     */
    public function updateBoardName($boardId, $name)
    {
        $this->api->board()->setName($boardId, $name);
    }

    /**
     * Get the displayed name of the company
     * @param $id: company's id
     * @return mixed
     */
    public function getCompanyName($id)
    {
        return $this->api->organization()->show($id)['displayName'];
    }

    /**
     * Close a board by default but can open it if $state = false
     * @param $id
     * @param $state
     */
    public function closeBoard($id, $state = true)
    {
        $this->api->board()->setClosed($id, $state);
    }

    /**
     * Get Trello manager
     * @return Manager
     */
    public function getManager()
    {
        return $this->manager;
    }

    /**
     * Get Trello API
     * @return Client
     */
    public function getApi()
    {
        return $this->api;
    }
    
}
