<?php
namespace App\Services;

use App\Models\Project;

class AsanaService
{

    public function createProject($name, $teamId)
    {
        // Create project
        $data = [
            'name' => $name,
            'team' => $teamId,
            'workspace' => config('asana.workspaceId')
        ];
        $project = \Asana::createProject($data);

        return $project->data->id;
    }

    public function deleteProject($projectId)
    {
        \Asana::deleteProject($projectId);
    }

    public function getTeamName($id)
    {
        $team = \Asana::getTeam($id);

        return $team->data->name;
    }

    public function getProjectName($id)
    {
        $project = \Asana::getProject($id);

        return $project->data->name;
    }

    public function updateProjectName($projectId, $name)
    {
        $data = [
            'name' => $name,
        ];

        \Asana::updateProject($projectId, $data);
    }


    public function closeProject($id)
    {
        $data = [
            'archived' => true,
        ];

        \Asana::updateProject($id, $data);
    }

    public function openProject($id)
    {
        $data = [
            'archived' => false,
        ];

        \Asana::updateProject($id, $data);
    }


}
