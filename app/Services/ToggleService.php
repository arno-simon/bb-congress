<?php
namespace App\Services;

use AJT\Toggl\TogglClient;
use AJT\Toggl\ReportsClient;
use App\Models\Project;

/**
 * Important: See available methods that you can use
 * - Client instance: /vendor/ajt/guzzle-toggl/src/AJT/Toggl/services_v8.json
 * - Report instance: /vendor/ajt/guzzle-toggl/src/AJT/Toggl/reporting_v2.json
 */
class ToggleService
{
    private $client;
    private $report;

    public function __construct()
    {
        $this->client = TogglClient::factory(array('api_key' => config('toggle.api_token')));
        $this->report = ReportsClient::factory(array('api_key' => config('toggle.api_token')));
    }

    /**
     * Create toggle project
     * @param $name
     * @param $cid
     * @return mixed: project id
     */
   public function createProject($name, $cid)
   {
       $projectData = array('project' => array('name' => $name, 'wid' => config('toggle.workspace_id'), 'cid' => $cid, 'is_private' => false));
       return $this->client->createProject($projectData)['id'];
       
   }

    /**
     * Get available clients
     * @return mixed : array of clients
     */
    public function getClients()
    {
        return $this->client->getClients();
    }

    /**
     * Get or create a toggle client that match the project's company name
     * @param Project $project
     * @return mixed
     */
    public function getOrCreateClient(Project $project)
    {
        // Get or create toggle client
        $clients = $this->getClients();
        foreach($clients as $client)
        {
            if($client['name'] == $project->company)
            {
                $clientId = $client['id'];
                break;
            }
        }
        // Create client id
        if( ! isset($clientId) )
        {
            $clientdata = array('client' => array('name' => $project->company, 'wid' => config('toggle.workspace_id')));
            $clientId = $this->client->createClient($clientdata)['id'];
        }
        
        return $clientId;
    }

    /**
     * Get time spent for a given toggle project (nb hours)
     * @param $id
     * @param $since: since date, format YYYY-MM-DD
     * @param $until: until date, format YYYY-MM-DD
     * @return string
     */
    public function getTimeSpent($id, $since, $until)
    {
        
        $reportData = array('user_agent' => 'BB-Congress', 'workspace_id' => config('toggle.workspace_id'), 'project_ids' => $id, 'since' => $since, 'until' => $until);

        // Time in milliseconds
        $timeSpent = $this->report->summary($reportData)['total_grand'];

        $nbHours = round($timeSpent / (1000 * 60 * 60), 1);

        return $nbHours;
    }

    /**
     * Create client
     * @param $name
     * @return mixed : client id
     */
    public function createClient($name)
    {
        $clientdata = array('client' => array('name' => $name, 'wid' => config('toggle.workspace_id')));
        return $this->client->createClient($clientdata)['id'];
    }

    /**
     * Update toggle project's name
     * @param $id
     * @param $name
     * @param $cid
     */
    public function updateProjectName($id, $name, $cid)
    {
        $projectData = array('id' => $id, 'project' => array('name' => $name, 'wid' => config('toggle.workspace_id'), 'cid' => $cid, 'is_private' => false));
        $this->client->updateProject($projectData);
    }

    /**
     * Delete project from Toggle
     * @param $id: toggle project's id
     */
    public function deleteProject($id)
    {
        $projectData = array('id' => $id);
        $this->client->deleteProject($projectData);
        
    }

    /**
     * Set project's state - ie active or inactive
     * @param $id
     * @param $name
     * @param $cid
     * @param $state
     */
    public function setProjectState($id, $name, $cid, $state)
    {
        $projectData = array('id' => $id, 'project' => array('name' => $name, 'active' => $state, 'wid' => config('toggle.workspace_id'), 'cid' => $cid));
        $this->client->updateProject($projectData);
    }

    /**
     * Get all workspace users
     * @return mixed
     */
    public function getWorkspaceUsers()
    {
        $option = array('id' => config('toggle.workspace_id'));
        return $this->client->getWorkspaceUsers($option);
    }

    /**
     * @param $projectId
     * @param $userId
     * @return mixed
     */
    public function getTimeSpentPerProjectPerUser($projectId, $userId)
    {
        $option = array(
            'user_agent' => 'arno@bluebaboondigital.com',
            'workspace_id' => config('toggle.workspace_id'),
            'project_ids' => $projectId,
            'user_ids' => $userId
        );

        // Time in milliseconds
        $timeSpent = $this->report->details($option)['total_grand'];

        $nbHours = round($timeSpent / (1000 * 60 * 60), 1);

        return $nbHours;
    }


}
