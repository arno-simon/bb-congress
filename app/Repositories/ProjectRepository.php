<?php namespace App\Repositories;

use App\Models\Project;
use App\Models\RetainerPeriod;
use App\Services\ToggleService;
use App\Services\TrelloService;
use DB;

class ProjectRepository{

    protected $toggleService;
    protected $trelloService;

    public function __construct(ToggleService $toggleService = null, TrelloService $trelloService = null)
    {
        $this->toggleService = $toggleService;
        $this->trelloService = $trelloService;
    }
    /**
     * Search a project
     *
     * @param $term : project to search
     * @param null $limit : number of results returned
     * @return array|static[]
     */
    public function searchProjects($term, $limit = null)
    {
        return DB::table('projects')
            ->select('id')
            ->distinct()
            ->where('name', 'LIKE',  '%'.$term.'%')
            ->take($limit)->get();
    }

    public function getActivePeriod($id)
    {
        return RetainerPeriod::where('project_id', $id)
            ->where('status', 'active')
            ->first();
    }

    public function updateActivePeriod($id)
    {
        $project = Project::findOrFail($id);
        $period = $this->getActivePeriod($id);
        if(isset($period))
        {
            $project->is_supported ? $toggleProjectId = $project->toggle_support_project_id : $toggleProjectId = $project->toggle_project_id;
            $period->time_spent = $this->toggleService->getTimeSpent($toggleProjectId, $period->from, $period->to);
            $period->time_left = $period->time_due - $period->time_spent;
            $period->save();
        }

    }

    public function updateUserRate($projectId, $userId, $rate)
    {
        DB::table('project_user')
            ->where('user_id', $userId)
            ->where('project_id', $projectId)
            ->update(['rate' => $rate]);
    }
    
}
