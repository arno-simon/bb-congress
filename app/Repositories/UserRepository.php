<?php namespace App\Repositories;

use DB;

class UserRepository{

    /**
     * Search a user
     *
     * @param $term : user to search
     * @param null $limit : number of results returned
     * @return array|static[]
     */
    public function searchUsers($term, $limit = null)
    {
        return DB::table('users')
            ->select('id')
            ->distinct()
            ->where('firstname', 'LIKE',  '%'.$term.'%')
            ->orWhere('lastname', 'LIKE',  '%'.$term.'%')
            ->take($limit)->get();
    }
    
}
