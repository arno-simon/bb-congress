<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class TypeProject extends Model
{
    protected $table = 'type_project';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    public function projects()
    {
        return $this->belongsToMany('App\Models\Project', 'project_type_project', 'project_type_id', 'project_id');
    }
}
