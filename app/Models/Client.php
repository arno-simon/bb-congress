<?php

namespace App\Models;

use App\Services\TrelloService;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Client extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'slug'
    ];


    public function projects()
    {
        return $this->hasMany('App\Models\Project', 'client_id');
    }
    
}
