<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class CMSUser extends Model
{
    protected $table = 'cms_users';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'user',
        'password',
        'project_id',
    ];

    public function project()
    {
        return $this->belongsTo('App\Models\Project');
    }
}

