<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class RetainerPeriod extends Model
{
    protected $table = 'retainer_periods';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'id',
        'from',
        'to',
        'time_due',
        'monthly_time',
        'time_spent',
        'time_left',
        'status'
    ];

    public function project()
    {
        return $this->belongsTo('App\Models\Project');
    }
}

