<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'company',
        'status',
        'website_url',
        'cpanel_url',
        'sitemap_url',
        'invision_url',
        'description',
        'cms_url',
        'ftp_type',
        'ftp_host',
        'ftp_port',
        'retainer_type_id'
    ];

    public function client()
    {
        return $this->belongsTo('App\Models\Client');
    }
    
    public function users()
    {
        return $this->belongsToMany('App\User', 'project_user', 'project_id', 'user_id');
    }


    public function contractors()
    {
        return $this->belongsToMany('App\Models\Contractor', 'project_contractor', 'project_id', 'contractor_id');
    }

    public function cmsUsers()
    {
        return $this->hasMany('App\Models\CMSUser', 'project_id');
    }
    public function ftpUsers()
    {
        return $this->hasMany('App\Models\FTPUser', 'project_id');
    }

    public function retainerPeriods()
    {
        return $this->hasMany('App\Models\RetainerPeriod', 'project_id');
    }
    
    public function retainerType()
    {
        return $this->belongsTo('App\Models\RetainerType');
    }
    
    public function files()
    {
        return $this->hasMany('App\Models\ProjectFile', 'project_id');
    }
    public function types()
    {
        return $this->belongsToMany('App\Models\TypeProject', 'project_type_project', 'project_id', 'project_type_id');
    }
    
   

}
