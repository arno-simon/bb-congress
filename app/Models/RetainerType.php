<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class RetainerType extends Model
{
    protected $table = 'retainer_types';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'id',
        'name',
        'slug',
    ];

    public function projects()
    {
        return $this->hasMany('App\Models\Project', 'retainer_type_id');
    }
}

