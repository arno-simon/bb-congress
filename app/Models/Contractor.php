<?php

namespace App\Models;

use App\Services\TrelloService;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Contractor extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'slug'
    ];
    

    public function projects()
    {
        return $this->belongsToMany('App\Models\Project', 'project_contractor', 'contractor_id', 'project_id');
    }
    
}
