<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class UserFile extends Model
{
    protected $table = 'user_files';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'path',
        'user_id'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
