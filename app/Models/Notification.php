<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'id',
        'content',
    ];

    public function roles()
    {
        return $this->belongsToMany('App\Models\Role', 'notification_role', 'notification_id', 'role_id');
    }
}

