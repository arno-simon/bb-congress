<?php

namespace App;

use App\Models\Project;
use App\Services\ToggleService;
use App\Services\TrelloService;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'slug'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function roles()
    {
        return $this->belongsToMany('App\Models\Role', 'role_user', 'user_id', 'role_id');
    }

    public function projects()
    {
        return $this->belongsToMany('App\Models\Project', 'project_user', 'user_id', 'project_id');
    }

    public function files()
    {
        return $this->hasMany('App\Models\UserFile', 'user_id');
    }

    /**
     * Get the 5 latest notifications
     */
    public function latestNotifications()
    {
        $notifications = array();
        $user = \Auth::user();

        foreach($user->roles as $role)
        {
            foreach($role->notifications as $notification)
            {
                $notification = $notification->toArray();
                if( ! array_key_exists($notification['id'], $notifications)) $notifications[$notification['id']] = $notification;
            }
        }
        // Sort notif
        usort($notifications, function($a, $b){
            $t1 = strtotime($a['created_at']);
            $t2 = strtotime($b['created_at']);
            return $t2 - $t1;
        });

        // Get last 5 notif
        $notifications = array_slice($notifications, 0, 5);

        return $notifications;
    }

    /**
     * Get the all notifications
     */
    public function notifications()
    {
        $notifications = array();
        $user = \Auth::user();

        foreach($user->roles as $role)
        {
            foreach($role->notifications as $notification)
            {
                $notification = $notification->toArray();
                if( ! array_key_exists($notification['id'], $notifications)) $notifications[$notification['id']] = $notification;
            }
        }
        // Sort notif
        usort($notifications, function($a, $b){
            $t1 = strtotime($a['created_at']);
            $t2 = strtotime($b['created_at']);
            return $t2 - $t1;
        });

        return $notifications;
    }


    public function isAdmin()
    {
        foreach($this->roles as $role)
        {
            if($role->slug == 'admin')
            {
                return true;
            }
        }
        return false;
    }
    
    public function isProjectManager()
    {
        if($this->isAdmin()) return true;
        
        foreach($this->roles as $role)
        {
            if($role->slug == 'project-manager')
            {
                return true;
            }
        }
        return false;
    }

    public function isAccountManager()
    {
        if($this->isAdmin()) return true;

        foreach($this->roles as $role)
        {
            if($role->slug == 'account-manager')
            {
                return true;
            }
        }
        return false;
    }

    public function isDesigner()
    {
        if($this->isAdmin()) return true;
        
        foreach($this->roles as $role)
        {
            if($role->slug == 'designer')
            {
                return true;
            }
        }
        return false;
    }

    public function isMarketingManager()
    {
        if($this->isAdmin()) return true;
        
        foreach($this->roles as $role)
        {
            if($role->slug == 'marketing-manager')
            {
                return true;
            }
        }
        return false;
    }
    
    public function isDeveloper()
    {
        if($this->isAdmin()) return true;
        
        foreach($this->roles as $role)
        {
            if($role->slug == 'developer')
            {
                return true;
            }
        }
        return false;
    }

    /**
     * Return user's rate on a given project
     * @param $id
     * @return mixed
     */
    public function getRateByProject($id)
    {
        return DB::table('project_user')
            ->select('rate')
            ->where('user_id', $this->id)
            ->where('project_id', $id)
            ->get();

    }

    /**
     * Return time spent by user on a given project. 
     * @param $projectId
     * @return bool|string
     */
    public function getTimeSpentPerProject($projectId)
    {
        $toggleService = new ToggleService();
        return $toggleService->getTimeSpentPerProjectPerUser($projectId, $this->toggle_user_id);
    }

    /*
     * GET Trello service
     */
    public function getApi()
    {
        $token = \Auth::user()->trello_token;
        $service = new TrelloService($token);
        return $service->getApi();
    }
}
