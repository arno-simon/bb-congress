<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('/test-period', function (\App\Repositories\ProjectRepository $projectRepository){
    $projects = \App\Models\Project::where('is_supported', true)->get();

    foreach($projects as $project)
    {
        $activePeriod = $projectRepository->getActivePeriod($project->id);

        $projectRepository->updateActivePeriod($project->id);


        if(($project->retainerType->slug == 'retainer' ||  $project->retainerType->slug == 'monthly-support') && $activePeriod->to == \Carbon\Carbon::now()->toDateString())
        {
            // New period
            $period = new \App\Models\RetainerPeriod();
            $period->project_id = $project->id;
            $period->from = \Carbon\Carbon::now()->toDateString();
            //$period->to = \Carbon\Carbon::now()->addWeeks(4)->toDateString();
            $period->to = \Carbon\Carbon::now()->toDateString();
            $period->time_due = $activePeriod->monthly_time + $activePeriod->time_left;
            $period->monthly_time = $activePeriod->monthly_time;
            $period->time_spent = 0;
            $period->time_left = $period->time_due;
            $period->status = 'active';
            $period->save();


            $activePeriod->status = 'past';
            $activePeriod->time_left = 0;
            $activePeriod->save();


            \Illuminate\Support\Facades\Mail::queue('emails.retainers', array('last_period' => $activePeriod, 'new_period' => $period), function($message)
            {
                $message->to('arno@bluebaboondigital.com', 'BB-Congress')->subject('Project retainers updates');
            });

            $notification = new \App\Models\Notification();
            $notification->content = "Test notification";
            // Only for admin and director
            $notification->save();
            $notification->roles()->attach(array(1,2));

            event(new \App\Events\PeriodUpdate($notification));
        }
    }

    return "<html><body>Ye</body></html>";
});

Route::get('/fire', function (){
    $notification = new \App\Models\Notification();
    $notification->content = "Test notification";

    event(new \App\Events\PeriodUpdate($notification));
    return "<html><body></body></html>";

});

Route::get('test', function (){

    $data = [
        'name' => 'test project',
        'team' => '186316521194991',
        'workspace' => '82767740528600'
    ];
    $project = Asana::createProject($data);

    // Test template
    $tasks = Asana::getTasksByFilter(['project' => '186343700349483']);
    $templateSections = Asana::getProjectSections('186343700349483');
    $sections = [];
    foreach($templateSections->data as $section)
    {
        array_push($sections, $section->name);
    }
    // Create sections
    foreach($tasks->data as $task) {
        $taskTemplate = Asana::getTask($task->id);
        if(in_array($taskTemplate->data->name, $sections))
        {
            $data = [
                'projects' => [$project->data->id],
                'name' => $taskTemplate->data->name,
            ];
            Asana::createTask($data);
        }
    }

    // Get sections
    $projectSections = Asana::getProjectSections($project->data->id);

    // Add tasks
    foreach($tasks->data as $task) {
        $taskTemplate = Asana::getTask($task->id);

        if( !in_array($taskTemplate->data->name, $sections))
        {
            // Get section id
            foreach($projectSections->data as $section)
            {
                if($section->name ==  $taskTemplate->data->memberships[0]->section->name)
                {
                    $sectionId = $section->id;
                }
            }
            if(isset($sectionId))
            {
                $data = [
                    'projects' => [$project->data->id],
                    'name' => $taskTemplate->data->name,
                    'memberships' =>
                        [
                            [
                                'project' => $project->data->id,
                                'section' => $sectionId
                            ]
                        ]
                ];
                Asana::createTask($data);
            }
        }
    }

//    $info = Asana::getProjectsInWorkspace();
//    dd($info);
//
//    $info = Asana::getWorkspaceUsers();
//    dd($info);
//
//    $info = Asana::getWorkspaces();
//    dd($info);
//
//
//    $users = Asana::getUsers();
//    dd($users);

});

Route::group(['middleware' => 'auth'], function(){
    // Dashboard
    Route::get('/', 'DashboardController@getDashboard');
    
    // Logout
    Route::get('auth/logout', 'Auth\AuthController@logout');

    // Account
    Route::get('account', 'UserController@getAccount');
    
    // Update Personal information
    Route::post('account/personal-information/update', 'UserController@postUpdatePersonalInformation');
    
    // Update password
    Route::post('account/password/update', 'UserController@postUpdatePassword');
    
    // Project file upload
    Route::post('projects/{slug}/file-upload', 'ProjectController@postFileUpload');
    // Get project file
    Route::get('projects/{slug}/file/{id}', 'ProjectController@getFile');
    // Delete project file
    Route::get('projects/{slug}/file/{id}/delete', 'ProjectController@deleteFile');

    // User file upload
    Route::post('baboons/{slug}/file-upload', 'UserController@postFileUpload');

    // Get user file
    Route::get('baboons/{slug}/file/{id}', 'UserController@getFile');
    
    // Search
    Route::get('search/autocomplete/results', 'SearchController@autocompleteResults');

    // Admin routes
    Route::group(['middleware' => 'admin'], function(){
        // Get user files
        Route::post('baboons/{slug}/files', 'UserController@getFiles');
        
        // Rename file
        Route::post('file/rename', 'UserController@renameFile');

        // Rename folder
        Route::post('folder/rename', 'UserController@renameFolder');

        // Create folder
        Route::post('baboons/{slug}/folder/create', 'UserController@createFolder');

        // Delete folder
        Route::post('baboons/{slug}/folder/delete', 'UserController@deleteFolder');

        // Delete user file
        Route::get('baboons/{slug}/file/{id}/delete', 'UserController@deleteFile');

        // Update user's rate on a given project
        Route::post('project/updateUserRate', 'ProjectController@updateUserRate');
    });

    // Account manager routes
    Route::group(['middleware' => 'account-manager'], function(){

        // Add new baboon
        Route::get('baboons/new', 'UserController@getNewBaboon');
        Route::post('baboons/new', 'UserController@postNewBaboon');

        // Edit baboon
        Route::get('baboons/{slug}/edit', 'UserController@getEditBaboon');
        Route::post('baboons/{slug}/edit', 'UserController@postEditBaboon');

        // Delete baboon
        Route::get('baboons/{slug}/delete', 'UserController@getDeleteBaboon');
    });

    // Project manager routes
    Route::group(['middleware' => 'project-manager'], function(){
        // New project
        Route::get('projects/new', 'ProjectController@getNewProject');
        Route::post('projects/new', 'ProjectController@postNewProject');
        Route::post('projects/update/company', 'ProjectController@postUpdateCompany');
        Route::post('projects/update/board-id', 'ProjectController@postUpdateBoardId');

        // Edit project
        Route::get('projects/{slug}/edit', 'ProjectController@getEditProject');
        Route::post('projects/{slug}/edit', 'ProjectController@postEditProject');

        // Delete project
        Route::get('projects/{slug}/delete', 'ProjectController@getDeleteProject');

        // Add retainer
        Route::get('projects/{slug}/retain', 'RetainerController@getNewRetainer');
        Route::post('projects/{slug}/retain', 'RetainerController@postNewRetainer');

        // Delete retainer
        Route::get('retainers/{slug}/delete', 'RetainerController@getDeleteRetainer');
        
        // Open project
        Route::get('projects/{slug}/open', 'ProjectController@openProject');

        // Close project
        Route::get('projects/{slug}/close', 'ProjectController@closeProject');
    });

    // Account manager + Project manager routes
    Route::group(['middleware' => 'client-manager'], function(){

        // Add new client
        Route::get('clients/new', 'ClientController@getNewClient');
        Route::post('clients/new', 'ClientController@postNewClient');
        Route::post('ajax/client/new', 'ClientController@ajaxNewClient');

        // Edit client
        Route::get('clients/{slug}/edit', 'ClientController@getEditClient');
        Route::post('clients/{slug}/edit', 'ClientController@postEditClient');

        // Delete client
        Route::get('clients/{slug}/delete', 'ClientController@getDeleteClient');

        // Add new contractor
        Route::get('contractors/new', 'ContractorController@getNewContractor');
        Route::post('contractors/new', 'ContractorController@postNewContractor');

        // Edit contractor
        Route::get('contractors/{slug}/edit', 'ContractorController@getEditContractor');
        Route::post('contractors/{slug}/edit', 'ContractorController@postEditContractor');

        // Delete contractor
        Route::get('contractors/{slug}/delete', 'ContractorController@getDeleteContractor');

    });

    // Public routes

    // List of projects
    Route::get('projects', 'ProjectController@getProjects');

    // Project details
    Route::get('projects/{slug}', 'ProjectController@getProject');

    // People routes
    Route::get('/baboons', 'UserController@getBaboons');
    Route::get('/clients', 'ClientController@getClients');
    Route::get('/contractors', 'ContractorController@getContractors');

    // Single people
    Route::get('baboons/{slug}', 'UserController@getBaboon');
    Route::get('clients/{slug}', 'ClientController@getClient');
    Route::get('contractors/{slug}', 'ContractorController@getContractor');

    // Retainers
    Route::get('retainers/{slug}/report', 'RetainerController@getProjectReport');
    Route::get('retainers/{slug}/period/{id}/report', 'RetainerController@getPeriodReport');

    // Notifications
    Route::get('notifications', 'UserController@getNotifications');

    // New bug
    Route::get('bug/new', 'RetainerController@getNewBug');
    Route::post('bug/new', 'RetainerController@postNewBug');

});

Route::group(['middleware' => 'guest'], function () {
    // Login
    Route::get('/login', 'Auth\AuthController@getLogin');
    Route::post('/login', 'Auth\AuthController@postLogin');
    // Password reset link request routes...
    Route::get('/password/email', 'Auth\PasswordController@getEmail');
    Route::post('/password/email', ['as' => 'password.email.post', 'uses' => 'Auth\PasswordController@postEmail']);
    // Password reset routes...
    Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
    Route::post('password/reset', ['as' => 'password.reset.post', 'uses' => 'Auth\PasswordController@postReset']);
});
