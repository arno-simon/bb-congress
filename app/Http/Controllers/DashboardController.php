<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\Project;

class DashboardController extends Controller
{
    
    public function getDashboard()
    {
        // Get user's projects
//        $projects = \Auth::user()->projects()->get();
//
//        return view('dashboard')->with(['projects' => $projects]);

        $projects = Project::all();
        return view('projects.projects')->with(['projects' => $projects]);
    }


}
