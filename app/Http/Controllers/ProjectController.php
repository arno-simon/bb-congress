<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\Role;
use App\Repositories\ProjectRepository;
use App\Services\AsanaService;
use App\Services\TrelloService;
use App\Services\ToggleService;
use Illuminate\Http\Request;
use App\Models\CMSUser;
use App\Models\FTPUser;
use App\Models\Project;
use App\Models\ProjectFile;
use App\Models\TypeProject;
use App\User;
use App\Models\Contractor;
use App\Models\Client;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Input;

class ProjectController extends Controller
{
    protected $projectRepository;
    protected $toggleService;
    protected $trelloService;
    protected $asanaService;

    public function __construct(ProjectRepository $projectRepository, ToggleService $toggleService, TrelloService $trelloService, AsanaService $asanaService)
    {
        $this->projectRepository = $projectRepository;
        $this->toggleService = $toggleService;
        $this->trelloService = $trelloService;
        $this->asanaService = $asanaService;
    }
    /*
     * GET new project view
     */
    public function getNewProject()
    {
        $clients = Client::all();
        $contractors = Contractor::all();
        $baboons = User::all();
        $types = TypeProject::all();
        $teams = \Asana::getTeams();

        return view('projects.new')->with([
            'types' => $types,
            'clients' => $clients,
            'contractors' => $contractors,
            'baboons' => $baboons,
            'teams' => $teams
        ]);
    }

    /*
     * POST new project
     */
    public function postNewProject(Requests\CreateProjectRequest $request)
    {
        $project = new Project();

        $project->name = $request->get('name');
        $project->slug = str_slug($request->get('name'), '-');
        $project->status = 'ongoing';
        $project->asana_team_id = $request->get('company');
        $project->company = $this->asanaService->getTeamName($request->get('company'));
        if($request->has('client_id')) $project->client_id = $request->get('client_id');

        if($request->has('budget')) $project->budget = $request->get('budget');
        if($request->has('website_url')) $project->website_url = $request->get('website_url');
        if($request->has('cpanel_url')) $project->cpanel_url = $request->get('cpanel_url');
        if($request->has('description')) $project->description = $request->get('description');
        if($request->has('sitemap_url')) $project->sitemap_url = $request->get('sitemap_url');
        if($request->has('invision_url')) $project->invision_url = $request->get('invision_url');
        
        // CMS
        if($request->has('cms_url')) $project->cms_url = $request->get('cms_url');

        // FTP
        if($request->has('ftp_type')) $project->ftp_type = $request->get('ftp_type');
        if($request->has('ftp_host')) $project->ftp_host = $request->get('ftp_host');
        if($request->has('ftp_port')) $project->ftp_port = $request->get('ftp_port');

        // Domain
        if($request->has('domain_url')) $project->domain_url = $request->get('domain_url');
        if($request->has('domain_user')) $project->domain_user = $request->get('domain_user');
        if($request->has('domain_password')) $project->domain_password = Crypt::encrypt($request->get('domain_password'));

        $project->save();
        // Attach types
        $project->types()->attach($request->get('project_type_id'));

        // Attach contractors and baboons
        if($request->has('contractors_id')) $project->contractors()->attach($request->get('contractors_id'));
        if($request->has('baboons_id')) $project->users()->attach($request->get('baboons_id'));

        // Attach admins and project managers to project
        $admins = \App\Models\Role::where('slug', 'admin')->first()->users()->get();
        foreach($admins as $admin)
        {
            $project->users()->attach($admin->id);
        }
        $projectManagers = \App\Models\Role::where('slug', 'project-manager')->first()->users()->get();
        foreach($projectManagers as $projectManager)
        {
            $project->users()->attach($projectManager->id);
        }
        
        // Add CMS users
        if($request->has('cms_login'))
        {
            foreach ($request->get('cms_login') as $user)
            {
                if( !empty($user['user'] || !empty($user['password'])))
                {
                    $cmsUser = new CMSUser();
                    if( !empty($user['user']) ) $cmsUser->user = $user['user'];
                    if( !empty($user['password']) ) $cmsUser->password = Crypt::encrypt($user['password']);
                    $cmsUser->project_id = $project->id;
                    $cmsUser->save();
                }
            }
        }
        // Add FTP users
        if($request->has('ftp_login'))
        {
            foreach ($request->get('ftp_login') as $user)
            {
                if( !empty($user['user'] || !empty($user['password'])))
                {
                    $ftpUser = new FTPUser();
                    if( !empty($user['user']) ) $ftpUser->user = $user['user'];
                    if( !empty($user['password']) ) $ftpUser->password = Crypt::encrypt($user['password']);
                    $ftpUser->project_id = $project->id;
                    $ftpUser->save();
                }
            }
        }

        // Create storage folder
        $path = '/projects/'.$project->slug.'/';
        Storage::makeDirectory($path);

        // Save file
        if($request->hasFile('file'))
        {
            $pathFile = storage_path().'/app/projects/'.$project->slug.'/';
            $file = new ProjectFile();
            $file->name = $request->file('file')->getClientOriginalName();
            $file->path = $pathFile.$request->file('file')->getClientOriginalName();
            $file->project_id = $project->id;
            $file->save();
            Input::file('file')->move($pathFile, $file->name);
        }
        
        // Create asana project
        $project->asana_project_id = $this->asanaService->createProject($project->name, $project->asana_team_id);

        // Create toggle project
        $cid = $this->toggleService->getOrCreateClient($project);
        $project->toggle_client_id = $cid;
        $project->toggle_project_id = $this->toggleService->createProject($project->name, $cid);
        
        $project->save();

        return redirect('/projects')->with([
            'project' => $project,
            'status' => 'Your project '.$project->name.' has been successfully created in Asana and Toggle.',
        ]);

    }


    /*
     * GET all projects view
     */
    public function getProjects()
    {
        $projects = Project::all();
        return view('projects.projects')->with(['projects' => $projects]);
    }

    /*
     * GET single project view
     */
    public function getProject($slug)
    {
        $project = Project::where('slug', $slug)->firstOrFail();

        if($project->is_supported)
        {
            // Update periods
            $this->projectRepository->updateActivePeriod($project->id);
            $activePeriod = $this->projectRepository->getActivePeriod($project->id);
        }
        else{
            $activePeriod = false;
        }
        return view('projects.view')->with(['project' => $project, 'activePeriod' => $activePeriod]);
    }

    /*
     * GET edit project view
     */
    public function getEditProject($slug)
    {
        $clients = Client::all();
        $contractors = Contractor::all();
        $baboons = User::all();
        $types = TypeProject::all();

        $project = Project::where('slug', $slug)->firstOrFail();


        // If current user (a project manager) is not in the project, unauthorized
        if( $project->users()->where('id', \Auth::user()->id)->get()->isEmpty()) abort(401);
        
        return view('projects.edit')->with([
            'types' => $types,
            'project' => $project,
            'clients' => $clients,
            'baboons' => $baboons,
            'contractors' => $contractors,
        ]);
    }

    /*
     * POST edit project
     */
    public function postEditProject(Requests\UpdateProjectRequest $request, $slug)
    {

        $project = Project::where('slug', $slug)->firstOrfail();
        $project->name = $request->get('name');
        if($request->has('client_id')) $project->client_id = $request->get('client_id');


        //Update slug and directory
        $path = '/projects/'.$project->slug.'/';
        if($project-> slug !== str_slug($request->get('name'), '-'))
        {
            $project->slug = str_slug($request->get('name'), '-');
            Storage::move($path, '/projects/'.$project->slug.'/');
        }

        if($request->has('budget')) $project->budget = $request->get('budget');
        if($request->has('website_url')) $project->website_url = $request->get('website_url');
        if($request->has('cpanel_url')) $project->cpanel_url = $request->get('cpanel_url');
        if($request->has('description')) $project->description = $request->get('description');
        if($request->has('sitemap_url')) $project->sitemap_url = $request->get('sitemap_url');
        if($request->has('invision_url')) $project->invision_url = $request->get('invision_url');
        
        // CMS
        if($request->has('cms_url')) $project->cms_url = $request->get('cms_url');

        // FTP
        if($request->has('ftp_type')) $project->ftp_type = $request->get('ftp_type');
        if($request->has('ftp_host')) $project->ftp_host = $request->get('ftp_host');
        if($request->has('ftp_port')) $project->ftp_port = $request->get('ftp_port');

        // Domain
        if($request->has('domain_url')) $project->domain_url = $request->get('domain_url');
        if($request->has('domain_user')) $project->domain_user = $request->get('domain_user');
        if($request->has('domain_password')) $project->domain_password = Crypt::encrypt($request->get('domain_password'));

        $project->save();


        // Attach types
        $project->types()->detach();
        $project->types()->attach($request->get('project_type_id'));

        // Attach client contractors and baboons
        if($request->has('baboons_id'))
        {
            foreach ($project->users as $user)
            {
                if( !in_array($user->id, $request->get('baboons_id')))
                {
                    $project->users()->detach($user->id);
                }

            }

            foreach ($request->get('baboons_id') as $id)
            {
                if( !in_array($id, $project->users->pluck('id')->all()))
                {
                    $project->users()->attach($id);
                }

            }
        }
        $project->contractors()->detach();
        if($request->has('contractors_id')) $project->contractors()->attach($request->get('contractors_id'));


        // Attach admins and project managers to project
        $project->users()->detach();
        $admins = \App\Models\Role::where('slug', 'admin')->first()->users()->get();
        foreach($admins as $admin)
        {
            $project->users()->attach($admin->id);
        }
        $projectManagers = \App\Models\Role::where('slug', 'project-manager')->first()->users()->get();
        foreach($projectManagers as $projectManager)
        {
            $project->users()->attach($projectManager->id);
        }

        // Add CMS users
        if($request->has('cms_login'))
        {
            // Delete existing users
            foreach ($project->cmsUsers()->get() as $user)
            {
                $user->delete();
            }
            foreach ($request->get('cms_login') as $user)
            {
                if( !empty($user['user'] || !empty($user['password'])))
                {
                    $cmsUser = new CMSUser();
                    if( !empty($user['user']) ) $cmsUser->user = $user['user'];
                    if( !empty($user['password']) ) $cmsUser->password = Crypt::encrypt($user['password']);
                    $cmsUser->project_id = $project->id;
                    $cmsUser->save();
                }
            }
        }
        // Add FTP users
        if($request->has('ftp_login'))
        {
            // Delete existing users
            foreach ($project->ftpUsers()->get() as $user)
            {
                $user->delete();
            }
            foreach ($request->get('ftp_login') as $user)
            {
                if( !empty($user['user'] || !empty($user['password'])))
                {
                    $ftpUser = new FTPUser();
                    if( !empty($user['user']) ) $ftpUser->user = $user['user'];
                    if( !empty($user['password']) ) $ftpUser->password = Crypt::encrypt($user['password']);
                    $ftpUser->project_id = $project->id;
                    $ftpUser->save();
                }
            }
        }

        // Save file
        $path = '/projects/'.$project->slug.'/';
        if($request->hasFile('file'))
        {
            $file = new ProjectFile();
            $file->name = $request->file('file')->getClientOriginalName();
            $file->path = $path.$request->file('file')->getClientOriginalName();
            $file->project_id = $project->id;
            $file->save();
            Storage::put($file->path, $request->file('file'));
        }

        // Support type
        switch ($project->support_type)
        {
            case 'monthly': $supportType = 'Monthly support (2H)';
                break;
            case 'paytogo': $supportType = 'Pay to go support';
                break;
            case 'retainer': $supportType = 'Retainer';
                break;
            default: $supportType = '';
        }

        // Update Asana
        if($project->asana_project_id != '') $this->asanaService->updateProjectName($project->asana_project_id, $project->name);
        if($project->asana_support_project_id != '' && $supportType != '') $this->asanaService->updateProjectName($project->asana_support_project_id, $project->name.' - '.$supportType);

        // Update toggle
        if($project->toggle_project_id > 0 && $project->toggle_client_id) $this->toggleService->updateProjectName($project->toggle_project_id, $project->name, $project->toggle_client_id);
        if($project->toggle_support_project_id > 0 && $supportType != '') $this->toggleService->updateProjectName($project->toggle_support_project_id, $project->name.' - '.$supportType, $project->toggle_client_id);


        return redirect('/projects')->with([
            'project' => $project,
            'status' => 'Your project '.$project->name.' has been successfully updated',
        ]);
        
    }

    /*
     * Delete project
     */
    public function getDeleteProject($slug)
    {
        $project = Project::where('slug', $slug)->firstOrfail();
        // If current user (a project manager) is not in the project, unauthorized
        if( $project->users()->where('id', \Auth::user()->id)->get()->isEmpty()) abort(401);
        $name = $project->name;

        // Detach associated users
        $project->users()->detach();
        $project->contractors()->detach();

        // Detach associated types
        $project->types()->detach();

        // Delete files
        foreach ($project->files()->get() as $file)
        {
            $path = '/projects/'.$project->slug.'/'.$file->name;
            Storage::delete($path);
            $file->delete();
        }

        // Delete directory
        Storage::deleteDirectory('/projects/'.$project->slug);

        // Delete cms users
        foreach ($project->cmsUsers()->get() as $user)
        {
            $user->delete();
        }
        // Delete ftp users
        foreach ($project->ftpUsers()->get() as $user)
        {
            $user->delete();
        }
        // Delete boards from Asana
        if($project->asana_project_id != '') $this->asanaService->deleteProject($project->asana_project_id);
        if($project->asana_support_project_id != '') $this->asanaService->deleteProject($project->asana_support_project_id);

        // Delete projects from Toggle
        if($project->toggle_project_id > 0) $this->toggleService->deleteProject($project->toggle_project_id);
        if($project->toggle_support_project_id > 0) $this->toggleService->deleteProject($project->toggle_support_project_id);

        // Delete retainer periods
        foreach ($project->retainerPeriods()->get() as $period)
        {
            $period->delete();
        }
        $project->delete();

        return redirect('/projects')->with([
            'status' => 'The project '.$name.' has been successfully deleted'
        ]);
    }

    /*
     * Upload file to a project from ajax request
     */
    public function postFileUpload($slug)
    {
        $project = Project::where('slug', $slug)->firstOrFail();

        $file = Input::file('file');

        $destinationPath = storage_path().'/app/projects/'.$project->slug.'/';
        $filename = $file->getClientOriginalName();


        $upload_success = Input::file('file')->move($destinationPath, $filename);

        if ($upload_success) {

            $file = new ProjectFile();
            $file->name = $filename;
            $file->path = $destinationPath.$filename;
            $file->project_id = $project->id;
            $file->save();

            return response()->json([
                'success' => true,
            ], 200);
        } else {
            return response()->json([
                'error' => true,
            ], 400);

        }
    }

    /*
     * Download file
     */
    public function getFile($slug, $id)
    {
        $project = Project::where('slug', $slug)->firstOrFail();
        $file = $project->files()->where('id', $id)->first();
        return response()->download($file->path);
    }

    /*
     * Delete file
     */
    public function deleteFile($slug, $id)
    {
        $project = Project::where('slug', $slug)->firstOrFail();
        $file = $project->files()->where('id', $id)->first();
        $name =  $file->name;
        $path = '/projects/'.$project->slug.'/'.$file->name;
        Storage::delete($path);
        $file->delete();
        
        return redirect()->back()->with(['status' => 'The file '.$name.' has been successfully deleted', 'delete_file' => 1]);
    }
    
    /*
     * POST update board id
     */
    public function postUpdateBoardId(Request $request)
    {
        $project = Project::where('slug', $request->get('slug'))->firstOrFail();
        $project->trello_board_id = $request->get('trello_board_id');
        $project->save();

        return response()->json([
            'success' => true,
        ], 200);
    }

    /*
     * POST update company name
     */
    public function postUpdateCompany(Request $request)
    {
        $project = Project::where('slug', $request->get('slug'))->firstOrFail();
        $project->company = $request->get('company');
        $project->save();

        return response()->json([
            'success' => true,
        ], 200);
    }

    /**
     * Set a project to the ongoing status.
     * Close the retainer project from Toggle and the retainer board from Trello.
     * Open main project from Toggle and the main board from Trello.
     * @param $slug
     */
    public function openProject($slug)
    {
        $project = Project::where('slug', $slug)->firstOrFail();

        if($project->status == 'ongoing') return redirect()->back()->with(['warning' => 'Sorry, this project is already ongoing.']);

        // Open main Asana project
        if($project->asana_project_id != '')
        {
            $this->asanaService->openProject($project->asana_project_id);
        }

        // Close support Asana project
        if($project->asana_support_project_id != '')
        {
            $this->asanaService->closeProject($project->asana_support_project_id);
        }

        // Get toggle client id
        $cid = $this->toggleService->getOrCreateClient($project);

        // Open main Toggle project
        if($project->toggle_project_id > 0)
        {
            $this->toggleService->setProjectState($project->toggle_project_id, $project->name, $cid, true);
        }

        // Close support project from Toggle
        if($project->toggle_support_project_id > 0)
        {
            $this->toggleService->setProjectState($project->toggle_support_project_id, $project->name.' - '.$project->retainerType->name, $cid, false);
        }

        // Update project
        $project->status = 'ongoing';
        $project->is_supported = false;
        $project->save();

        return redirect()->back()->with(['status' => 'The project '.$project->name.' has been successfully updated']);

    }

    /**
     * Set a project to the closed status.
     * Close all boards from Trello and projects from Toggle
     * @param $slug
     */
    public function closeProject($slug)
    {
        $project = Project::where('slug', $slug)->firstOrFail();

        if($project->status == 'closed') return redirect()->back()->with(['warning' => 'Sorry, this project is already closed.']);

        // Close main Asana  project
        if($project->asana_project_id != '')
        {
            $this->asanaService->closeProject($project->asana_project_id);
        }

        // Close support Asana project
        if($project->asana_support_project_id != '')
        {
            $this->asanaService->closeProject($project->asana_support_project_id);
        }

        // Get toggle client id
        $cid = $this->toggleService->getOrCreateClient($project);

        // Close main Toggle project
        if($project->toggle_project_id > 0)
        {
            $this->toggleService->setProjectState($project->toggle_project_id, $project->name, $cid, false);
        }

        // Close support project from Toggle
        if($project->toggle_support_project_id > 0)
        {
            $this->toggleService->setProjectState($project->toggle_support_project_id, $project->name.' - '.$project->retainerType->name, $cid, false);
        }

        // Update project
        $project->status = 'closed';
        $project->is_supported = false;
        $project->save();

        return redirect()->back()->with(['status' => 'The project '.$project->name.' has been successfully updated']);
    }
    
    
    public function updateUserRate(Request $request)
    {
        if($request->ajax())
        {
            $this->projectRepository->updateUserRate($request->get('projectId'), $request->get('userId'), $request->get('rate'));
            
            return response()->json([
            ], 200);
        }
        return response()->json([
        ], 401);
    }

}
