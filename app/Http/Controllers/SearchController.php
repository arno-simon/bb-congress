<?php

namespace App\Http\Controllers;

use App\Models\Client;
use App\Models\Contractor;
use App\Models\Project;
use App\Repositories\ProjectRepository;
use App\User;
use Illuminate\Http\Request;
use App\Repositories\UserRepository;

class SearchController extends Controller
{
    protected $userRepository;
    protected $projectRepository;

    public function __construct(UserRepository $userRepository, ProjectRepository $projectRepository)
    {
        $this->userRepository = $userRepository;
        $this->projectRepository = $projectRepository;
    }

    /**
     * Get all users and projects and their URLs
     *
     * @param Request $request
     * @return mixed
     */
    public function autocompleteResults(Request $request)
    {
        if($request->ajax())
        {
            $results = array();

            // Get baboons
            $users = User::all();

            foreach ($users as $user)
            {
                $results[$user->slug]['name'] = $user->firstname . ' ' . $user->lastname;
                $results[$user->slug]['url'] = '/baboons/' . $user->slug;
            }

            // Get clients
            $users = Client::all();

            foreach ($users as $user)
            {
                $results[$user->slug]['name'] = $user->firstname . ' ' . $user->lastname;
                $results[$user->slug]['url'] = '/clients/' . $user->slug;
            }

            // Get contractors
            $users = Contractor::all();

            foreach ($users as $user)
            {
                $results[$user->slug]['name'] = $user->firstname . ' ' . $user->lastname;
                $results[$user->slug]['url'] = '/contractors/' . $user->slug;
            }

            // Get projects results
            $projects = Project::all();

            foreach ($projects as $project)
            {
                $results[$project->slug]['name'] = $project->name;
                $results[$project->slug]['url'] = '/projects/' . $project->slug;
            }
            return \Response::json($results);

        }
        return \Response::json("Error, this is not an ajax request");
    }
    
}
