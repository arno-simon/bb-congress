<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\UserFile;
use App\User;
use Illuminate\Http\Request;
use App\Models\Role;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Input;

class UserController extends Controller
{
    
    /**
     * Show the user account.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAccount()
    {
        $user = \Auth::user();
        $roles = Role::all();
        return view('user.account')->with(['user' => $user, 'roles' => $roles]);
    }

    /*
     * POST update personal information
     */
    public function postUpdatePersonalInformation(Requests\UpdatePersonalInformationRequest $request){
        $user = \Auth::user();
        $user->firstname = $request->get('firstname');
        $user->lastname = $request->get('lastname');
        $user->email = $request->get('email');

        //Update slug and directory
        if($user->slug !== str_slug($request->get('firstname').' '.$request->get('lastname'),'-'))
        {
            $user->slug = str_slug($request->get('firstname').' '.$request->get('lastname'),'-');
            if($user->type->name == 'baboon')
            {
                // Create storage folder
                $path = '/users/'.$user->slug.'/';
                Storage::move($path, '/users/'.$user->slug.'/');
            }

        }

        if($request->hasFile('picture'))
        {
            $file = $request->file('picture');
            $fileName = pathinfo($file->getClientOriginalName(),PATHINFO_FILENAME);
            $originalName = $fileName;
            $extension = $request->file('picture')->getClientOriginalExtension();
            $fileName .= ".".$extension;
            $path = public_path().'/images/users/';

            // Rename if file exists
            $i = 1;
            while(file_exists($path.$fileName))
            {
                $actual_name = $originalName.'-'.$i;
                $fileName = $actual_name.".".$extension;
                $i++;
            }
            // Save file
            $request->file('picture')->move($path, $fileName);
            $user->picture = '/images/users/'.$request->file('picture')->getClientOriginalName();
        }elseif($request->get('hasPicture') == 'false'){
            $user->picture = '/images/users/baboon.png';
        }


        $user->save();
        return redirect('/account')->with(['ok-personal-information' => 'Your personal information have been updated']);

    }

    /*
     * POST update password
     */
    public function postUpdatePassword(Requests\UpdatePasswordRequest $request){
        $user = \Auth::user();

        if(\Hash::check($request->get('old_password'), $user->password))
        {
            $user->password = bcrypt($request->get('password'));
            $user->save();
            return redirect('/account')->with(['ok-update-settings' => 'Your password has been updated.']);
        }

        return redirect()->back()->withErrors(['old_password' => 'Your old password is incorrect.']);
    }

    /*
     * GET new people view
     */
    public function getNewBaboon()
    {
        $roles = Role::all();
        return view('user.new')->with(['roles' => $roles]);
    }

    /*
     * POST new people
     */
    public function postNewBaboon(Requests\CreateBaboonRequest $request)
    {
        $user = new User();
        $user->firstname = $request->get('firstname');
        $user->lastname = $request->get('lastname');
        $user->email = $request->get('email');
        $user->password = bcrypt($request->get('password'));
        $user->picture = '/images/users/baboon.png';
        $user->slug = str_slug($request->get('firstname').' '.$request->get('lastname'),'-');

        $user->save();

        // Roles
        $user->roles()->attach($request->get('roles'));
        $user->save();

        // Create storage folder
        $path = '/users/'.$user->slug.'/';
        Storage::makeDirectory($path);

        // Save file
        if($request->hasFile('file'))
        {
            $pathFile = storage_path().'/app/users/'.$user->slug.'/';
            $file = new UserFile();
            $file->name = $request->file('file')->getClientOriginalName();
            $file->path = $pathFile.$request->file('file')->getClientOriginalName();
            $file->user_id = $user->id;
            $file->save();

            Input::file('file')->move($pathFile, $file->name);
        }

        return redirect()->back()->with(['status' => $user->firstname.' '.$user->lastname.' has been successfully added']);
    }
    
    /*
     * GET edit people view
     */
    public function getEditBaboon($slug)
    {
        $user = User::where('slug', $slug)->firstOrFail();
        $roles = Role::all();
        return view('user.edit')->with(['user' => $user, 'roles' => $roles]);
    }

    /*
     * POST edit people
     */
    public function postEditBaboon(Requests\UpdateBaboonRequest $request, $slug)
    {
        $user = User::where('slug', $slug)->firstOrFail();
        $user->firstname = $request->get('firstname');
        $user->lastname = $request->get('lastname');
        $user->email = $request->get('email');

        //Update slug and directory
        if($user->slug !== str_slug($request->get('firstname').' '.$request->get('lastname'),'-'))
        {
            $user->slug = str_slug($request->get('firstname').' '.$request->get('lastname'),'-');
            // Create storage folder
            $path = '/users/'.$user->slug.'/';
            Storage::move($path, '/users/'.$user->slug.'/');
            
        }

        // Roles
        $user->roles()->detach();
        $user->roles()->attach($request->get('roles'));

        $user->save();
        return redirect('/baboons')->with(['status' => $user->firstname.' '.$user->lastname.' has been successfully updated']);
    }

    /*
     * GET user profile
     */
    public function getBaboon($slug)
    {
        $user = User::where('slug', $slug)->firstOrFail();
        return view('user.profile')->with(['user' => $user]);
    }
    
    public function getBaboons()
    {
        $baboons = User::all();
        return view('user.baboons')->with(['baboons' => $baboons]);
    }

    /*
     * Delete people
     */
    public function getDeleteBaboon($slug)
    {
        $user = User::where('slug', $slug)->firstOrfail();
        $firstname = $user->firstname;
        $lastname = $user->lastname;
        $user->projects()->detach();
        $user->roles()->detach();

        // Delete files
        foreach ($user->files()->get() as $file)
        {
            $path = '/users/'.$user->slug.'/'.$file->name;
            Storage::delete($path);
            $file->delete();
        }
        // Delete directory
        Storage::deleteDirectory('/users/'.$user->slug);
        
        $user->delete();

        return redirect('/baboons/')->with(['status' => $firstname.' '.$lastname.' has been successfully deleted']);
    }

    /*
     * Upload file to a project from ajax request
     */
    public function postFileUpload(Request $request, $slug)
    {
        $user = User::where('slug', $slug)->firstOrFail();

        $file = Input::file('file');

        $path = trim($request->get('path'), '/');
        $request->get('path') !== '' ? $destinationPath = storage_path().'/app/users/'.$user->slug.'/'.$path.'/' : $destinationPath = storage_path().'/app/users/'.$user->slug.'/';

        $filename = $file->getClientOriginalName();


        $upload_success = Input::file('file')->move($destinationPath, $filename);

        if ($upload_success) {

            $file = new UserFile();
            $file->name = $filename;
            $file->path = $destinationPath.$filename;
            $file->user_id = $user->id;
            $file->save();

            return response()->json([
                'success' => true,
            ], 200);
        } else {
            return response()->json([
                'error' => true,
            ], 400);

        }
    }
    /*
     * Get Files
     */
    public function getFiles(Request $request, $slug)
    {
        $user = User::where('slug', $slug)->firstOrFail();

        $dir = storage_path() . '/app/users/' . $user->slug;

        if($request->get('path') !== '/') $dir .= $request->get('path');

        // Run the recursive function
        $response = scan($dir);
        
        // Output the directory listing as JSON

        return response()->json([
            'name' => 'Files',
            'type' => 'folder',
            'path' => $dir,
            'items' => $response
        ], 200);

    }

    /*
     * Rename file
     */
    public function renameFile(Request $request)
    {
        $file = UserFile::findOrFail($request->get('id'));

        // Update DB File
        $file->name = $request->get('value');
        $path = explode('/', $file->path);
        $index = count($path) -1;
        $path[$index] = $request->get('value');
        $newPath = implode('/', $path);

        // Rename file
        if(file_exists($file->path)) rename($file->path, $newPath);

        $file->path = $newPath;

        $file->save();

        return response()->json([
        ], 200);

    }

    /*
     * Rename folder
     */
    public function renameFolder(Request $request)
    {
        $oldPath = $request->get('old_path');

        $path = explode('/', $oldPath);
        $index = count($path) -1;
        $path[$index] = $request->get('value');
        $newPath = implode('/', $path);
        
        // Rename folder
        if(is_dir($oldPath)) rename($oldPath, $newPath);
        
        return response()->json([
        ], 200);

    }

    /*
     * Create folder
     */
    public function createFolder(Request $request, $slug)
    {
        $user = User::where('slug', $slug)->firstOrFail();

        $dir = storage_path() . '/app/users/' . $user->slug;

        if($request->get('path') !== '') $dir .= $request->get('path');
        
        $dir .= '/' . $request->get('name');

        // Create folder
        if( !is_dir($dir)) mkdir($dir);

        return response()->json([
        ], 200);

    }

    /*
     * Delete folder
     */
    public function deleteFolder(Request $request, $slug)
    {
        $user = User::where('slug', $slug)->firstOrFail();

        $dir = $request->get('path');

        // Delete user files in DB
        foreach ($user->files()->get()  as $file)
        {
            if (strpos($file->path, $dir) !== false) {
                $file->delete();
            }
        }

        // Delete folder
        removeDirectory($dir);

        return response()->json([
        ], 200);

    }

    /*
     * Download file
     */
    public function getFile($slug, $id)
    {
        $user = User::where('slug', $slug)->firstOrFail();
        $file = $user->files()->where('id', $id)->first();
        return response()->download($file->path);
    }
    
    /*
     * Delete file
     */
    public function deleteFile($slug, $id)
    {
        $user = User::where('slug', $slug)->firstOrFail();
        $file = $user->files()->where('id', $id)->first();
        $name =  $file->name;
        if(is_file($file->path)) unlink($file->path);
        $file->delete();

        return redirect()->back()->with(['status' => 'The file '.$name.' has been successfully deleted']);
    }


    public function getNotifications()
    {
        $notifications = \Auth::user()->notifications();

        return view('user.notifications')->with(['notifications' => $notifications]);
    }

}


// This function scans the files folder recursively, and builds a large array
function scan($dir){
    $files = array();

    // Is there actually such a folder/file?
    if(file_exists($dir)){
        foreach(scandir($dir) as $f) {
            if(!$f || $f[0] == '.') {
                continue; // Ignore hidden files
            }

            if(is_dir($dir . '/' . $f)) {
                // The path is a folder
                $files[] = array(
                    "name" => $f,
                    "type" => "folder",
                    "path" => $dir . '/' . $f,
                    "items" => scan($dir . '/' . $f) // Recursively get the contents of the folder
                );
            }
            else {
                $file = UserFile::where('path', 'like', '%'.$dir.'/'.$f)->firstOrFail();

                // It is a file
                $files[] = array(
                    "name" => $f,
                    "type" => "file",
                    "path" =>  $dir . '/' . $f,
                    "id" => $file->id,
                    "src" => "/people/".$file->user()->first()->slug."/file/".$file->id,
                    "delete" => "/people/".$file->user()->first()->slug."/file/".$file->id."/delete",
                    "size" => filesize($dir . '/' . $f)// Gets the size of this file
                );
            }
        }
    }
    return $files;
}



/*
 * Delete directory and his files recursively
 */
function removeDirectory($path) {
    $files = glob($path . '/*');
    foreach ($files as $file) {
        is_dir($file) ? removeDirectory($file) : unlink($file);
    }
    rmdir($path);
    return;
}

