<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\UserFile;
use App\Services\TrelloService;
use App\Models\Client;
use Illuminate\Http\Request;
use App\Models\Role;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class ClientController extends Controller
{
    
    /*
     * GET new people view
     */
    public function getNewClient()
    {
        return view('clients.new');
    }

    /*
     * POST new people
     */
    public function postNewClient(Requests\CreateClientRequest $request)
    {
        $user = new Client();
        $user->firstname = $request->get('firstname');
        $user->lastname = $request->get('lastname');
        $user->email = $request->get('email');
        $user->picture = '/images/users/client.png';
        $user->slug = str_slug($request->get('firstname').' '.$request->get('lastname'),'-');

        $user->save();
        
        return redirect()->back()->with(['status' => $user->firstname.' '.$user->lastname.' has been successfully added']);
    }
    
    /*
     * GET edit people view
     */
    public function getEditClient($slug)
    {
        $user = Client::where('slug', $slug)->firstOrFail();
        return view('clients.edit')->with(['user' => $user]);
    }

    /*
     * POST edit people
     */
    public function postEditClient(Requests\UpdateClientRequest $request, $slug)
    {
        $user = Client::where('slug', $slug)->firstOrFail();
        $user->firstname = $request->get('firstname');
        $user->lastname = $request->get('lastname');
        $user->email = $request->get('email');
        $user->slug = str_slug($request->get('firstname').' '.$request->get('lastname'),'-');

        $user->save();
        return redirect('/clients/'.$user->slug)->with(['status' => $user->firstname.' '.$user->lastname.' has been successfully updated']);
    }

    /*
     * GET user profile
     */
    public function getClient($slug)
    {
        $user = Client::where('slug', $slug)->firstOrFail();
        return view('clients.profile')->with(['user' => $user]);
    }
    
    public function getClients()
    {
        $clients = Client::all();
        return view('clients.clients')->with(['clients' => $clients]);
    }

    /*
     * Delete people
     */
    public function getDeleteClient($slug)
    {
        $user = Client::where('slug', $slug)->firstOrfail();

        if(count($user->projects) > 0) return redirect('/clients')->with(['warning' => 'Sorry, you cannot delete this client as long as he owns some projects. Delete his projects first.']);

        $firstname = $user->firstname;
        $lastname = $user->lastname;

        $user->delete();

        return redirect('/clients/')->with(['status' => $firstname.' '.$lastname.' has been successfully deleted']);
    }

    
    public function ajaxNewClient(Request $request)
    {
        $this->validate($request, [
            'lastname' => 'required|string',
            'firstname' => 'required|string',
            'email' => 'required|email|unique:clients,email',
        ]);


        $user = new Client();
        $user->firstname = $request->get('firstname');
        $user->lastname = $request->get('lastname');
        $user->email = $request->get('email');
        $user->picture = '/images/users/client.png';
        $user->slug = str_slug($request->get('firstname').' '.$request->get('lastname'),'-');

        $user->save();


        return response()->json([
            'client' => $user
        ], 200);

    }

}
