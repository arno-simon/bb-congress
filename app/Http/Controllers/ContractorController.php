<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\Contractor;


class ContractorController extends Controller
{
    
    /*
     * GET new people view
     */
    public function getNewContractor()
    {
        return view('contractors.new');
    }

    /*
     * POST new people
     */
    public function postNewContractor(Requests\CreateContractorRequest $request)
    {
        $user = new Contractor();
        $user->firstname = $request->get('firstname');
        $user->lastname = $request->get('lastname');
        $user->email = $request->get('email');
        $user->picture = '/images/users/client.png';
        $user->slug = str_slug($request->get('firstname').' '.$request->get('lastname'),'-');

        $user->save();
        
        return redirect()->back()->with(['status' => $user->firstname.' '.$user->lastname.' has been successfully added']);
    }
    
    /*
     * GET edit people view
     */
    public function getEditContractor($slug)
    {
        $user = Contractor::where('slug', $slug)->firstOrFail();
        return view('contractors.edit')->with(['user' => $user]);
    }

    /*
     * POST edit people
     */
    public function postEditContractor(Requests\UpdateContractorRequest $request, $slug)
    {
        $user = Contractor::where('slug', $slug)->firstOrFail();
        $user->firstname = $request->get('firstname');
        $user->lastname = $request->get('lastname');
        $user->email = $request->get('email');
        $user->slug = str_slug($request->get('firstname').' '.$request->get('lastname'),'-');

        $user->save();
        return redirect('/contractors/'.$user->slug)->with(['status' => $user->firstname.' '.$user->lastname.' has been successfully updated']);
    }

    /*
     * GET user profile
     */
    public function getContractor($slug)
    {
        $user = Contractor::where('slug', $slug)->firstOrFail();
        return view('contractors.profile')->with(['user' => $user]);
    }
    
    public function getContractors()
    {
        $contractors = Contractor::all();
        return view('contractors.contractors')->with(['contractors' => $contractors]);
    }

    /*
     * Delete people
     */
    public function getDeleteContractor($slug)
    {
        $user = Contractor::where('slug', $slug)->firstOrfail();
        
        $firstname = $user->firstname;
        $lastname = $user->lastname;
        $user->projects()->detach();
        $user->delete();

        return redirect('/contractors/')->with(['status' => $firstname.' '.$lastname.' has been successfully deleted']);
    }

}
