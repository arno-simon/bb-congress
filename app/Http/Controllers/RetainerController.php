<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\Project;
use App\Models\RetainerPeriod;
use App\Models\RetainerType;
use App\Repositories\ProjectRepository;
use App\Services\AsanaService;
use App\Services\ToggleService;
use App\Services\TrelloService;
use Carbon\Carbon;

class RetainerController extends Controller
{
    protected $projectRepository;
    protected $toggleService;
    protected $asanaService;

    public function __construct(ProjectRepository $projectRepository, ToggleService $toggleService, AsanaService $asanaService)
    {
        $this->projectRepository = $projectRepository;
        $this->toggleService = $toggleService;
        $this->asanaService = $asanaService;
    }
    public function getNewRetainer($slug)
    {
        $project = Project::where('slug', $slug)->firstOrFail();

        // Project currently supported
        if( $project->is_supported) return redirect()->back()->with(['warning' => 'Sorry, this project is already retained.']);

        // Project has been previously supported
        if( $project->asana_support_project_id != '')
        {
            // Close main Asana project
            if($project->asana_project_id != '')
            {
                $this->asanaService->closeProject($project->asana_project_id);
            }

            // Open support Asana project
            $this->asanaService->openProject($project->asana_support_project_id);

            // Get toggle client id
            $cid = $this->toggleService->getOrCreateClient($project);

            // Close main Toggle project
            if($project->toggle_project_id > 0)
            {
                $this->toggleService->setProjectState($project->toggle_project_id, $project->name, $cid, false);
            }

            // Open support project from Toggle
            if($project->toggle_support_project_id > 0)
            {
                $this->toggleService->setProjectState($project->toggle_support_project_id, $project->name.' - '.$project->retainerType->name, $cid, true);
            }

            // Update project
            $project->status = 'retained';
            $project->is_supported = true;
            $project->save();

            return redirect()->back()->with(['status' => 'The project '.$project->name.' has been successfully updated']);
        }

        // Project has never been supported
        $retainerTypes = RetainerType::all();

        return view('retainers.new')->with(['project' => $project, 'retainerTypes' => $retainerTypes]);
    }
    
    public function postNewRetainer(Requests\CreateRetainerRequest $request, $slug)
    {
        $project = Project::where('slug', $slug)->firstOrFail();
        if( $project->is_supported) return redirect()->back()->with(['warning' => 'Sorry, this project is already retained.']);

        // Update project
        $project->is_supported = true;
        $project->retainer_type_id = $request->get('retainer_type_id');
        $project->save();

        // New period
        $period = new RetainerPeriod();
        $period->project_id = $project->id;
        $period->from = Carbon::createFromFormat('d-m-Y', $request->get('from'));
        $period->to = Carbon::createFromFormat('d-m-Y', $request->get('to'));
        $period->time_due = $request->get('time_due');
        $period->time_left = $request->get('time_due');
        $period->monthly_time = $request->get('time_due');
        $period->status = 'active';
        $period->save();

        // Create Asana retainer project
        $project->asana_support_project_id = $this->asanaService->createProject($project->name.' - '.$project->retainerType->name, $project->asana_team_id);

        // Close Asana main project
        $this->asanaService->closeProject($project->asana_project_id);

        // Create toggle project
        $cid = $this->toggleService->getOrCreateClient($project);
        $project->toggle_client_id = $cid;
        $project->toggle_support_project_id = $this->toggleService->createProject($project->name.' - '.$project->retainerType->name, $cid);

        // Close toggle main project
        $this->toggleService->setProjectState($project->toggle_project_id, $project->name, $cid, false);

        $project->status = 'retained';
        $project->save();

        return redirect('/projects/')->with([
            'project' => $project,
            'status' => $project->name.' - Retainer has been created in Asana and Toggle.'
        ]);
    }

    public function getProjectReport($slug)
    {
        $project = Project::where('slug', $slug)->firstOrFail();
        $projectName = preg_replace('/\s+/', '+', $project->name . ' - '. $project->retainerType->name);

        // Year report
        $since = Carbon::now()->startOfYear()->toDateString();
        $until = Carbon::now()->toDateString();

        $urlReport = "https://toggl.com/reports/api/v2/details.pdf?rounding=Off&time_format_mode=improved&project_count=1&project_names=".$projectName."&status=active&billable=both&calculate=time&sortDirection=asc&sortBy=date&page=1&subgrouping=time_entries&order_field=date&order_desc=off&distinct_rates=Off&project_ids=".$project->toggle_support_project_id."&description=&since=".$since."&until=".$until."&with_total_currencies=1&user_agent=Toggl+New+3.84.0&workspace_id=".config('toggle.workspace_id')."&bars_count=31&subgrouping_ids=true&bookmark_token=&date_format=MM%2FDD%2FYYYY";
        return redirect($urlReport);
    }
    
    public function getPeriodReport($slug, $id)
    {
        $project = Project::where('slug', $slug)->firstOrFail();
        $projectName = preg_replace('/\s+/', '+', $project->name . ' - '. $project->retainerType->name);
        
        $period = RetainerPeriod::findOrFail($id);

        $urlReport = "https://toggl.com/reports/api/v2/details.pdf?rounding=Off&time_format_mode=improved&project_count=1&project_names=".$projectName."&status=active&billable=both&calculate=time&sortDirection=asc&sortBy=date&page=1&subgrouping=time_entries&order_field=date&order_desc=off&distinct_rates=Off&project_ids=".$project->toggle_support_project_id."&description=&since=".$period->from."&until=".$period->to."&with_total_currencies=1&user_agent=Toggl+New+3.84.0&workspace_id=".config('toggle.workspace_id')."&bars_count=31&subgrouping_ids=true&bookmark_token=&date_format=MM%2FDD%2FYYYY";
        return redirect($urlReport);
    }
    
    public function getDeleteRetainer($slug)
    {
        $project = Project::where('slug', $slug)->firstOrFail();

        // If current user (a project manager) is not in the project, unauthorized
        if( $project->users()->where('id', \Auth::user()->id)->get()->isEmpty()) abort(401);
        
        $retainerType = $project->retainerType->name;
        
        // Delete Asana Support project
        $this->asanaService->deleteProject($project->asana_support_project_id);
        
        // Delete Toggle Project
        $this->toggleService->deleteProject($project->toggle_support_project_id);
        
        // Update project
        $project->is_supported = false;
        $project->retainer_type_id = null;
        $project->asana_support_project_id = null;
        $project->toggle_support_project_id = null;
        $project->status = 'closed';
        $project->save();


        // Delete periods
        RetainerPeriod::where('project_id', $project->id)->delete();
        

        return redirect('/projects/'.$project->slug)->with(['status' => 'The '. $project->name.' - '.$retainerType.' has been deleted from Asana and Toggle.']);
    }

    public function getNewBug()
    {
        $projects = Project::where('is_supported', false)->get();

        return view('retainers.newBug')->with(['projects' => $projects]);
    }
}
