<?php

namespace App\Http\Middleware;

use Closure;

class ProjectManager
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        /*
         * Authorize Project Manager
         */

        if( !$request->user()->isProjectManager()) abort(401);

        return $next($request);
    }
}
