<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UpdatePersonalInformationRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = \Auth::user()->id;
        return [
            'lastname' => 'required|string',
            'firstname' => 'required|string',
            'email' => 'required|email|unique:users,email,'.$id,
            'picture' => 'mimes:jpeg,bmp,png'
        ];
    }
}
