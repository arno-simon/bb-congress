<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\User;

class UpdateBaboonRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = User::find($this->get('id'))->id;
        return [
            'lastname' => 'required|string',
            'firstname' => 'required|string',
            'email' => 'required|email|unique:users,email,'.$id,
            'roles' => 'required'
        ];
    }
}
