<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CreateRetainerRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'retainer_type_id' => 'required',
            'time_due' => 'required|numeric',
            'from' => 'required|date_format:d-m-Y',
            'to' => 'required|date_format:d-m-Y'
        ];
    }
}
