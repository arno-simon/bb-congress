<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CreateBaboonRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'lastname' => 'required|string',
            'firstname' => 'required|string',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|confirmed',
            'file' => 'mimes:pdf,doc',
            'roles' => 'required'
        ];
    }
}
