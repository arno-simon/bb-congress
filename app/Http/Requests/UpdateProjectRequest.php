<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Models\Project;

class UpdateProjectRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->get('id');
        return [
            'project_type_id' => 'required',
            'name' => 'required|string|unique:projects,name,'.$id,
            'budget' => 'numeric',
            'website_url' => 'url',
            'cpanel_url' => 'url',
            'sitemap_url' => 'url',
            'invision_url' => 'url',
            'description' => 'string',
            'file' => 'mimes:pdf,doc',
            'client_id' => 'exists:clients,id',
            'baboons_id' => 'exists:users,id',
            'retailers_id' => 'exists:contractors,id',
            'cms_url' => 'url',
            'ftp_type' => 'string',
            'ftp_host' => 'ip',
            'ftp_port' => 'numeric',
            'domain_url' => 'url',
            'domain_user' => 'string',
            'domain_password' => 'string'
        ];
    }
}
