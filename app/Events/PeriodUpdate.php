<?php

namespace App\Events;

use App\Models\Notification;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class PeriodUpdate extends Event implements ShouldBroadcast{
    public $notification;

    public function __construct(Notification $notification)
    {
        $this->notification = $notification;
    }

    public function broadcastOn()
    {
        return ['period-channel'];
    }
}