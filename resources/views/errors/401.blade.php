@extends('errors.template')

@section('body')
    <style type="text/css">
        html,body {
            height: 100%;
        }
        body {
            background: #fff;
            overflow: hidden;
        }

    </style>

    <script type="text/javascript" src="/assets/widgets/wow/wow.js"></script>
    <script type="text/javascript">
        /* WOW animations */

        wow = new WOW({
            animateClass: 'animated',
            offset: 100
        });
        wow.init();
    </script>

    <img src="/assets/image-resources/blurred-bg/blurred-bg-7.jpg" class="login-img wow fadeIn" alt="">

    <div class="center-vertical">
        <div class="center-content">

            <div class="col-md-6 center-margin">
                <div class="server-message wow bounceInDown inverse">
                    <h1>Error 401</h1>
                    <h2>Unauthorized </h2>
                    <p>Sorry, you are not allowed to access this page. Please contact your website administrator for more information.</p><br/><br/>

                    <p><a href="{{ url('/') }}" class="btn btn-lg btn-success">Return to dashboard</a></p>
                </div>
            </div>

        </div>
    </div>
@endsection