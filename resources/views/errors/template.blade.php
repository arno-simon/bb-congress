<!DOCTYPE html>
<html lang="en">
<head>

    <style>
        #loading .svg-icon-loader {position: absolute;top: 50%;left: 50%;margin: -50px 0 0 -50px;}
    </style>


    <meta charset="UTF-8">
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->

    <title>BB-Congress</title>

    @yield('header')
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- Favicons -->
    <link href="/assets/images/icons/favicon-012.png" rel="shortcut icon" />

    <!-- Custom CSS -->
    <link rel="stylesheet" type="text/css" href="/css/custom.css">

    <!-- HELPERS -->

    <link rel="stylesheet" type="text/css" href="/assets/helpers/animate.css">
    <link rel="stylesheet" type="text/css" href="/assets/helpers/boilerplate.css">
    <link rel="stylesheet" type="text/css" href="/assets/helpers/border-radius.css">
    <link rel="stylesheet" type="text/css" href="/assets/helpers/grid.css">
    <link rel="stylesheet" type="text/css" href="/assets/helpers/page-transitions.css">
    <link rel="stylesheet" type="text/css" href="/assets/helpers/spacing.css">
    <link rel="stylesheet" type="text/css" href="/assets/helpers/typography.css">
    <link rel="stylesheet" type="text/css" href="/assets/helpers/utils.css">
    <link rel="stylesheet" type="text/css" href="/assets/helpers/colors.css">


    <!-- ELEMENTS -->

    <link rel="stylesheet" type="text/css" href="/assets/elements/buttons.css">
    <link rel="stylesheet" type="text/css" href="/assets/elements/forms.css">
    <link rel="stylesheet" type="text/css" href="/assets/elements/response-messages.css">


    <!-- Admin theme -->

    <link rel="stylesheet" type="text/css" href="/assets/themes/admin/layout.css">
    <link rel="stylesheet" type="text/css" href="/assets/themes/admin/color-schemes/default.css">

    <!-- Components theme -->

    <link rel="stylesheet" type="text/css" href="/assets/themes/components/default.css">
    <link rel="stylesheet" type="text/css" href="/assets/themes/components/border-radius.css">

    <!-- Admin responsive -->

    <link rel="stylesheet" type="text/css" href="/assets/helpers/responsive-elements.css">
    <link rel="stylesheet" type="text/css" href="/assets/helpers/admin-responsive.css">

    <!-- JS Core -->
    <script type="text/javascript" src="/assets/js-core/jquery-core.js"></script>
    <script type="text/javascript" src="/assets/js-core/jquery-ui-core.js"></script>

</head>
<body>

@yield('body')


</body>
</html>
