@extends('template')
@section('body')

    @include('breadcrumbs', array('breadcrumb_title' => 'New retainer for project '.$project->name, 'breadcrumb_list' => [['Dashboard','/'], ['Retainers','']]) )

    @if(session()->has('status'))
        <div class="alert alert-success flex mrg25B">
            <div class="bg-green alert-icon">
                <i class="glyph-icon icon-check"></i>
            </div>
            <div class="alert-content">
                <h4 class="alert-title">{{ session('status') }}</h4>
            </div>
        </div>
    @endif

    @if(session()->has('project'))
        <script>
            @foreach(session('project')->users() as $baboon)

                @if(\Auth::user()->id == $baboon->id)
                    type = 'admin';
                @else
                    type = 'normal';
                @endif
                addMemberToBoard("{{ session('project')->trello_support_board_id }}", "{{ $baboon->trello_user_id }}", type);
            @endforeach
        </script>
    @endif

    <div class="row">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                    <div class="content-box mrg15B">
                        <h3 class="content-box-header clearfix">
                            <i class="glyph-icon icon-info-circle"></i>&nbsp;
                            Retainer information
                        </h3>
                        <div class="content-box-wrapper pad0T clearfix">
                            <form id="add-new-people" class="form-horizontal pad15L pad15R bordered-row" method="POST" action="{{ url('projects/'.$project->slug.'/retain') }}">
                                {!! csrf_field() !!}

                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-auto control-label">Type *</label>
                                        <div class="col-xs-10 col-sm-9 col-md-8">
                                            <select id="retainer-type-id" name="retainer_type_id" class="form-control" id="retainer-type">
                                                @foreach($retainerTypes as $retainerType)
                                                    <option value="{{ $retainerType->id }}" @if(old('retainer_type_id') == $retainerType->id){{ 'selected' }}@endif>{{ $retainerType->name }}</option>
                                                @endforeach
                                            </select>
                                            @if ($errors->has('retainer_type_id'))
                                                <span class="help-block">
                                                    {{ $errors->first('retainer_type_id') }}
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-auto control-label col-hours">Monthly hours *</label>
                                        <div class="col-xs-10 col-sm-9 col-md-3">
                                            <input id="input-time-due" type="number" min="1" class="form-control {{ $errors->has('time_due') ? ' parsley-error' : '' }}" name="time_due" value="{{ old('time_due') }}">
                                        @if ($errors->has('time_due'))
                                                <span class="help-block">
                                                    {{ $errors->first('time_due') }}
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-auto control-label">From *</label>
                                        <div class="col-xs-5 col-sm-4 col-md-4">
                                            <div class="input-prepend input-group">
                                                <span class="add-on input-group-addon">
                                                    <i class="glyph-icon icon-calendar"></i>
                                                </span>
                                                <input type="text" name="from" class="bootstrap-datepicker form-control" value="{{ old('from') }}">
                                            </div>
                                            @if ($errors->has('from'))
                                                <span class="help-block">
                                                    {{ $errors->first('from') }}
                                                </span>
                                            @endif
                                        </div>
                                        <label class="col-xs-1 control-label">To</label>
                                        <div class="col-xs-5 col-sm-4 col-md-4">
                                            <div class="input-prepend input-group">
                                                <span class="add-on input-group-addon">
                                                    <i class="glyph-icon icon-calendar"></i>
                                                </span>
                                                <input type="text" name="to" id="to" class="form-control disabled" value="{{ old('to') }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="button-pane mrg20T">
                                    <button type="submit" class="btn btn-info">Add retainer</button>
                                </div>
                            </form>

                        </div>

                    </div>
                </div>
            </div>
    </div>
@stop

@section('script')
    <script>
        $(function() {
            "use strict";

            // Monthly support
            if($('#retainer-type-id').val() == 2)
            {
                $('#input-time-due').val(2).prop('readonly', true);
            }
            if($('#retainer-type-id').val() == 3)
            {
                $('.col-hours').html('Hours paid *');
            }

            $('#retainer-type-id').change(function () {
               if($(this).val() == 2)
               {
                   $('.col-hours').html('Monthly hours *');
                   $('#input-time-due').val(2).prop('readonly', true);
               }
               else if($(this).val() == 3)
               {
                   $('.col-hours').html('Hours paid *');
                   $('#input-time-due').val('').prop('readonly', false);
               }

               else{
                   $('.col-hours').html('Monthly hours *');
                   $('#input-time-due').val('').prop('readonly', false);
               }
            });

            $('.bootstrap-datepicker').bsdatepicker({
                format: 'dd-mm-yyyy'
            })
                    .on('changeDate', function (e) {
                        var to = moment(e.date).add(4, 'weeks').format('DD-MM-YYYY');
                        $('#to').val(to);
                    });


        });
    </script>
@stop
