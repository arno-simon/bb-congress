@extends('template')
@section('body')

    @include('breadcrumbs', array('breadcrumb_title' => 'New bug', 'breadcrumb_list' => [['Dashboard','/'], ['Bugs','']]) )

    @if(session()->has('status'))
        <div class="alert alert-success flex mrg25B">
            <div class="bg-green alert-icon">
                <i class="glyph-icon icon-check"></i>
            </div>
            <div class="alert-content">
                <h4 class="alert-title">{{ session('status') }}</h4>
            </div>
        </div>
    @endif

    <div class="row">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                    <div class="content-box mrg15B">
                        <h3 class="content-box-header clearfix">
                            <i class="glyph-icon icon-info-circle"></i>&nbsp;
                            Bug information
                        </h3>
                        <div class="content-box-wrapper pad0T clearfix">
                            <form id="add-new-bug" class="form-horizontal pad15L pad15R bordered-row" method="POST" action="{{ url('bug/new') }}">
                                {!! csrf_field() !!}
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-auto control-label">Project *</label>
                                            <div class="col-xs-10 col-sm-9 col-md-8">
                                                <select name="project_id" class="chosen-select" id="type-select" style="display: none;">
                                                    <option></option>
                                                    @foreach($projects as $project)
                                                        <option value="{{ $project->id }}" @if(old('project_id') == $project->id){{ 'selected' }}@endif>{{ $project->name }}</option>
                                                    @endforeach
                                                </select>
                                                @if ($errors->has('project_id'))
                                                    <span class="help-block">
                                                        {{ $errors->first('project_id') }}
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group hide" id="form-group-role">
                                        <div class="row">
                                            <label class="col-auto control-label">Roles</label>
                                            <div class="col-xs-10 col-sm-9 col-md-10">
                                                @foreach($roles as $role)
                                                    <div class="col-xs-4 col-sm-4 col-md-4">
                                                        <div class="checkbox checkbox-info font-gray-dark">
                                                            <label>
                                                                <input type="checkbox" @if($role->slug == 'admin'){{ "disabled='disabled'" }}@endif id="{{ $role->slug }}" class="custom-checkbox" name="roles[]" value="{{ $role->id }}">
                                                                {{ $role->title }}
                                                            </label>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>

                                <div class="form-group hide" id="form-group-trello">
                                    <div class="row">
                                        <label class="col-auto control-label">Trello Username</label>
                                        <div class="col-xs-10 col-sm-9 col-md-4">
                                            <input type="text" class="form-control {{ $errors->has('trello_username') ? ' parsley-error' : '' }}" id="" placeholder="e.g. samy46" name="trello_username" value="{{ old('trello_username') }}">
                                            @if ($errors->has('trello_username'))
                                                <span class="help-block">
                                                        {{ $errors->first('trello_username') }}
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-auto control-label">First name *</label>
                                            <div class="col-xs-10 col-sm-9 col-md-4">
                                                <input type="text" class="form-control {{ $errors->has('firstname') ? ' parsley-error' : '' }}" id="" placeholder="" name="firstname" value="{{ old('firstname') }}">
                                                @if ($errors->has('firstname'))
                                                    <span class="help-block">
                                                        {{ $errors->first('firstname') }}
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-auto control-label">Last name *</label>
                                            <div class="col-xs-10 col-sm-9 col-md-4">
                                                <input type="text" class="form-control {{ $errors->has('lastname') ? ' parsley-error' : '' }}" id="" placeholder="" name="lastname" value="{{ old('lastname') }}">
                                                @if ($errors->has('lastname'))
                                                    <span class="help-block">
                                                        {{ $errors->first('lastname') }}
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-auto control-label">E-mail *</label>
                                            <div class="col-xs-10 col-sm-9 col-md-4">
                                                <input type="email" class="form-control {{ $errors->has('email') ? ' parsley-error' : '' }}" id="" placeholder="" name="email" value="{{ old('email') }}">
                                                @if ($errors->has('email'))
                                                    <span class="help-block">
                                                        {{ $errors->first('email') }}
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-auto control-label">Password *</label>
                                            <div class="col-xs-10 col-sm-9 col-md-4">
                                                <input type="password" class="form-control {{ $errors->has('password') ? ' parsley-error' : '' }}" id="" placeholder="" name="password" value="{{ old('password') }}">
                                                @if ($errors->has('password'))
                                                    <span class="help-block">
                                                        {{ $errors->first('password') }}
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="row flex">
                                            <label class="col-auto control-label">Password <br/>confirmation *</label>
                                            <div class="col-xs-10 col-sm-9 col-md-4">
                                                <input type="password" class="form-control {{ $errors->has('password_confirmation') ? ' parsley-error' : '' }}" id="" placeholder="" name="password_confirmation" value="{{ old('password_confirmation') }}">
                                                @if ($errors->has('password_confirmation'))
                                                    <span class="help-block">
                                                        {{ $errors->first('password_confirmation') }}
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-auto control-label">File</label>
                                            <div class="col-xs-10 col-sm-9 col-md-4 mrg10T">
                                                <input type="file" name="file">
                                                @if ($errors->has('file'))
                                                    <span class="help-block">
                                                    {{ $errors->first('file') }}
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    <div class="button-pane mrg20T">
                                        <button type="submit" class="btn btn-info">Add people</button>
                                    </div>
                            </form>

                        </div>

                    </div>
                </div>
            </div>
    </div>
@stop
