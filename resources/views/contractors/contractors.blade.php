@extends('template')
@section('body')

    @include('breadcrumbs', array('breadcrumb_title' => 'Contractors', 'breadcrumb_list' => [['Dashboard','/'], ['Contractors','']]) )

    @if(session()->has('status'))
        <div class="alert alert-success flex mrg25B">
            <div class="bg-green alert-icon">
                <i class="glyph-icon icon-check"></i>
            </div>
            <div class="alert-content">
                <h4 class="alert-title">{{ session('status') }}</h4>
            </div>
        </div>
    @endif

    <div class="panel">
        <div class="panel-body">
            <h3 class="title-hero">
                Contractors
            </h3>
            <div class="example-box-wrapper">
                <div id="datatable-reorder_wrapper" class="dataTables_wrapper form-inline">
                    <div id="datatable-reorder_filter" class="dataTables_filter" style="display:block;float: left">
                        <label>
                            <input type="search" class="form-control" placeholder="Search..." aria-controls="datatable-reorder">
                        </label>
                    </div>
                    <table id="datatable-reorder" class="table table-striped table-bordered dataTable" cellspacing="0" role="grid" aria-describedby="datatable-reorder_info" style="width: 100%;">
                        <thead>
                            <tr role="row">
                                <th>Name</th>
                                <th>E-mail</th>
                                <th class="width-350">Action</th>
                            </tr>
                        </thead>

                        <tbody>
                            <?php $i = 0; ?>
                            @foreach($contractors as $user)
                                <tr role="row" class="{{ $i%2 == 0 ? 'odd' : 'even' }}">
                                    <td class="width-350"><a class="flex" href="/contractors/{{ $user->slug }}"><img class="img-circle" width="40" height="40" src="{{ $user->picture }}" alt="{{ $user->firstname }} {{ $user->lastname }}">&nbsp;&nbsp;{{ $user->firstname }} {{ $user->lastname }}</a></td>
                                    <td class="width-350">{{ $user->email }}</td>
                                    <td class="text-center">
                                        <a href="/contractors/{{ $user->slug }}" class="btn btn-info"><i class="glyph-icon icon-eye"></i>&nbsp;View profile<div class="ripple-wrapper"></div></a>
                                        @if(\Auth::user()->isAccountManager() || \Auth::user()->isProjectManager() )
                                            <a href="/contractors/{{ $user->slug }}/edit" class="btn btn-success"><i class="glyph-icon icon-edit"></i>&nbsp;Edit<div class="ripple-wrapper"></div></a>
                                            <button data-toggle="modal" data-target="#modal{{ $user->id }}" class="btn btn-danger"><i class="glyph-icon icon-elusive-cancel-circled"></i>&nbsp;Delete<div class="ripple-wrapper"></div></button>
                                            <div class="modal fade" id="modal{{ $user->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                            <h4 class="modal-title">Delete people</h4>
                                                        </div>
                                                        <div class="modal-body text-left">
                                                            <p>Are you sure you want to delete {{ $user->firstname }} {{ $user->lastname }}?</p>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                            <a href="/contractors/{{ $user->slug }}/delete" class="btn btn-danger">Delete</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop
