@extends('template')

@section('body')

    @include('breadcrumbs', array('breadcrumb_title' => 'Profile', 'breadcrumb_list' => [['Dashboard','/'], ['Contractors','/contractors/'.$user->name], ['View profile', '']]) )

    @if(session()->has('status'))
        <div class="alert alert-success flex mrg25B">
            <div class="bg-green alert-icon">
                <i class="glyph-icon icon-check"></i>
            </div>
            <div class="alert-content">
                <h4 class="alert-title">{{ session('status') }}</h4>
            </div>
        </div>
    @endif

    <input type="hidden" id="slug" value="{{ $user->slug }}">


    @if(\Auth::user()->isAccountManager() || \Auth::user()->isProjectManager() )
        <div class="row">
            <div class="col-md-12">
                <button data-toggle="modal" data-target="#modal-user-{{ $user->id }}" class="btn btn-danger float-right mrg25B mrg20L"><i class="glyph-icon icon-elusive-cancel-circled"></i>&nbsp;Delete<div class="ripple-wrapper"></div></button>
                <div class="modal fade" id="modal-user-{{ $user->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h4 class="modal-title">Delete user</h4>
                            </div>
                            <div class="modal-body">
                                <p>Are you sure that you want to delete the user {{ $user->firstname }} {{ $user->lastname }}?</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <a href="/contractors/{{ $user->slug }}/delete" class="btn btn-danger">Delete</a>
                            </div>
                        </div>
                    </div>
                </div>
                <a href="/contractors/{{ $user->slug }}/edit" class="btn btn-success float-right mrg25B"><i class="glyph-icon icon-edit"></i>&nbsp;Edit<div class="ripple-wrapper"></div></a>
            </div>
        </div>
    @endif
    <div class="row">
        <div class="col-md-12">
            <div class="example-box-wrapper">

                <div class="row">
                    <div class="col-md-4">
                        <div class="panel-layout">
                            <div class="panel-box">
                                <div class="panel-content image-box">
                                    <div class="ribbon">
                                        <div class="bg-purple">Contractor</div>
                                    </div>
                                    <div class="image-content font-white">
                                        <div class="meta-box">
                                            <img src="{{ $user->picture }}" alt="" class="meta-image img-bordered img-circle">
                                            <h3 class="meta-heading">{{ $user->firstname }} {{ $user->lastname }}</h3>
                                            <p class="mrg15T">{{ $user->email }}</p>
                                        </div>
                                    </div>
                                    <img src="/assets/image-resources/blurred-bg/blurred-bg-13.jpg" alt="">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-8">
                        <div class="row">
                            <div class="content-box mrg15B">
                                <h3 class="content-box-header clearfix">
                                    <i class="glyph-icon icon-elusive-desktop"></i>
                                    Projects
                                </h3>
                                <div class="content-box-wrapper pad0T clearfix">
                                    <table class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Company</th>
                                            <th>Website</th>
                                            <th>Status</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($user->projects as $project)
                                            <tr>
                                                <td><a href="{{ url('/projects/'.$project->slug) }}">{{ $project->name }}</a></td>
                                                <td>{{ $project->company }}</td>
                                                <td><a href="{{ $project->website_url }}">{{ $project->website_url }}</a></td>
                                                <td>
                                                    @if($project->status == 'ongoing')
                                                        <a class="btn btn-azure disabled" href="#" role="button">{{ $project->status }}</a>
                                                    @elseif($project->status == 'retained')
                                                        <a class="btn btn-yellow disabled" href="#" role="button">{{ $project->status }}</a>
                                                    @else
                                                        <a class="btn btn-black disabled" href="#" role="button">{{ $project->status }}</a>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>

                                </div>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>


@stop
