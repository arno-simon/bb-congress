<div id="page-sidebar">
    <div id="header-logo" class="logo-bg">
        <a href="{{ url('/') }}" class="logo-content-big" title="BB-Congress">
            <img src="/assets/images/bb-logo.png" alt="bb-logo">
            BB-Congress
        </a>
    </div>
    <div class="scroll-sidebar">
        <ul id="sidebar-menu" class="sf-js-enabled sf-arrows">
            <li class="header"><span>Projects</span></li>

            @if(\Auth::user()->isProjectManager())
                <li id="/projects/new"><a href="{{ url('/projects/new') }}" title="Add a new project"><i class="glyph-icon icon-plus"></i><span>Add New</span></a></li>
                <li id="/projects"><a href="{{ url('/projects') }}" title="Projects"><i class="glyph-icon icon-elusive-desktop"></i><span>Projects</span></a></li>

            @else
                <li id="/projects"><a href="{{ url('/projects') }}" title="Projects"><i class="glyph-icon icon-elusive-desktop"></i><span>Projects</span></a></li>
            @endif

            <li class="header"><span>People</span></li>

            @if(\Auth::user()->isAccountManager())
                <li id="baboons">
                    <a href="javascript:void(0);" title="Baboons" class="sf-with-ul">
                        <i class="glyph-icon icon-paw"></i>
                        <span>Baboons</span>
                    </a>
                    <div class="sidebar-submenu hide">
                        <ul>
                            <li id="new"><a href="{{ url('/baboons/new') }}" title="Add new"><span>Add new</span></a></li>
                            <li id=""><a href="{{ url('/baboons') }}" title="Baboons"><span>Baboons</span></a></li>
                        </ul>
                    </div><!-- .sidebar-submenu -->
                </li>
            @else
                <li id="baboons"><a href="{{ url('/baboons') }}" title="Baboons"><i class="glyph-icon icon-paw"></i><span>Baboons</span></a></li>
            @endif

            @if(\Auth::user()->isProjectManager() || \Auth::user()->isAccountManager())
                <li id="clients">
                    <a href="javascript:void(0);" title="Clients" class="sf-with-ul">
                        <i class="glyph-icon icon-elusive-group"></i>
                        <span>Clients</span>
                    </a>
                    <div class="sidebar-submenu hide">
                        <ul>
                            <li id="new"><a href="{{ url('/clients/new') }}" title="Add new"><span>Add new</span></a></li>
                            <li id=""><a href="{{ url('/clients') }}" title="Clients"><span>Clients</span></a></li>
                        </ul>
                    </div><!-- .sidebar-submenu -->
                </li>
                <li id="contractors">
                    <a href="javascript:void(0);" title="Contractors" class="sf-with-ul">
                        <i class="glyph-icon icon-share-square-o"></i>
                        <span>Contractors</span>
                    </a>
                    <div class="sidebar-submenu hide">
                        <ul>
                            <li id="new"><a href="{{ url('/contractors/new') }}" title="Add new"><span>Add new</span></a></li>
                            <li id=""><a href="{{ url('/contractors') }}" title="Contractors"><span>Contractors</span></a></li>
                        </ul>
                    </div><!-- .sidebar-submenu -->
                </li>
            @else
                <li id="clients"><a href="{{ url('/clients') }}" title="Clients"><i class="glyph-icon icon-elusive-group"></i><span>Clients</span></a></li>
                <li id="contractors"><a href="{{ url('/contractors') }}" title="Contractors"><i class="glyph-icon icon-share-square-o"></i><span>Contractors</span></a></li>
            @endif

            {{--<li class="header"><span>Bugs</span></li>--}}
            {{--<li id="bugs"><a href="{{ url('/bug/new') }}" title="Report a bug" class="disabled"><i class="glyph-icon icon-bug"></i><span>Report a bug</span></a></li>--}}
        </ul><!-- #sidebar-menu -->
    </div>
</div>
