@extends('auth.template')

@section('body')

<div id="loading">
    <div class="svg-icon-loader">
        <img src="/assets/images/svg-loaders/bars.svg" width="40" alt="">
    </div>
</div>

<style type="text/css">

    html,body {
        height: 100%;
    }

</style>
<div class="center-vertical bg-black">
    <div class="center-content">
        <form class="col-md-5 col-sm-5 col-xs-11 center-margin" method="POST" action="{{ url('/password/reset') }}">
            {!! csrf_field() !!}
            <input type="hidden" name="token" value="{{ $token }}">
            <h3 class="text-center pad25B font-gray font-size-23">BB Congress</h3>
            <div id="login-form" class="content-box">
                <div class="content-box-wrapper pad20A">

                    <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                        <div class="input-group input-group-lg">
                            <span class="input-group-addon addon-inside bg-white font-primary">
                                <i class="glyph-icon icon-envelope-o"></i>
                            </span>
                            <input type="email" class="form-control {{ $errors->has('email') ? ' parsley-error' : '' }}" id="exampleInputEmail1" placeholder="Enter email" value="{{ old('email') }}" name="email">
                            @if ($errors->has('email'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                        <div class="input-group input-group-lg">
                            <span class="input-group-addon addon-inside bg-white font-primary">
                                <i class="glyph-icon icon-unlock-alt"></i>
                            </span>
                            <input type="password" class="form-control {{ $errors->has('password') ? ' parsley-error' : '' }}" id="exampleInputPassword1" placeholder="Password" name="password">
                            @if ($errors->has('password'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                        <div class="input-group input-group-lg">
                            <span class="input-group-addon addon-inside bg-white font-primary">
                                <i class="glyph-icon icon-unlock-alt"></i>
                            </span>
                            <input type="password" class="form-control {{ $errors->has('password') ? ' parsley-error' : '' }}" id="exampleInputPassword1" placeholder="Password confirmation" name="password_confirmation">
                        </div>
                    </div>

                </div>
                <div class="button-pane">
                    <button type="submit" class="btn btn-block btn-primary">Change password</button>
                </div>
            </div>

        </form>

    </div>
</div>
@endsection

