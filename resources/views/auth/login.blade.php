@extends('auth.template')

@section('body')



<div id="loading">
    <div class="svg-icon-loader">
        <img src="/assets/images/svg-loaders/bars.svg" width="40" alt="">
    </div>
</div>

<style type="text/css">

    html,body {
        height: 100%;
    }

</style>
<div class="center-vertical bg-black">
    <div class="center-content">
        <form id="login-validation" class="col-md-5 col-sm-5 col-xs-11 center-margin" method="POST" action="{{ url('/login') }}">
            {!! csrf_field() !!}
            <h3 class="text-center pad25B font-gray font-size-23">BB Congress</h3>
            <div id="login-form" class="content-box">
                <div class="content-box-wrapper pad20A">
                    <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                        <div class="input-group input-group-lg">
                            <span class="input-group-addon addon-inside bg-white font-primary">
                                <i class="glyph-icon icon-envelope-o"></i>
                            </span>
                            <input type="email" class="form-control {{ $errors->has('email') ? ' parsley-error' : '' }}" id="exampleInputEmail1" placeholder="Enter email" value="{{ old('email') }}" name="email">
                            @if ($errors->has('email'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                        <div class="input-group input-group-lg">
                            <span class="input-group-addon addon-inside bg-white font-primary">
                                <i class="glyph-icon icon-unlock-alt"></i>
                            </span>
                            <input type="password" class="form-control {{ $errors->has('password') ? ' parsley-error' : '' }}" id="exampleInputPassword1" placeholder="Password" name="password">
                            @if ($errors->has('password'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="checkbox checkbox-info col-xs-6 col-sm-6 col-md-6 font-gray-dark">
                            <label>
                                <input type="checkbox" id="inlineCheckbox114" class="custom-checkbox" name="remember">
                                Remember me
                            </label>
                        </div>
                        <div class="text-right col-xs-6 col-sm-6 col-md-6">
                            <a href="{{ url('/password/email') }}" title="Recover password">Forgot your password?</a>
                        </div>
                    </div>
                </div>
                <div class="button-pane">
                    <button type="submit" class="btn btn-block btn-primary">Login</button>
                </div>
            </div>

        </form>

    </div>
</div>
@endsection

