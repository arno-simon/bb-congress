@extends('auth.template')

@section('body')

    <div id="loading">
        <div class="svg-icon-loader">
            <img src="/assets/images/svg-loaders/bars.svg" width="40" alt="">
        </div>
    </div>

    <style type="text/css">

        html,body {
            height: 100%;
        }
    </style>

    <div class="center-vertical bg-black">
        <div class="center-content">
                <form class="col-md-5 col-sm-5 col-xs-11 center-margin" method="POST" action="{{ url('/password/email') }}">
                    {!! csrf_field() !!}
                    <h3 class="text-center pad25B font-gray font-size-23">BB Congress</h3>
                    @if(session()->has('status'))
                        <div class="alert alert-success mrg20T mrg20B">
                            <div class="bg-green alert-icon">
                                <i class="glyph-icon icon-check"></i>
                            </div>
                            <div class="alert-content">
                                <h4 class="alert-title">Email sent successfully</h4>
                                <p>{{ session()->get('status') }}</p>
                            </div>
                        </div>
                    @endif
                    <div class="content-box">
                        <div class="content-box-wrapper pad20A">
                            <h3 class="text-center pad25B font-black">Reset your password</h3>

                            <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                <div class="input-group input-group-lg">
                                <span class="input-group-addon addon-inside bg-white font-primary">
                                    <i class="glyph-icon icon-envelope-o"></i>
                                </span>
                                    <input type="email" class="form-control {{ $errors->has('email') ? ' parsley-error' : '' }}" id="exampleInputEmail1" placeholder="Enter your email" value="{{ old('email') }}" name="email">
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="button-pane">
                            <button type="submit" class="btn btn-block btn-primary">Reset</button>
                        </div>
                    </div>

                </form>

        </div>
    </div>
@stop
