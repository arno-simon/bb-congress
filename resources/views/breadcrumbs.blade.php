<div id="page-title">
    <div class="row">
        <div class="col-xs-4 col-sm-4 col-md-4 flex col-title">
            <h2>{{$breadcrumb_title}}</h2>
        </div>
        <div class="col-xs-8 col-sm-8 col-md-8 col-links">
            <ol class="breadcrumb pull-right">
                @foreach($breadcrumb_list as $item)
                    @if($item == end($breadcrumb_list))
                        <li class="active">
                            {{$item[0]}}
                        </li>
                    @else
                        <li>
                            <a href="{{ url($item[1]) }}">
                                {{$item[0]}}
                            </a>
                        </li>
                    @endif
                @endforeach
            </ol>
        </div>
    </div>

</div>