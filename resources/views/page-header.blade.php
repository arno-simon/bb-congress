<div id="page-header">

    <div class="dropdown" id="notifications-btn">
        <a data-toggle="dropdown" href="#" title="">
            <span id="count-notifications" class="bs-badge badge-danger"></span>
            <i class="glyph-icon icon-bell font-size-20"></i>
        </a>
        <div class="dropdown-menu box-md float-left">

            <div class="popover-title display-block clearfix pad10A">
                Notifications
            </div>
            <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto;">
                <div class="scrollable-content scrollable-slim-box" style="overflow: hidden; width: auto;height: auto;">
                    <ul class="no-border notifications-box">
                        @foreach(\Auth::user()->latestNotifications() as $notification)
                            <li class="flex">
                                <div class="col-md-2">
                                    <span class="bg-blue icon-notification glyph-icon icon-info"></span>
                                </div>
                                <div class="width-100">
                                    <span class="notification-text">{{ $notification['content'] }}</span>
                                </div>
                                <div class="notification-time">
                                    {{ \Carbon\Carbon::parse($notification['created_at'])->diffForHumans() }}
                                    <span class="glyph-icon icon-clock-o"></span>
                                </div>

                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
            <div class="button-pane button-pane-alt pad5T pad5L pad5R text-center">
                <a class="text-transform-cap font-primary font-normal btn-link" href="{{ url('/notifications') }}" title="View more options">
                    View all notifications
                </a>
            </div>
        </div>
    </div>

    <div class="searchbar">
        <div id="typeahead">
            <div class="input-group">
                <input id="search" class="typeahead form-control autocomplete-input ui-autocomplete-input" type="text" placeholder="Search for projects, people...">
            </div>
        </div>
    </div>



    <div id="header-nav-left">
        <div class="user-account-btn dropdown">
            <a href="#" title="My Account" class="user-profile clearfix flex" data-toggle="dropdown">
                <img src="{{ Auth::user()->picture }}" alt="Profile image">
                <span>{{ Auth::user()->firstname }}</span>
                <i class="glyph-icon icon-angle-down"></i>
            </a>

            <div class="dropdown-menu float-right">
                <div class="box-sm">
                    <ul class="reset-ul mrg5B">
                        <li>
                            <a href="{{ url('account') }}">
                                <i class="glyph-icon icon-cog"></i> My account
                            </a>
                        </li>
                    </ul>
                    <div class="button-pane button-pane-alt pad5L pad5R text-center">
                        <a href="{{ url('/auth/logout') }}" class="btn btn-flat display-block font-normal btn-danger">
                            <i class="glyph-icon icon-power-off"></i>
                            Logout
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
