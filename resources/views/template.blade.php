<!DOCTYPE html>
<html lang="en">
<head>

    <style>
        #loading .svg-icon-loader {position: absolute;top: 50%;left: 50%;margin: -50px 0 0 -50px;}
    </style>


    <meta charset="UTF-8">
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->

    <title>BB-Congress</title>

    @yield('header')
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- Favicons -->
    <link href="/assets/images/icons/favicon-012.png" rel="shortcut icon" />

    <!-- Custom CSS -->
    <link rel="stylesheet" type="text/css" href="/css/custom.css">
    <link href="/css/autocomplete.css" rel="stylesheet">
    <link href="/css/nouislider.css" rel="stylesheet">

    <!-- HELPERS -->

    <link rel="stylesheet" type="text/css" href="/assets/helpers/animate.css">
    <link rel="stylesheet" type="text/css" href="/assets/helpers/boilerplate.css">
    <link rel="stylesheet" type="text/css" href="/assets/helpers/border-radius.css">
    <link rel="stylesheet" type="text/css" href="/assets/helpers/grid.css">
    <link rel="stylesheet" type="text/css" href="/assets/helpers/page-transitions.css">
    <link rel="stylesheet" type="text/css" href="/assets/helpers/spacing.css">
    <link rel="stylesheet" type="text/css" href="/assets/helpers/typography.css">
    <link rel="stylesheet" type="text/css" href="/assets/helpers/utils.css">
    <link rel="stylesheet" type="text/css" href="/assets/helpers/colors.css">

    <!-- MATERIAL -->

    <link rel="stylesheet" type="text/css" href="/assets/material/ripple.css">

    <!-- ELEMENTS -->

    <link rel="stylesheet" type="text/css" href="/assets/elements/badges.css">
    <link rel="stylesheet" type="text/css" href="/assets/elements/buttons.css">
    <link rel="stylesheet" type="text/css" href="/assets/elements/content-box.css">
    <link rel="stylesheet" type="text/css" href="/assets/elements/dashboard-box.css">
    <link rel="stylesheet" type="text/css" href="/assets/elements/forms.css">
    <link rel="stylesheet" type="text/css" href="/assets/elements/images.css">
    <link rel="stylesheet" type="text/css" href="/assets/elements/info-box.css">
    <link rel="stylesheet" type="text/css" href="/assets/elements/invoice.css">
    <link rel="stylesheet" type="text/css" href="/assets/elements/loading-indicators.css">
    <link rel="stylesheet" type="text/css" href="/assets/elements/menus.css">
    <link rel="stylesheet" type="text/css" href="/assets/elements/panel-box.css">
    <link rel="stylesheet" type="text/css" href="/assets/elements/response-messages.css">
    <link rel="stylesheet" type="text/css" href="/assets/elements/responsive-tables.css">
    <link rel="stylesheet" type="text/css" href="/assets/elements/ribbon.css">
    <link rel="stylesheet" type="text/css" href="/assets/elements/social-box.css">
    <link rel="stylesheet" type="text/css" href="/assets/elements/tables.css">
    <link rel="stylesheet" type="text/css" href="/assets/elements/tile-box.css">
    <link rel="stylesheet" type="text/css" href="/assets/elements/timeline.css">

    <!-- ICONS -->

    <link rel="stylesheet" type="text/css" href="/assets/icons/fontawesome/fontawesome.css">
    <link rel="stylesheet" type="text/css" href="/assets/icons/linecons/linecons.css">
    <link rel="stylesheet" type="text/css" href="/assets/icons/spinnericon/spinnericon.css">
    <link rel="stylesheet" type="text/css" href="/assets/icons/elusive/elusive.css">
    <link rel="stylesheet" type="text/css" href="/assets/icons/iconic/iconic.css">
    <link rel="stylesheet" type="text/css" href="/assets/icons/typicons/typicons.css">


    <!-- WIDGETS -->

    <link rel="stylesheet" type="text/css" href="/assets/widgets/accordion-ui/accordion.css">
    <link rel="stylesheet" type="text/css" href="/assets/widgets/calendar/calendar.css">
    <link rel="stylesheet" type="text/css" href="/assets/widgets/carousel/carousel.css">

    <link rel="stylesheet" type="text/css" href="/assets/widgets/charts/justgage/justgage.css">
    <link rel="stylesheet" type="text/css" href="/assets/widgets/charts/morris/morris.css">
    <link rel="stylesheet" type="text/css" href="/assets/widgets/charts/piegage/piegage.css">
    <link rel="stylesheet" type="text/css" href="/assets/widgets/charts/xcharts/xcharts.css">

    <link rel="stylesheet" type="text/css" href="/assets/widgets/chosen/chosen.css">
    <link rel="stylesheet" type="text/css" href="/assets/widgets/colorpicker/colorpicker.css">
    <link rel="stylesheet" type="text/css" href="/assets/widgets/datatable/datatable.css">
    <link rel="stylesheet" type="text/css" href="/assets/widgets/datepicker/datepicker.css">
    <link rel="stylesheet" type="text/css" href="/assets/widgets/datepicker-ui/datepicker.css">
    <link rel="stylesheet" type="text/css" href="/assets/widgets/daterangepicker/daterangepicker.css">
    <link rel="stylesheet" type="text/css" href="/assets/widgets/dialog/dialog.css">
    <link rel="stylesheet" type="text/css" href="/assets/widgets/dropdown/dropdown.css">
    <link rel="stylesheet" type="text/css" href="/assets/widgets/dropzone/dropzone.css">
    <link rel="stylesheet" type="text/css" href="/assets/widgets/file-input/fileinput.css">
    <link rel="stylesheet" type="text/css" href="/assets/widgets/input-switch/inputswitch.css">
    <link rel="stylesheet" type="text/css" href="/assets/widgets/input-switch/inputswitch-alt.css">
    <link rel="stylesheet" type="text/css" href="/assets/widgets/ionrangeslider/ionrangeslider.css">
    <link rel="stylesheet" type="text/css" href="/assets/widgets/jcrop/jcrop.css">
    <link rel="stylesheet" type="text/css" href="/assets/widgets/jgrowl-notifications/jgrowl.css">
    <link rel="stylesheet" type="text/css" href="/assets/widgets/loading-bar/loadingbar.css">
    <link rel="stylesheet" type="text/css" href="/assets/widgets/maps/vector-maps/vectormaps.css">
    <link rel="stylesheet" type="text/css" href="/assets/widgets/markdown/markdown.css">
    <link rel="stylesheet" type="text/css" href="/assets/widgets/modal/modal.css">
    <link rel="stylesheet" type="text/css" href="/assets/widgets/multi-select/multiselect.css">
    <link rel="stylesheet" type="text/css" href="/assets/widgets/multi-upload/fileupload.css">
    <link rel="stylesheet" type="text/css" href="/assets/widgets/nestable/nestable.css">
    <link rel="stylesheet" type="text/css" href="/assets/widgets/noty-notifications/noty.css">
    <link rel="stylesheet" type="text/css" href="/assets/widgets/popover/popover.css">
    <link rel="stylesheet" type="text/css" href="/assets/widgets/pretty-photo/prettyphoto.css">
    <link rel="stylesheet" type="text/css" href="/assets/widgets/progressbar/progressbar.css">
    <link rel="stylesheet" type="text/css" href="/assets/widgets/range-slider/rangeslider.css">
    <link rel="stylesheet" type="text/css" href="/assets/widgets/slider-ui/slider.css">
    <link rel="stylesheet" type="text/css" href="/assets/widgets/summernote-wysiwyg/summernote-wysiwyg.css">
    <link rel="stylesheet" type="text/css" href="/assets/widgets/tabs-ui/tabs.css">
    <link rel="stylesheet" type="text/css" href="/assets/widgets/timepicker/timepicker.css">
    <link rel="stylesheet" type="text/css" href="/assets/widgets/tocify/tocify.css">
    <link rel="stylesheet" type="text/css" href="/assets/widgets/tooltip/tooltip.css">
    <link rel="stylesheet" type="text/css" href="/assets/widgets/touchspin/touchspin.css">
    <link rel="stylesheet" type="text/css" href="/assets/widgets/uniform/uniform.css">
    <link rel="stylesheet" type="text/css" href="/assets/widgets/wizard/wizard.css">
    <link rel="stylesheet" type="text/css" href="/assets/widgets/xeditable/xeditable.css">

    <!-- SNIPPETS -->

    <link rel="stylesheet" type="text/css" href="/assets/snippets/chat.css">
    <link rel="stylesheet" type="text/css" href="/assets/snippets/files-box.css">
    <link rel="stylesheet" type="text/css" href="/assets/snippets/login-box.css">
    <link rel="stylesheet" type="text/css" href="/assets/snippets/notification-box.css">
    <link rel="stylesheet" type="text/css" href="/assets/snippets/progress-box.css">
    <link rel="stylesheet" type="text/css" href="/assets/snippets/todo.css">
    <link rel="stylesheet" type="text/css" href="/assets/snippets/user-profile.css">
    <link rel="stylesheet" type="text/css" href="/assets/snippets/mobile-navigation.css">


    <!-- Admin theme -->

    <link rel="stylesheet" type="text/css" href="/assets/themes/admin/layout.css">
    <link rel="stylesheet" type="text/css" href="/assets/themes/admin/color-schemes/default.css">

    <!-- Components theme -->

    <link rel="stylesheet" type="text/css" href="/assets/themes/components/default.css">
    <link rel="stylesheet" type="text/css" href="/assets/themes/components/border-radius.css">

    <!-- Admin responsive -->

    <link rel="stylesheet" type="text/css" href="/assets/helpers/responsive-elements.css">
    <link rel="stylesheet" type="text/css" href="/assets/helpers/admin-responsive.css">

    <!-- JS Core -->
    <script type="text/javascript" src="/assets/js-core/jquery-core.js"></script>
    <script type="text/javascript" src="/assets/js-core/jquery-ui-core.js"></script>
    <script type="text/javascript" src="/assets/js-core/jquery-ui-widget.js"></script>
    <script type="text/javascript" src="/assets/js-core/jquery-ui-mouse.js"></script>
    <script type="text/javascript" src="/assets/js-core/jquery-ui-position.js"></script>
    <script type="text/javascript" src="/assets/js-core/transition.js"></script>
    <script type="text/javascript" src="/assets/js-core/modernizr.js"></script>
    <script type="text/javascript" src="/assets/js-core/jquery-cookie.js"></script>

    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/typeahead.js/0.11.1/typeahead.bundle.min.js"></script>
    <script type="text/javascript" src="/js/nouislider.min.js"></script>
    <script src="//cdn.socket.io/socket.io-1.4.5.js"></script>

    <!-- Custom JS -->
    <script type="text/javascript" src="/js/custom.js"></script>

    <!-- Trello -->
    <script src="https://api.trello.com/1/client.js?key=176ccfbd6ab7543ba474a6a765b179e7"></script>
    <script type="text/javascript" src="/js/trello.js"></script>

    <script type="text/javascript">
        $(window).load(function(){
            setTimeout(function() {
                $('#loading').fadeOut( 400, "linear" );
            }, 300);
        });
    </script>
</head>
<body>
<meta name="csrf-token" content="{{ csrf_token() }}">
<div id="sb-site">
    <div id="loading">
        <div class="svg-icon-loader">
            <img src="/assets/images/svg-loaders/bars.svg" width="40" alt="">
        </div>
    </div>
    <div id="page-wrapper">
        <div id="mobile-navigation">
            <button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button>
        </div>
        @include('side-menu')

        <div id="page-content-wrapper">
            <div id="page-content">
                @include('page-header')

                @yield('body')
            </div>
        </div>
    </div>
</div>



<!-- TYPE AHEAD - BLOODHOUND -->
<script>
    $(document).ready(function () {

        // Search

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: "GET",
            url: "/search/autocomplete/results",
            contentType: "application/json; charset=UTF-8"
        })
        .done(function(results) {
            var $search = $('#search');

            var substringMatcher = function(strs) {
                return function findMatches(q, cb) {
                    var matches, substringRegex;

                    // an array that will be populated with substring matches
                    matches = [];

                    // regex used to determine if a string contains the substring `q`
                    substrRegex = new RegExp(q, 'i');

                    // iterate through the pool of strings and for any string that
                    // contains the substring `q`, add it to the `matches` array
                    $.each(strs, function(i, str) {
                        if (substrRegex.test(str.name)) {
                            matches.push(str);
                        }
                    });

                    cb(matches);
                };
            };


            $search.typeahead({
                hint: false,
                highlight: true,
                minLength: 1
            },
            {
                name: 'results',
                source: substringMatcher(results),
                display: function(x){
                    return x.name;
                }
            })
            .bind('typeahead:select', function(ev, suggestion) {
                window.location.replace(suggestion.url);
            });
            var spanIcon = "<span id='icon-search' class='input-group-addon addon-inside bg-black'> <i class='glyph-icon icon-search'></i> </span>";
            $search.before(spanIcon);

        });


        {{--@if(\Auth::user()->isAdmin() || \Auth::user()->isProjectManager())--}}
            {{--// Notifications--}}
            {{--var socket = io('http://127.0.0.1:3000',  {'reconnection' : false});--}}
            {{--//var socket = io('http://192.168.10.10:3000', {'reconnection' : false});--}}
            {{--socket.on('period-channel:App\\Events\\PeriodUpdate', function(message){--}}

                {{--var time = moment(message.notification.created_at).from(moment().add(5, 's'));--}}
                {{--var li = "<li class='flex'> " +--}}
                            {{--"<div class='col-md-2'>" +--}}
                                {{--"<span class='bg-blue icon-notification glyph-icon icon-info'></span> " +--}}
                            {{--"</div>" +--}}
                            {{--"<div class='width-100'>" +--}}
                                {{--"<span class='notification-text'>"+message.notification.content+"</span> " +--}}
                            {{--"</div>" +--}}
                            {{--"<div class='notification-time'>" +--}}
                                {{--time + " "+--}}
                                {{--"<span class='glyph-icon icon-clock-o'></span> " +--}}
                            {{--"</div> " +--}}
                        {{--"</li>";--}}
                {{--$('.notifications-box').prepend(li);--}}
                {{--var nbNotifications = $('.notifications-box li').length,--}}
                    {{--$countNotifications = $('#count-notifications');--}}
                {{--// Remove oldest notif if nbNotif > 5--}}
                {{--if(nbNotifications > 5) $('.notifications-box li:last-child').remove();--}}

                {{--isNaN(parseInt($countNotifications.html())) ? nbNewNotifications = 0 : nbNewNotifications = parseInt($countNotifications.html());--}}
                {{--$countNotifications.html(++nbNewNotifications);--}}

            {{--});--}}
        {{--@endif--}}

    });
</script>



        <!-- WIDGETS -->

<script type="text/javascript" src="/assets/widgets/dropdown/dropdown.js">
</script><script type="text/javascript" src="/assets/widgets/tooltip/tooltip.js"></script>
<script type="text/javascript" src="/assets/widgets/popover/popover.js"></script>
<script type="text/javascript" src="/assets/widgets/progressbar/progressbar.js"></script>
<script type="text/javascript" src="/assets/widgets/button/button.js"></script>
<script type="text/javascript" src="/assets/widgets/collapse/collapse.js"></script>
<script type="text/javascript" src="/assets/widgets/input-switch/inputswitch-alt.js"></script>
<script type="text/javascript" src="/assets/widgets/slimscroll/slimscroll.js"></script>
<script type="text/javascript" src="/assets/widgets/slidebars/slidebars.js"></script>
<script type="text/javascript" src="/assets/widgets/slidebars/slidebars-demo.js"></script>
<script type="text/javascript" src="/assets/widgets/charts/piegage/piegage.js"></script>
<script type="text/javascript" src="/assets/widgets/charts/piegage/piegage-demo.js"></script>
<script type="text/javascript" src="/assets/widgets/screenfull/screenfull.js"></script>
<script type="text/javascript" src="/assets/widgets/content-box/contentbox.js"></script>
<script type="text/javascript" src="/assets/widgets/material/material.js"></script>
<script type="text/javascript" src="/assets/widgets/material/ripples.js"></script>
<script type="text/javascript" src="/assets/widgets/overlay/overlay.js"></script>
<script type="text/javascript" src="/assets/widgets/uniform/uniform.js"></script>
<script type="text/javascript" src="/assets/widgets/tabs/tabs.js"></script>
<script type="text/javascript" src="/assets/widgets/wow/wow.js"></script>
<script type="text/javascript" src="/assets/widgets/uniform/uniform-demo.js"></script>
<script type="text/javascript" src="/assets/widgets/accordion-ui/accordion.js"></script>
<script type="text/javascript" src="/assets/widgets/datatable/datatable.js"></script>
<script type="text/javascript" src="/assets/widgets/datatable/datatable-bootstrap.js"></script>
<script type="text/javascript" src="/assets/widgets/datatable/datatable-tabletools.js"></script>
<script type="text/javascript" src="/assets/widgets/datatable/datatable-reorder.js"></script>
<script type="text/javascript" src="/assets/widgets/overlay/overlay.js"></script>
<script type="text/javascript" src="/assets/widgets/multi-select/multiselect.js"></script>
<script type="text/javascript" src="/assets/widgets/chosen/chosen.js"></script>
<script type="text/javascript" src="/assets/widgets/chosen/chosen-demo.js"></script>
<script type="text/javascript" src="/assets/widgets/modal/modal.js"></script>
<script type="text/javascript" src="/assets/widgets/dropzone/dropzone.js"></script>
<script type="text/javascript" src="/assets/widgets/interactions-ui/resizable.js"></script>
<script type="text/javascript" src="/assets/widgets/interactions-ui/draggable.js"></script>
<script type="text/javascript" src="/assets/widgets/interactions-ui/sortable.js"></script>
<script type="text/javascript" src="/assets/widgets/interactions-ui/selectable.js"></script>
<script type="text/javascript" src="/assets/widgets/xeditable/xeditable.js"></script>
<script type="text/javascript" src="/assets/widgets/daterangepicker/moment.js"></script>
<script type="text/javascript" src="/assets/widgets/daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="/assets/widgets/datepicker/datepicker.js"></script>

<script type="text/javascript" src="/assets/js-init/widgets-init.js"></script>
@yield('script')

</body>
</html>
