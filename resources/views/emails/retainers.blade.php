<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>
<h2>{{ $last_period->project->name }} - {{ $last_period->project->retainerType->name }}</h2>

<h3>Last period from {{ date('F d, Y', strtotime($last_period->from)) }} to {{ date('F d, Y', strtotime($last_period->to)) }}</h3>
<p>Monthly time: {{ $last_period->monthly_time }}H</p>
<p>Time due: {{ $last_period->time_due }}H</p>
<p>Time spent: {{ $last_period->time_spent }}H</p>
<p>Time left: {{ $last_period->time_left }}H</p>
<br/>
<hr>
<br/>
<h3>New period from {{ date('F d, Y', strtotime($new_period->from)) }} to {{ date('F d, Y', strtotime($new_period->to)) }}</h3>
<p>Monthly time: {{ $last_period->monthly_time }}H</p>
<p>Time due: {{ $new_period->time_due }}H</p>
<p>Time spent: {{ $new_period->time_spent }}H</p>
<p>Time left: {{ $new_period->time_left }}H</p>

</body>
</html>
