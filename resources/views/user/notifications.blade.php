@extends('template')
@section('body')

    @include('breadcrumbs', array('breadcrumb_title' => 'Notifications', 'breadcrumb_list' => [['Dashboard','/'], ['Notifications','']]) )

    <div class="panel">
        <div class="panel-body">
            <h3 class="title-hero">
                Notifications
            </h3>
            <div class="example-box-wrapper">
                <div id="datatable-reorder_wrapper" class="dataTables_wrapper form-inline">

                    <table id="datatable-notification-reorder" class="table table-striped table-bordered dataTable" cellspacing="0" width="100%" role="grid" aria-describedby="datatable-reorder_info" style="width: 100%;">
                        <thead>
                            <tr role="row">
                                <th>Content</th>
                                <th class="width-240">Date</th>
                            </tr>
                        </thead>

                        <tbody>
                            <?php $i = 0; ?>
                            @foreach($notifications as $notification)
                                <tr role="row" class="{{ $i%2 == 0 ? 'odd' : 'even' }}">
                                    <td>{{ $notification['content'] }}</td>
                                    <td>{{ \Carbon\Carbon::parse($notification['created_at'])->diffForHumans() }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop
