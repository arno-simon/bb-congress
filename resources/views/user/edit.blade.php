@extends('template')
@section('body')

    @include('breadcrumbs', array('breadcrumb_title' => 'Edit baboon', 'breadcrumb_list' => [['Dashboard','/'], ['Baboons','/baboons/'.$user->name], ['Edit profile', '']]) )

    @if(session()->has('status'))
        <div class="alert alert-success flex mrg25B">
            <div class="bg-green alert-icon">
                <i class="glyph-icon icon-check"></i>
            </div>
            <div class="alert-content">
                <h4 class="alert-title">{{ session('status') }}</h4>
            </div>
        </div>
    @endif

    <div class="row">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                <div class="content-box mrg15B">
                    <h3 class="content-box-header clearfix">
                        <i class="glyph-icon icon-info-circle"></i>&nbsp;
                        Personal information
                    </h3>
                    <div class="content-box-wrapper pad0T clearfix">
                        <form id="add-new-people" class="form-horizontal pad15L pad15R bordered-row" method="POST"
                              action="{{ url('baboons/'.$user->slug.'/edit') }}">
                            {!! csrf_field() !!}
                            <input type="hidden" name="id" value="{{ $user->id }}">

                            <div class="form-group">
                                <div class="row">
                                    <label class="col-auto control-label">First name *</label>
                                    <div class="col-xs-10 col-sm-9 col-md-4">
                                        <input type="text"
                                               class="form-control {{ $errors->has('firstname') ? ' parsley-error' : '' }}"
                                               id="" placeholder="" name="firstname" value="{{ $user->firstname }}">
                                        @if ($errors->has('firstname'))
                                            <span class="help-block">
                                                        {{ $errors->first('firstname') }}
                                                    </span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <label class="col-auto control-label">Last name *</label>
                                    <div class="col-xs-10 col-sm-9 col-md-4">
                                        <input type="text"
                                               class="form-control {{ $errors->has('lastname') ? ' parsley-error' : '' }}"
                                               id="" placeholder="" name="lastname" value="{{ $user->lastname }}">
                                        @if ($errors->has('lastname'))
                                            <span class="help-block">
                                                        {{ $errors->first('lastname') }}
                                                    </span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <label class="col-auto control-label">E-mail *</label>
                                    <div class="col-xs-10 col-sm-9 col-md-4">
                                        <input type="email"
                                               class="form-control {{ $errors->has('email') ? ' parsley-error' : '' }}"
                                               id="" placeholder="" name="email" value="{{ $user->email }}">
                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                                        {{ $errors->first('email') }}
                                                    </span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <label class="col-auto control-label">Roles</label>
                                    <div class="col-xs-10 col-sm-9 col-md-10">
                                        @foreach($roles as $role)
                                            <div class="col-xs-4 col-sm-4 col-md-4">
                                                <div class="checkbox checkbox-info font-gray-dark">
                                                    <label>
                                                        <?php $checked = false; ?>
                                                        @foreach($user->roles as $role_user)
                                                            @if($role_user->id == $role->id)
                                                                <?php $checked = true; ?>
                                                                @break
                                                            @endif

                                                        @endforeach
                                                        <input type="checkbox"
                                                               @if($role->slug == 'admin' && !\Auth::user()->isAdmin()){{ "disabled='disabled'" }}@endif id="{{ $role->slug }}"
                                                               class="custom-checkbox" name="roles[]"
                                                               value="{{ $role->id }}" @if($checked) {{ 'checked' }}@endif>
                                                        {{ $role->title }}
                                                    </label>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>

                            <div class="button-pane mrg20T">
                                <button type="submit" class="btn btn-info">Update</button>
                            </div>
                        </form>

                    </div>

                </div>
            </div>
        </div>
    </div>
@stop
