@extends('template')
@section('script')
    <script type="text/javascript" src="/js/file-manager.js"></script>
@endsection
@section('body')

    @include('breadcrumbs', array('breadcrumb_title' => 'Profile', 'breadcrumb_list' => [['Dashboard','/'], ['Baboons','/baboons/'.$user->name], ['View profile', '']]) )

    @if(session()->has('status'))
        <div class="alert alert-success flex mrg25B">
            <div class="bg-green alert-icon">
                <i class="glyph-icon icon-check"></i>
            </div>
            <div class="alert-content">
                <h4 class="alert-title">{{ session('status') }}</h4>
            </div>
        </div>
    @endif

    <input type="hidden" id="slug" value="{{ $user->slug }}">

    <script>
        $(document).ready(function () {
            setFileStructure('{{ $user->slug }}');
            $('#submit-create-folder').on('click', function (e) {
                var path = $('#dropzone-path').val();
                var slug = $('#slug').val();
                var name = $('#folder-name').val();
                if(name == '')
                {
                    $('#folder-name').val('This field is required');
                }else{
                    createFolder(slug, name, path);
                }
            })
        })
    </script>

    @if(\Auth::user()->isAccountManager())
        <div class="row">
            <div class="col-md-12">
                <button data-toggle="modal" data-target="#modal-user-{{ $user->id }}" class="btn btn-danger float-right mrg25B mrg20L"><i class="glyph-icon icon-elusive-cancel-circled"></i>&nbsp;Delete<div class="ripple-wrapper"></div></button>
                <div class="modal fade" id="modal-user-{{ $user->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h4 class="modal-title">Delete user</h4>
                            </div>
                            <div class="modal-body">
                                <p>Are you sure that you want to delete the user {{ $user->firstname }} {{ $user->lastname }}?</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <a href="/baboons/{{ $user->slug }}/delete" class="btn btn-danger">Delete</a>
                            </div>
                        </div>
                    </div>
                </div>
                <a href="/baboons/{{ $user->slug }}/edit" class="btn btn-success float-right mrg25B"><i class="glyph-icon icon-edit"></i>&nbsp;Edit<div class="ripple-wrapper"></div></a>
            </div>
        </div>
    @endif
    <div class="row">
        <div class="col-md-12">
            <div class="example-box-wrapper">
                <ul class="list-group row list-group-icons list-group-centered list-group-separator">
                    <li class="col-md-2 active">
                        <a href="#tab-profile" data-toggle="tab" class="list-group-item">
                            <i class="glyph-icon font-red icon-elusive-user"></i>
                            Profile
                        </a>
                    </li>
                    <li class="col-md-2">
                        <a href="#tab-files" data-toggle="tab" class="list-group-item @if( !\Auth::user()->isAdmin()){{ 'disabled' }}@endif">
                            <i class="glyph-icon icon-file"></i>
                            Files
                        </a>
                    </li>
                </ul>
                <div class="tab-content">

                    <div class="tab-pane pad0A fade active in" id="tab-profile">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="panel-layout">
                                    <div class="panel-box">
                                        <div class="panel-content image-box">
                                            <div class="ribbon">
                                                <div class="bg-blue">Baboon</div>
                                                {{--<div class="bg-primary">Client</div>--}}
                                                {{--<div class="bg-purple">Contractor</div>--}}
                                            </div>
                                            <div class="image-content font-white">
                                                <div class="meta-box">
                                                    <img src="{{ $user->picture }}" alt="" class="meta-image img-bordered img-circle">
                                                    <h3 class="meta-heading">{{ $user->firstname }} {{ $user->lastname }}</h3>
                                                    <h4 class="meta-subheading mrg15T font-light-blue {{ $user->roles->isEmpty() ? 'hide' : ''}}">
                                                        @foreach($user->roles as $role)
                                                            {{ $role->title }}

                                                            {{ $role == $user->roles->last() ? '' : ', ' }}
                                                        @endforeach
                                                    </h4>
                                                    <p class="mrg15T">{{ $user->email }}</p>
                                                </div>
                                            </div>
                                            <img src="/assets/image-resources/blurred-bg/blurred-bg-13.jpg" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-8">
                                <div class="row">
                                    <div class="content-box mrg15B">
                                        <h3 class="content-box-header clearfix">
                                            <i class="glyph-icon icon-elusive-desktop"></i>
                                            Projects
                                        </h3>
                                        <div class="content-box-wrapper pad0T clearfix">
                                            <table class="table table-striped">
                                                <thead>
                                                <tr>
                                                    <th>Name</th>
                                                    <th>Company</th>
                                                    <th>Website</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($user->projects as $project)
                                                    <tr>
                                                        <td><a href="{{ url('/projects/'.$project->slug) }}">{{ $project->name }}</a></td>
                                                        <td>{{ $project->company }}</td>
                                                        <td><a href="{{ $project->website_url }}">{{ $project->website_url }}</a></td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>

                                        </div>

                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    @if( \Auth::user()->isAdmin())
                    <div class="tab-pane pad0A fade" id="tab-files">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="content-box mrg15B">
                                    <h3 id="breadcrumb" class="content-box-header text-left" style="border-bottom-color: rgba(158, 173, 195, 0.16);">

                                    </h3>
                                    <div class="content-box-wrapper">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <button data-toggle="modal" data-target="#modal-folder" class="btn btn-info mrg25B mrg10L"><i class="glyph-icon icon-elusive-plus"></i>&nbsp;Create folder<div class="ripple-wrapper"></div></button>
                                                <div class="modal fade" id="modal-folder" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                                <h4 class="modal-title">Create folder</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <form class="form-horizontal">
                                                                    <div class="form-group">
                                                                        <div class="col-md-2"></div>
                                                                        <label class="col-auto control-label">Name</label>
                                                                        <div class="col-sm-6">
                                                                            <input type="text" class="form-control" id="folder-name" required>
                                                                        </div>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                <button id="submit-create-folder" class="btn btn-info">Create</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" id="dropzone-example">
                                            <div class="col-md-12">
                                                <form action="/people/{{ $user->slug }}/file-upload" class="dropzone bg-gray col-md-12 center-margin flex" id="demo-upload" enctype="multipart/form-data">
                                                    {!! csrf_field() !!}
                                                    <input type="hidden" name="path" value="" id="dropzone-path">
                                                </form>
                                            </div>
                                        </div>
                                        <div class="row mrg20T">
                                            <div class="col-md-12" id="file-cols">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>


@stop
