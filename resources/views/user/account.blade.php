@extends('template')
@section('body')

    @include('breadcrumbs', array('breadcrumb_title' => 'Account', 'breadcrumb_list' => [['Dashboard','/'], ['Account','/account']]) )

    @if(session()->has('status'))
        <div class="alert alert-close alert-success">
            <a href="#" title="Close" class="glyph-icon alert-close-btn icon-remove"></a>
            <div class="bg-green alert-icon">
                <i class="glyph-icon icon-check"></i>
            </div>
            <div class="alert-content">
                <h4 class="alert-title">{{ session()->get('status') }}</h4>
            </div>
        </div>
    @endif

        <div class="col-md-12">
            <div class="example-box-wrapper">
                <ul class="list-group row list-group-icons">
                    <li class="col-md-3 {{ $errors->has('password') || $errors->has('old_password') || session()->has('ok-update-settings') ? '' : 'active' }}">
                        <a href="#tab-personal-info" data-toggle="tab" class="list-group-item">
                            <i class="glyph-icon font-red icon-bullhorn"></i>
                            Personal Information
                        </a>
                    </li>
                    <li class="col-md-3 {{ $errors->has('password') || $errors->has('old_password') || session()->has('ok-update-settings') ? 'active' : '' }}">
                        <a href="#tab-account-settings" data-toggle="tab" class="list-group-item">
                            <i class="glyph-icon icon-dashboard"></i>
                            Account Settings
                        </a>
                    </li>
                </ul>
                <div class="tab-content">

                    <div class="tab-pane pad0A fade {{ $errors->has('password') || $errors->has('old_password') || session()->has('ok-update-settings') ? '' : 'active in' }}" id="tab-personal-info">
                        @if(session()->has('ok-personal-information'))
                            <div class="alert alert-success flex">
                                <div class="bg-green alert-icon">
                                    <i class="glyph-icon icon-check"></i>
                                </div>
                                <div class="alert-content">
                                    <h4 class="alert-title">{{ session('ok-personal-information') }}</h4>
                                </div>
                            </div>
                        @endif

                        <div class="content-box">
                            <form class="form-horizontal pad15L pad15R bordered-row" method="POST" action="{{ url('account/personal-information/update') }}" enctype="multipart/form-data">
                                {!! csrf_field() !!}
                                <input id="hasPicture" type="hidden" name="hasPicture" value="{{ $user->picture != '' ? 'true' : 'false' }}">
                                <div class="form-group remove-border">
                                    <label class="col-auto control-label">First Name:</label>
                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-4">
                                        <input type="text" class="form-control {{ $errors->has('firstname') ? ' parsley-error' : '' }}" name="firstname" value="{{ $user->firstname }}" placeholder="First name">
                                        @if ($errors->has('firstname'))
                                            <span class="help-block">
                                             {{ $errors->first('firstname') }}
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-auto control-label">Last Name:</label>
                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-4">
                                        <input type="text" class="form-control {{ $errors->has('lastname') ? ' parsley-error' : '' }}" name="lastname" value="{{ $user->lastname }}" placeholder="Last name">
                                        @if ($errors->has('lastname'))
                                            <span class="help-block">
                                             {{ $errors->first('lastname') }}
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-auto control-label">Email:</label>
                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-4">
                                        <input type="text" class="form-control {{ $errors->has('email') ? ' parsley-error' : '' }}" name="email" value="{{ $user->email }}" placeholder="Email address">
                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                             {{ $errors->first('email') }}
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-auto control-label">Profile Picture:</label>
                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-4">
                                        <div class="fileinput fileinput-new">
                                            <div id='file-preview' class="fileinput-preview thumbnail @if( $user->picture == ''){{ 'hide' }}@endif" style="height: 150px;max-width: 130px;">
                                                <img id="profile-picture-img" src="{{ $user->picture }}">
                                            </div>
                                            <div>
                                                <span class="btn btn-default btn-file">
                                                    <span id="change" class="@if( $user->picture == ''){{ 'hide' }}@endif">Change</span>
                                                    <span id="select" class="@if( $user->picture != ''){{ 'hide' }}@endif">Select image</span>
                                                    <input type="file" id="profile-picture-input" name="picture">
                                                </span>
                                                <a href="#" id="remove-profile-picture" class="btn btn-default @if( $user->picture == ''){{ 'hide' }}@endif"><i class="glyph-icon icon-times"></i></a>
                                                @if ($errors->has('picture'))
                                                    <span class="help-block">
                                                        {{ $errors->first('picture') }}
                                                     </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="button-pane mrg20T">
                                    <button type="submit" class="btn btn-info" id="btn-save">Save</button>
                                </div>
                            </form>

                        </div>
                    </div>
                    <div class="tab-pane fade {{ $errors->has('password') || $errors->has('old_password') || session()->has('ok-update-settings') ? 'active in' : '' }}" id="tab-account-settings">
                        @if(session()->has('ok-update-settings'))
                            <div class="alert alert-success flex">
                                <div class="bg-green alert-icon">
                                    <i class="glyph-icon icon-check"></i>
                                </div>
                                <div class="alert-content">
                                    <h4 class="alert-title">{{ session('ok-update-settings') }}</h4>
                                </div>
                            </div>
                        @endif
                        <div class="row">
                            <div class="col-md-6">
                                <div class="content-box mrg15B">
                                    <h3 class="content-box-header clearfix">
                                        Roles
                                    </h3>
                                    <div class="content-box-wrapper pad0T clearfix">
                                        <form class="form-horizontal pad15L pad15R bordered-row">
                                            <div class="form-group">
                                                <div class="row">
                                                    @foreach($roles as $role)
                                                        <div class="col-xs-4 col-sm-4 col-md-4">
                                                            <div class="checkbox checkbox-info font-gray-dark">
                                                                <label>
                                                                      <?php $checked = false; ?>
                                                                      @foreach($user->roles as $role_user)
                                                                          @if($role_user->id == $role->id)
                                                                              <?php $checked = true; ?>
                                                                              @break
                                                                          @endif

                                                                      @endforeach
                                                                    <input type="checkbox" disabled='disabled' id="{{ $role->slug }}" class="custom-checkbox" name="roles[]" value="{{ $role->id }}" @if($checked) {{ 'checked' }}@endif>
                                                                    {{ $role->title }}
                                                                </label>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </form>
                                    </div>

                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="content-box mrg15B">
                                    <h3 class="content-box-header clearfix">
                                        Change Password
                                    </h3>
                                    <div class="content-box-wrapper pad0T clearfix">
                                        <form class="form-horizontal pad15L pad15R bordered-row" method="POST" action="{{ url('account/password/update') }}">
                                            {!! csrf_field() !!}
                                            <div class="form-group">
                                                <label class="col-auto control-label">Old password:</label>
                                                <div class="col-sm-6">
                                                    <input type="password" class="form-control {{ $errors->has('old_password') ? ' parsley-error' : '' }}" id="" placeholder="" name="old_password">
                                                    @if ($errors->has('old_password'))
                                                        <span class="help-block">
                                                        {{ $errors->first('old_password') }}
                                                     </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-auto control-label">New password:</label>
                                                <div class="col-sm-6">
                                                    <input type="password" class="form-control {{ $errors->has('password') ? ' parsley-error' : '' }}" id="" placeholder="" name="password">
                                                    @if ($errors->has('password'))
                                                        <span class="help-block">
                                                        {{ $errors->first('password') }}
                                                     </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-auto control-label">Repeat password:</label>
                                                <div class="col-sm-6">
                                                    <input type="password" class="form-control {{ $errors->has('password') ? ' parsley-error' : '' }}" id="" placeholder="" name="password_confirmation">
                                                </div>
                                            </div>
                                            <div class="button-pane mrg20T">
                                                <button type="submit" class="btn btn-info">Update Password</button>
                                            </div>
                                        </form>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>

@stop
