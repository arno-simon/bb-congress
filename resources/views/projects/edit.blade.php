@extends('template')

@section('script')
    <script src="https://api.trello.com/1/client.js?key=176ccfbd6ab7543ba474a6a765b179e7"></script>
    <script type="text/javascript" src="/js/trello.js"></script>
@stop

@section('body')

    @include('breadcrumbs', array('breadcrumb_title' => 'Edit '.$project->name, 'breadcrumb_list' => [['Dashboard','/'], ['Projects','/projects'], ['Edit '.$project->name,'']]) )

    @if(session()->has('status'))
        <div class="alert alert-success flex mrg25B">
            <div class="bg-green alert-icon">
                <i class="glyph-icon icon-check"></i>
            </div>
            <div class="alert-content">
                <h4 class="alert-title">{{ session('status') }}</h4>
            </div>
        </div>
    @endif

    <script>
        $(document).ready(function () {
            // Set template name
            //var templateId = '{{ $project->trello_template_id }}';
            //setTemplateName(templateId);
        })
    </script>

    <div class="row">
        <form class="form-horizontal pad15L pad15R bordered-row" method="POST" action="{{ url('projects/'.$project->slug.'/edit') }}" enctype="multipart/form-data">
            {!! csrf_field() !!}
            <input type="hidden" name="id" value="{{ $project->id }}">
            <div class="row">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-success float-right mrg20B">Save changes</button>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4" id="edit-info">
                    <div class="content-box mrg15B">
                        <h3 class="content-box-header clearfix">
                            <i class="glyph-icon icon-info-circle"></i>&nbsp;
                            Project Information
                        </h3>
                        <div class="content-box-wrapper pad0T clearfix">
                            <div class="form-group">
                                <div class="row">
                                    <label class="col-auto control-label">Types *</label>
                                    <?php
                                    $type_ids =  [];
                                    foreach ($project->types as $type){
                                        array_push($type_ids, $type->id);
                                    }
                                    ?>
                                    <div class="col-xs-10 col-sm-9 col-md-8">
                                        <select name="project_type_id[]" multiple="" data-placeholder="Click to see available project types..." class="chosen-select" style="display: none;">
                                            @foreach($types as $type)
                                                @if( !is_null(old('project_type_id')))
                                                    <option value="{{ $type->id }}" @if(in_array($type->id, old('project_type_id'))){{ 'selected' }}@endif>{{ $type->name }}</option>
                                                @else
                                                    <option value="{{ $type->id }}" @if(in_array($type->id, $type_ids)){{ 'selected' }}@endif>{{ $type->name }}</option>
                                                @endif
                                            @endforeach
                                        </select>

                                        @if ($errors->has('project_type_id'))
                                            <span class="help-block">
                                                {{ $errors->first('project_type_id') }}
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <label class="col-auto control-label">Name *</label>
                                    <div class="col-xs-10 col-sm-9 col-md-8">
                                        <input type="text" class="form-control {{ $errors->has('name') ? ' parsley-error' : '' }}" id="" placeholder="" name="name" value="{{ $project->name }}">
                                        @if ($errors->has('name'))
                                            <span class="help-block">
                                                {{ $errors->first('name') }}
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <label class="col-auto control-label">Company *</label>
                                    <div class="col-xs-10 col-sm-9 col-md-8">
                                        <p>{{ $project->company }}</p>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <label class="col-auto control-label">Budget</label>
                                    <div class="col-xs-10 col-sm-9 col-md-8">
                                        <input type="number" class="form-control {{ $errors->has('budget') ? ' parsley-error' : '' }}" id="" placeholder="" name="budget" value="{{ $project->budget }}">
                                        @if ($errors->has('budget'))
                                            <span class="help-block">
                                                    {{ $errors->first('budget') }}
                                                </span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <label class="col-auto control-label">URL</label>
                                    <div class="col-xs-10 col-sm-9 col-md-8">
                                        <input type="text" class="form-control {{ $errors->has('website_url') ? ' parsley-error' : '' }}" id="" placeholder="" name="website_url" value="{{ $project->website_url }}">
                                        @if ($errors->has('website_url'))
                                            <span class="help-block">
                                                    {{ $errors->first('website_url') }}
                                                </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <label class="col-auto control-label">CPanel URL</label>
                                    <div class="col-xs-10 col-sm-9 col-md-8">
                                        <input type="text" class="form-control {{ $errors->has('cpanel_url') ? ' parsley-error' : '' }}" id="" placeholder="" name="cpanel_url" value="{{ $project->cpanel_url }}">
                                        @if ($errors->has('cpanel_url'))
                                            <span class="help-block">
                                                    {{ $errors->first('cpanel_url') }}
                                                </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <label class="col-auto control-label">Sitemap URL</label>
                                    <div class="col-xs-10 col-sm-9 col-md-8">
                                        <input type="text" class="form-control {{ $errors->has('sitemap_url') ? ' parsley-error' : '' }}" id="" placeholder="" name="sitemap_url" value="{{ $project->sitemap_url }}">
                                        @if ($errors->has('sitemap_url'))
                                            <span class="help-block">
                                                    {{ $errors->first('sitemap_url') }}
                                                </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <label class="col-auto control-label">Invision URL</label>
                                    <div class="col-xs-10 col-sm-9 col-md-8">
                                        <input type="text" class="form-control {{ $errors->has('invision_url') ? ' parsley-error' : '' }}" id="" placeholder="" name="invision_url" value="{{ $project->invision_url }}">
                                        @if ($errors->has('invision_url'))
                                            <span class="help-block">
                                                    {{ $errors->first('invision_url') }}
                                                </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <label class="col-auto control-label">Description</label>
                                    <div class="col-xs-10 col-sm-9 col-md-8">
                                        <textarea name="description" rows="4" class="form-control textarea-no-resize {{ $errors->has('description') ? ' parsley-error' : '' }}">{{ $project->description }}</textarea>
                                        @if ($errors->has('description'))
                                            <span class="help-block">
                                                {{ $errors->first('description') }}
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
                    <div class="content-box mrg15B">
                        <h3 class="content-box-header clearfix">
                            <i class="glyph-icon icon-elusive-group"></i>&nbsp;
                            People
                        </h3>
                        <div class="content-box-wrapper pad0T clearfix">
                            <div class="form-group">
                                <div class="row">
                                    <label class="col-xs-3 control-label">Client</label>
                                    <div class="col-xs-3 col-sm-9 col-md-9">
                                        <select name="client_id" class="form-control">
                                            <option value="">Select client</option>
                                            @foreach($clients as $client)
                                                @if(count($project->client) > 0)
                                                    <option value="{{ $client->id }}" @if($client->id == $project->client->id) {{ 'selected' }}@endif>{{ $client->firstname }} {{ $client->lastname }}</option>
                                                @else
                                                    <option value="{{ $client->id }}">{{ $client->firstname }} {{ $client->lastname }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                        @if ($errors->has('client_id'))
                                            <span class="help-block">
                                                {{ $errors->first('client_id') }}
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <label class="col-xs-3 control-label">Contractors</label>
                                    <?php
                                        $contractor_ids =  [];
                                      foreach ($project->contractors()->get() as $contractor){
                                          array_push($contractor_ids, $contractor->id);
                                      }
                                    ?>
                                    <div class="col-xs-3 col-sm-9 col-md-9">
                                        <select name="contractors_id[]" multiple="" data-placeholder="Click to see available contractors..." class="chosen-select" style="display: none;">
                                            @foreach($contractors as $contractor)
                                                @if( !is_null(old('contractors_id')))
                                                    <option value="{{ $contractor->id }}" @if(in_array($contractor->id, old('contractors_id'))){{ 'selected' }}@endif>{{ $contractor->firstname }} {{ $contractor->lastname }}</option>
                                                @else
                                                    <option value="{{ $contractor->id }}" @if(in_array($contractor->id, $contractor_ids)){{ 'selected' }}@endif>{{ $contractor->firstname }} {{ $contractor->lastname }}</option>
                                                @endif
                                            @endforeach
                                        </select>

                                        @if ($errors->has('contractors_id'))
                                            <span class="help-block">
                                                {{ $errors->first('contractors_id') }}
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <label class="col-auto control-label float-none mrg15B">Baboons</label>
                                    <?php
                                    $baboon_ids =  [];
                                    foreach ($project->users()->get() as $baboon){
                                        array_push($baboon_ids, $baboon->id);
                                    }
                                    ?>
                                    <div class="col-sm-12">
                                        <select multiple="" class="multi-select" name="baboons_id[]" id="92multiselect" style="position: absolute; left: -9999px;">
                                            @foreach($baboons as $baboon)
                                                <option value="{{ $baboon->id }}" @if(in_array($baboon->id, $baboon_ids)){{ 'selected' }}@endif>{{ $baboon->firstname }} {{ $baboon->lastname }}</option>
                                            @endforeach
                                        </select>

                                        @if ($errors->has('baboons_id'))
                                            <span class="help-block">
                                                {{ $errors->first('contractors_id') }}
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
                    <div class="row" id="edit-cms">
                        <div class="col-md-12">
                            <div class="content-box mrg15B">
                                <h3 class="content-box-header clearfix">
                                    <i class="glyph-icon icon-elusive-wordpress"></i>&nbsp;
                                    CMS Details
                                </h3>
                                <div class="content-box-wrapper pad0T clearfix">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-auto control-label">URL</label>
                                            <div class="col-xs-10 col-sm-9 col-md-8">
                                                <input type="text" class="form-control {{ $errors->has('cms_url') ? ' parsley-error' : '' }}" id="" placeholder="" name="cms_url" value="{{ $project->cms_url }}">
                                                @if ($errors->has('cms_url'))
                                                    <span class="help-block">
                                                                {{ $errors->first('cms_url') }}
                                                             </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-auto control-label">Login</label>
                                            <div class="col-xs-10 col-sm-9 col-md-10 cms-col-user">
                                                <div class="row" id="cms-row-user">
                                                    <div class="col-md-5">
                                                        <input type="text" class="form-control input-user" id="" placeholder="Username" name="cms_login[0][user]" value="{{ $project->cmsUsers->isEmpty() ? '' : $project->cmsUsers->first()->user }}">
                                                    </div>
                                                    <div class="col-md-5">
                                                        <input type="text" class="form-control input-password" id="" placeholder="Password" name="cms_login[0][password]" value="{{ $project->cmsUsers->isEmpty() ? '' : \Crypt::decrypt($project->cmsUsers->first()->password) }}">
                                                    </div>
                                                    <div class="col-md-2">
                                                        <a href="#" id="add-cms-user" class="btn btn-round btn-success" title="Add user">
                                                            <i class="glyph-icon icon-elusive-plus"></i>
                                                            <div class="ripple-wrapper"></div>
                                                        </a>
                                                        <a href="#" class="btn btn-round btn-danger hide remove-cms-user" title="Remove user">
                                                            <i class="glyph-icon icon-elusive-minus"></i>
                                                            <div class="ripple-wrapper"></div>
                                                        </a>
                                                    </div>
                                                </div>
                                                @if( !$project->cmsUsers->isEmpty())
                                                    <?php
                                                    $indexCMS = 1;
                                                    ?>
                                                    @foreach($project->cmsUsers as $user)
                                                        @if($user == $project->cmsUsers->first())@continue @endif
                                                        <div class="row mrg10T clone" id="cms-row-user">
                                                            <div class="col-md-5">
                                                                <input type="text" class="form-control input-user" id="" placeholder="Username" name="cms_login[{{ $indexCMS }}][user]" value="{{ $user->user }}">
                                                            </div>
                                                            <div class="col-md-5">
                                                                <input type="text" class="form-control input-password" id="" placeholder="Password" name="cms_login[{{ $indexCMS }}][password]" value="{{ \Crypt::decrypt($user->password) }}">
                                                            </div>
                                                            <div class="col-md-2">
                                                                <a href="#" id="add-cms-user" class="btn btn-round btn-success hide" title="Add user">
                                                                    <i class="glyph-icon icon-elusive-plus"></i>
                                                                    <div class="ripple-wrapper"></div>
                                                                </a>
                                                                <a href="#" class="btn btn-round btn-danger remove-cms-user" title="Remove user">
                                                                    <i class="glyph-icon icon-elusive-minus"></i>
                                                                    <div class="ripple-wrapper"></div>
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <?php $indexCMS++;?>
                                                    @endforeach
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="row" id="edit-ftp">
                        <div class="col-md-12">
                            <div class="content-box mrg15B" id="ftp">
                                <h3 class="content-box-header clearfix">
                                    <i class="glyph-icon icon-upload"></i>&nbsp;
                                    FTP Details
                                </h3>
                                <div class="content-box-wrapper pad0T clearfix">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-auto control-label">Type</label>
                                            <div class="col-xs-10 col-sm-9 col-md-6">
                                                <label class="radio-inline">
                                                    <div class="radio" id="uniform-inlineRadio110"><span id="ftp-box"><input type="radio" id="inlineRadio110" name="ftp_type" class="custom-radio" value="ftp" {{ $project->ftp_type == 'sftp'  ?  ''  :  'checked' }}><i class="glyph-icon icon-circle"></i></span></div>
                                                    FTP
                                                </label>
                                                <label class="radio-inline">
                                                    <div class="radio" id="uniform-inlineRadio110"><span id="sftp-box" class=""><input type="radio" id="inlineRadio110" name="ftp_type" class="custom-radio" value="sftp" {{ $project->ftp_type == 'sftp'  ?  'checked'  :  '' }}><i class="glyph-icon icon-circle"></i></span></div>
                                                    SFTP
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-auto control-label">Host</label>
                                            <div class="col-xs-10 col-sm-9 col-md-8">
                                                <div class="input-group">
                                                    <span id="ftp-host" class="input-group-addon">{{ old('ftp_type') == 'sftp'  ?  'sftp://'  :  'ftp://' }}</span>
                                                    <input type="text" class="form-control {{ $errors->has('ftp_host') ? ' parsley-error' : '' }}" id="" placeholder="" name="ftp_host" value="{{ $project->ftp_host }}">
                                                </div>
                                                @if ($errors->has('ftp_host'))
                                                    <span class="help-block">
                                                                {{ $errors->first('ftp_host') }}
                                                            </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-auto control-label">Port</label>
                                            <div class="col-xs-10 col-sm-9 col-md-8">
                                                <input type="number" class="form-control {{ $errors->has('ftp_port') ? ' parsley-error' : '' }}" id="" placeholder="" name="ftp_port" value="{{ $project->ftp_port }}">
                                                @if ($errors->has('ftp_port'))
                                                    <span class="help-block">
                                                                {{ $errors->first('ftp_port') }}
                                                            </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-auto control-label">Login</label>
                                            <div class="col-xs-10 col-sm-9 col-md-10 ftp-col-user">
                                                <div class="row" id="ftp-row-user">
                                                    <div class="col-md-5">
                                                        <input type="text" class="form-control input-user" id="" placeholder="Username" name="ftp_login[0][user]" value="{{ $project->ftpUsers->isEmpty() ? '' : $project->ftpUsers->first()->user }}">
                                                    </div>
                                                    <div class="col-md-5">
                                                        <input type="text" class="form-control input-password" id="" placeholder="Password" name="ftp_login[0][password]" value="{{ $project->ftpUsers->isEmpty() ? '' : \Crypt::decrypt($project->ftpUsers->first()->password) }}">
                                                    </div>
                                                    <div class="col-md-2">
                                                        <a href="#" id="add-ftp-user" class="btn btn-round btn-success" title="Add user">
                                                            <i class="glyph-icon icon-elusive-plus"></i>
                                                            <div class="ripple-wrapper"></div>
                                                        </a>
                                                        <a href="#" class="btn btn-round btn-danger hide remove-ftp-user" title="Remove user">
                                                            <i class="glyph-icon icon-elusive-minus"></i>
                                                            <div class="ripple-wrapper"></div>
                                                        </a>
                                                    </div>
                                                </div>
                                                @if( !$project->ftpUsers->isEmpty())
                                                    <?php
                                                    $indexFTP = 1;
                                                    ?>
                                                    @foreach($project->ftpUsers as $user)
                                                        @if($user == $project->ftpUsers->first())@continue @endif
                                                        <div class="row mrg10T clone" id="ftp-row-user">
                                                            <div class="col-md-5">
                                                                <input type="text" class="form-control input-user" id="" placeholder="Username" name="ftp_login[{{ $indexFTP }}][user]" value="{{ $user->user }}">
                                                            </div>
                                                            <div class="col-md-5">
                                                                <input type="text" class="form-control input-password" id="" placeholder="Password" name="ftp_login[{{ $indexFTP }}][password]" value="{{ \Crypt::decrypt($user->password) }}">
                                                            </div>
                                                            <div class="col-md-2">
                                                                <a href="#" id="add-ftp-user" class="btn btn-round btn-success hide" title="Add user">
                                                                    <i class="glyph-icon icon-elusive-plus"></i>
                                                                    <div class="ripple-wrapper"></div>
                                                                </a>
                                                                <a href="#" class="btn btn-round btn-danger remove-ftp-user" title="Remove user">
                                                                    <i class="glyph-icon icon-elusive-minus"></i>
                                                                    <div class="ripple-wrapper"></div>
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <?php $indexFTP++;?>
                                                    @endforeach
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="row" id="edit-domain">
                        <div class="col-md-12">
                            <div class="content-box mrg15B">
                                <h3 class="content-box-header clearfix">
                                    <i class="glyph-icon icon-elusive-globe"></i>&nbsp;
                                    Domain login
                                </h3>
                                <div class="content-box-wrapper pad0T clearfix">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-auto control-label">URL</label>
                                            <div class="col-xs-10 col-sm-9 col-md-8">
                                                <input type="text" class="form-control {{ $errors->has('domain_url') ? ' parsley-error' : '' }}" id="" placeholder="" name="domain_url" value="{{ $project->domain_url }}">
                                                @if ($errors->has('domain_url'))
                                                    <span class="help-block">
                                                                {{ $errors->first('domain_url') }}
                                                             </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-auto control-label">User</label>
                                            <div class="col-xs-10 col-sm-9 col-md-8">
                                                <input type="text" class="form-control {{ $errors->has('domain_user') ? ' parsley-error' : '' }}" id="" placeholder="" name="domain_user" value="{{ $project->domain_user }}">
                                                @if ($errors->has('domain_user'))
                                                    <span class="help-block">
                                                                {{ $errors->first('domain_user') }}
                                                             </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-auto control-label">Password</label>
                                            <div class="col-xs-10 col-sm-9 col-md-8">
                                                <input type="text" class="form-control {{ $errors->has('domain_password') ? ' parsley-error' : '' }}" id="" placeholder="" name="domain_password" value="{{ $project->domain_password == '' ? '' : \Crypt::decrypt($project->domain_password) }}">
                                                @if ($errors->has('domain_password'))
                                                    <span class="help-block">
                                                                {{ $errors->first('domain_password') }}
                                                             </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </form>
    </div>
@stop
