@extends('template')

@section('script')
    <script src="https://api.trello.com/1/client.js?key=176ccfbd6ab7543ba474a6a765b179e7"></script>
    <script type="text/javascript" src="/js/trello.js"></script>
@stop

@section('body')

    @include('breadcrumbs', array('breadcrumb_title' => 'New project', 'breadcrumb_list' => [['Dashboard','/'], ['Projects','/projects'], ['New project','']]) )

    @if(session()->has('status'))
        <div class="alert alert-success flex mrg25B">
            <div class="bg-green alert-icon">
                <i class="glyph-icon icon-check"></i>
            </div>
            <div class="alert-content">
                <h4 class="alert-title">{{ session('status') }}</h4>
            </div>
        </div>
    @endif

    <script>
        $(document).ready(function () {
            // Get templates
            //var organization = "{{ config('trello.template_organization') }}";
            //setTemplateList(organization);
            //setCompanyList();
        })
    </script>

    <div class="row">
        <form class="form-horizontal pad15L pad15R bordered-row" method="POST" action="{{ url('projects/new') }}" enctype="multipart/form-data">
            {!! csrf_field() !!}
            <meta name="csrf-token" content="{{ csrf_token() }}">
            <div class="row">
                <div class="col-md-9">
                    <div class="alert alert-warning">
                        <p>Note that a blank new project will be created within Asana. You will need to populate it with tasks manually afterwards.</p>
                    </div>
                </div>
                <div class="col-md-3">
                    <button type="submit" data-loading-text="Please wait..." class="btn loading-button btn-info float-right mrg20B">Create project</button>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4" id="edit-info">
                    <div class="content-box mrg15B">
                        <h3 class="content-box-header clearfix">
                            <i class="glyph-icon icon-info-circle"></i>&nbsp;
                            Project Information
                        </h3>
                        <div class="content-box-wrapper pad0T clearfix">
                            <div class="form-group">
                                <div class="row">
                                    <label class="col-auto control-label">Type *</label>
                                    <div class="col-xs-10 col-sm-9 col-md-8">
                                        <select name="project_type_id[]" multiple="" data-placeholder="Click to see available project types..." class="chosen-select" style="display: none;">
                                            @foreach($types as $type)
                                                @if( !is_null(old('project_type_id')))
                                                    <option value="{{ $type->id }}" @if(in_array($type->id, old('project_type_id'))){{ 'selected' }}@endif>{{ $type->name }}</option>
                                                @else
                                                    <option value="{{ $type->id }}">{{ $type->name }}</option>
                                                @endif
                                            @endforeach
                                        </select>

                                        @if ($errors->has('project_type_id'))
                                            <span class="help-block">
                                                {{ $errors->first('project_type_id') }}
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <label class="col-auto control-label">Name *</label>
                                    <div class="col-xs-10 col-sm-9 col-md-8">
                                        <input type="text" class="form-control {{ $errors->has('name') ? ' parsley-error' : '' }}" id="" placeholder="" name="name" value="{{ old('name') }}">
                                        @if ($errors->has('name'))
                                            <span class="help-block">
                                                {{ $errors->first('name') }}
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <label class="col-auto control-label">Company *</label>
                                    <div id="col-list-company" class="col-xs-10 col-sm-9 col-md-8">
                                        <select id="select-company" class="form-control" name="company">
                                            @foreach($teams->data as $team)
                                                <option value="{{ $team->id }}">{{ $team->name }}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('company'))
                                            <span class="help-block">
                                                {{ $errors->first('company') }}
                                            </span>
                                        @endif
                                    </div>

                                    {{--<div id="col-company" class="col-xs-10 col-sm-9 col-md-8" style="display:none">--}}
                                        {{--<input type="text" class="form-control" id="company-name" placeholder="Name" name="company-name">--}}
                                    {{--</div>--}}

                                </div>
                                {{--<div class="row mrg10T">--}}
                                    {{--<div class="col-auto">&nbsp;</div>--}}
                                    {{--<div class="col-xs-10 col-sm-9 col-md-8">--}}
                                        {{--<div id="col-add-company" class="float-right">--}}
                                            {{--<a href="#" id="add-company" class="btn btn-link pad0A" title="Add company">--}}
                                                {{--Add new company--}}
                                            {{--</a>--}}
                                        {{--</div>--}}

                                        {{--<div id="col-submit-company" class="float-right" style="display:none">--}}
                                            {{--<a href="#" id="add-submit-company" class="btn btn-info" title="Add company">--}}
                                                {{--ADD--}}
                                                {{--<div class="ripple-wrapper"></div>--}}
                                            {{--</a>--}}
                                            {{--<a href="#" id="cancel-company" class="btn btn-danger remove-company-name" title="Cancel">--}}
                                                {{--<i class="glyph-icon icon-elusive-cancel"></i>--}}
                                                {{--<div class="ripple-wrapper"></div>--}}
                                            {{--</a>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <label class="col-auto control-label">Budget</label>
                                    <div class="col-xs-10 col-sm-9 col-md-8">
                                        <input type="number" class="form-control {{ $errors->has('budget') ? ' parsley-error' : '' }}" id="" placeholder="" name="budget" value="{{ old('budget') }}">
                                        @if ($errors->has('budget'))
                                            <span class="help-block">
                                                    {{ $errors->first('budget') }}
                                                </span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <label class="col-auto control-label">URL</label>
                                    <div class="col-xs-10 col-sm-9 col-md-8">
                                        <input type="text" class="form-control {{ $errors->has('website_url') ? ' parsley-error' : '' }}" id="" placeholder="" name="website_url" value="{{ old('website_url') }}">
                                        @if ($errors->has('website_url'))
                                            <span class="help-block">
                                                    {{ $errors->first('website_url') }}
                                                </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <label class="col-auto control-label">CPanel URL</label>
                                    <div class="col-xs-10 col-sm-9 col-md-8">
                                        <input type="text" class="form-control {{ $errors->has('cpanel_url') ? ' parsley-error' : '' }}" id="" placeholder="" name="cpanel_url" value="{{ old('cpanel_url') }}">
                                        @if ($errors->has('cpanel_url'))
                                            <span class="help-block">
                                                    {{ $errors->first('cpanel_url') }}
                                                </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <label class="col-auto control-label">Sitemap URL</label>
                                    <div class="col-xs-10 col-sm-9 col-md-8">
                                        <input type="text" class="form-control {{ $errors->has('sitemap_url') ? ' parsley-error' : '' }}" id="" placeholder="" name="sitemap_url" value="{{ old('sitemap_url') }}">
                                        @if ($errors->has('sitemap_url'))
                                            <span class="help-block">
                                                    {{ $errors->first('sitemap_url') }}
                                                </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <label class="col-auto control-label">Invision URL</label>
                                    <div class="col-xs-10 col-sm-9 col-md-8">
                                        <input type="text" class="form-control {{ $errors->has('invision_url') ? ' parsley-error' : '' }}" id="" placeholder="" name="invision_url" value="{{ old('invision_url') }}">
                                        @if ($errors->has('invision_url'))
                                            <span class="help-block">
                                                    {{ $errors->first('invision_url') }}
                                                </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <label class="col-auto control-label">Description</label>
                                    <div class="col-xs-10 col-sm-9 col-md-8">
                                        <textarea name="description" rows="4" class="form-control textarea-no-resize {{ $errors->has('description') ? ' parsley-error' : '' }}">{{ old('description') }}</textarea>
                                        @if ($errors->has('description'))
                                            <span class="help-block">
                                                {{ $errors->first('description') }}
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <label class="col-auto control-label">Technical specification</label>
                                    <div class="col-xs-10 col-sm-9 col-md-6 mrg10T">
                                        <input type="file" name="file">
                                        @if ($errors->has('file'))
                                            <span class="help-block">
                                                {{ $errors->first('file') }}
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4" id="edit-people">
                    <div class="content-box mrg15B">
                        <h3 class="content-box-header clearfix">
                            <i class="glyph-icon icon-elusive-group"></i>&nbsp;
                            People
                        </h3>
                        <div class="content-box-wrapper pad0T clearfix">
                            <div class="form-group">
                                <div class="row">
                                    <label class="col-auto control-label">Client</label>
                                    <div class="col-xs-10 col-sm-9 col-md-9">
                                        <select id="client-select" name="client_id" class="form-control">
                                            <option value="">Select client</option>
                                            @foreach($clients as $client)
                                                <option value="{{ $client->id }}" @if(old('client_id') == $client->id){{ 'selected' }}@endif>{{ $client->firstname }} {{ $client->lastname }}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('client_id'))
                                            <span class="help-block">
                                                {{ $errors->first('client_id') }}
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="row mrg10T">
                                    <div class="col-auto">&nbsp;</div>
                                    <div class="col-xs-10 col-sm-9 col-md-9">
                                        <div id="col-add-company" class="float-right">
                                            <a href="#" id="" class="btn btn-link pad0A" title="Add company" data-toggle="modal" data-target="#addClient">
                                                Add new client
                                            </a>
                                        </div>

                                        <!-- Modal -->
                                        <div class="modal fade" id="addClient" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                            <div class="modal-dialog modal-sm" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                        <h4 class="modal-title" id="myModalLabel">Add new client</h4>
                                                    </div>
                                                    <div class="modal-body pad25A">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <input type="text" class="form-control" id="client-firstname" placeholder="First name">
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <input type="text" class="form-control" id="client-lastname" placeholder="Last name">
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <input type="email" class="form-control" id="client-email" placeholder="Email">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                        <button type="button" id="btn-add-client" class="btn btn-info">Add client</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <label class="col-auto control-label">Contractors</label>
                                    <div class="col-xs-10 col-sm-9 col-md-9">
                                        <select name="contractors_id[]" multiple="" data-placeholder="Select contractors..." class="chosen-select" style="display: none;">
                                            @foreach($contractors as $contractor)
                                                @if( !is_null(old('contractors_id')))
                                                    <option value="{{ $contractor->id }}" @if(in_array($contractor->id, old('contractors_id'))){{ 'selected' }}@endif>{{ $contractor->firstname }} {{ $contractor->lastname }}</option>
                                                @else
                                                    <option value="{{ $contractor->id }}">{{ $contractor->firstname }} {{ $contractor->lastname }}</option>
                                                @endif
                                            @endforeach
                                        </select>

                                        @if ($errors->has('contractors_id'))
                                            <span class="help-block">
                                                {{ $errors->first('contractors_id') }}
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <label class="col-auto control-label float-none mrg15B">Baboons</label>
                                    <div class="col-sm-12">
                                        <select multiple="" class="multi-select" name="baboons_id[]" id="92multiselect" style="position: absolute; left: -9999px;">
                                            @foreach($baboons as $baboon)
                                                <?php
                                                    $roles = '';
                                                    foreach ($baboon->roles as $role)
                                                    {
                                                        $roles .= $role->title;
                                                        $role == $baboon->roles->last() ? $roles .= '' : $roles .= ', ';
                                                    }
                                                ?>
                                                @if( !is_null(old('baboons_id')))
                                                    <option value="{{ $baboon->id }}" @if(in_array($baboon->id, old('baboons_id'))){{ 'selected' }}@endif>{{ $baboon->firstname }} {{ $baboon->lastname }}</option>
                                                @else
                                                    <option value="{{ $baboon->id }}">{{ $baboon->firstname }} {{ $baboon->lastname }}</option>
                                                @endif
                                            @endforeach
                                        </select>

                                        @if ($errors->has('baboons_id'))
                                            <span class="help-block">
                                                {{ $errors->first('contractors_id') }}
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
                    <div class="row" id="edit-cms">
                        <div class="col-md-12">
                            <div class="content-box mrg15B">
                                <h3 class="content-box-header clearfix">
                                    <i class="glyph-icon icon-elusive-wordpress"></i>&nbsp;
                                    CMS Details
                                </h3>
                                <div class="content-box-wrapper pad0T clearfix">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-auto control-label">URL</label>
                                            <div class="col-xs-10 col-sm-9 col-md-8">
                                                <input type="text" class="form-control {{ $errors->has('cms_url') ? ' parsley-error' : '' }}" id="" placeholder="" name="cms_url" value="{{ old('cms_url') }}">
                                                @if ($errors->has('cms_url'))
                                                    <span class="help-block">
                                                                {{ $errors->first('cms_url') }}
                                                             </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-auto control-label">Login</label>
                                            <div class="col-xs-10 col-sm-9 col-md-10 cms-col-user">
                                                <div class="row" id="cms-row-user">
                                                    <div class="col-md-5">
                                                        <input type="text" class="form-control input-user" id="" placeholder="Username" name="cms_login[0][user]">
                                                    </div>
                                                    <div class="col-md-5">
                                                        <input type="text" class="form-control input-password" id="" placeholder="Password" name="cms_login[0][password]">
                                                    </div>
                                                    <div class="col-md-2">
                                                        <a href="#" id="add-cms-user" class="btn btn-round btn-success" title="Add user">
                                                            <i class="glyph-icon icon-elusive-plus"></i>
                                                            <div class="ripple-wrapper"></div>
                                                        </a>
                                                        <a href="#" class="btn btn-round btn-danger hide remove-cms-user" title="Remove user">
                                                            <i class="glyph-icon icon-elusive-minus"></i>
                                                            <div class="ripple-wrapper"></div>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="row" id="edit-ftp">
                        <div class="col-md-12">
                            <div class="content-box mrg15B" id="ftp">
                                <h3 class="content-box-header clearfix">
                                    <i class="glyph-icon icon-upload"></i>&nbsp;
                                    FTP Details
                                </h3>
                                <div class="content-box-wrapper pad0T clearfix">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-auto control-label">Type</label>
                                            <div class="col-xs-10 col-sm-9 col-md-6">
                                                <label class="radio-inline">
                                                    <div class="radio" id="uniform-inlineRadio110"><span id="ftp-box"><input type="radio" id="inlineRadio110" name="ftp_type" class="custom-radio" value="ftp" {{ old('ftp_type') == 'sftp'  ?  ''  :  'checked' }}><i class="glyph-icon icon-circle"></i></span></div>
                                                    FTP
                                                </label>
                                                <label class="radio-inline">
                                                    <div class="radio" id="uniform-inlineRadio110"><span id="sftp-box" class=""><input type="radio" id="inlineRadio110" name="ftp_type" class="custom-radio" value="sftp" {{ old('ftp_type') == 'sftp'  ?  'checked'  :  '' }}><i class="glyph-icon icon-circle"></i></span></div>
                                                    SFTP
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-auto control-label">Host</label>
                                            <div class="col-xs-10 col-sm-9 col-md-8">
                                                <div class="input-group">
                                                    <span id="ftp-host" class="input-group-addon">{{ old('ftp_type') == 'sftp'  ?  'sftp://'  :  'ftp://' }}</span>
                                                    <input type="text" class="form-control {{ $errors->has('ftp_host') ? ' parsley-error' : '' }}" id="" placeholder="" name="ftp_host" value="{{ old('ftp_host') }}">
                                                </div>
                                                @if ($errors->has('ftp_host'))
                                                    <span class="help-block">
                                                                {{ $errors->first('ftp_host') }}
                                                            </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-auto control-label">Port</label>
                                            <div class="col-xs-10 col-sm-9 col-md-8">
                                                <input type="number" class="form-control {{ $errors->has('ftp_port') ? ' parsley-error' : '' }}" id="" placeholder="" name="ftp_port" value="{{ old('ftp_port') }}">
                                                @if ($errors->has('ftp_port'))
                                                    <span class="help-block">
                                                                {{ $errors->first('ftp_port') }}
                                                            </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-auto control-label">Login</label>
                                            <div class="col-xs-10 col-sm-9 col-md-10 ftp-col-user">
                                                <div class="row" id="ftp-row-user">
                                                    <div class="col-md-5">
                                                        <input type="text" class="form-control input-user" id="" placeholder="Username" name="ftp_login[0][user]">
                                                    </div>
                                                    <div class="col-md-5">
                                                        <input type="text" class="form-control input-password" id="" placeholder="Password" name="ftp_login[0][password]">
                                                    </div>
                                                    <div class="col-md-2">
                                                        <a href="#" id="add-ftp-user" class="btn btn-round btn-success" title="Add user">
                                                            <i class="glyph-icon icon-elusive-plus"></i>
                                                            <div class="ripple-wrapper"></div>
                                                        </a>
                                                        <a href="#" class="btn btn-round btn-danger hide remove-ftp-user" title="Remove user">
                                                            <i class="glyph-icon icon-elusive-minus"></i>
                                                            <div class="ripple-wrapper"></div>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="row" id="edit-domain">
                        <div class="col-md-12">
                            <div class="content-box mrg15B">
                                <h3 class="content-box-header clearfix">
                                    <i class="glyph-icon icon-elusive-globe"></i>&nbsp;
                                    Domain login
                                </h3>
                                <div class="content-box-wrapper pad0T clearfix">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-auto control-label">URL</label>
                                            <div class="col-xs-10 col-sm-9 col-md-8">
                                                <input type="text" class="form-control {{ $errors->has('domain_url') ? ' parsley-error' : '' }}" id="" placeholder="" name="domain_url" value="{{ old('domain_url') }}">
                                                @if ($errors->has('domain_url'))
                                                    <span class="help-block">
                                                                {{ $errors->first('domain_url') }}
                                                             </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-auto control-label">User</label>
                                            <div class="col-xs-10 col-sm-9 col-md-8">
                                                <input type="text" class="form-control {{ $errors->has('domain_user') ? ' parsley-error' : '' }}" id="" placeholder="" name="domain_user" value="{{ old('domain_user') }}">
                                                @if ($errors->has('domain_user'))
                                                    <span class="help-block">
                                                                {{ $errors->first('domain_user') }}
                                                             </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-auto control-label">Password</label>
                                            <div class="col-xs-10 col-sm-9 col-md-8">
                                                <input type="text" class="form-control {{ $errors->has('domain_password') ? ' parsley-error' : '' }}" id="" placeholder="" name="domain_password" value="{{ old('domain_password') }}">
                                                @if ($errors->has('domain_password'))
                                                    <span class="help-block">
                                                                {{ $errors->first('domain_password') }}
                                                             </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </form>
    </div>
@stop
