@extends('template')

@section('script')
    <script src="https://api.trello.com/1/client.js?key=176ccfbd6ab7543ba474a6a765b179e7"></script>
    <script type="text/javascript" src="/js/trello.js"></script>

    @if(\Auth::user()->isAdmin())
        <script>

        </script>
    @endif
@stop

@section('body')

    @include('breadcrumbs', array('breadcrumb_title' => $project->name, 'breadcrumb_list' => [['Dashboard','/'], ['Projects','/projects'], [$project->name,'']]) )

    @if(session()->has('status'))
        <div class="alert alert-success flex mrg25B">
            <div class="bg-green alert-icon">
                <i class="glyph-icon icon-check"></i>
            </div>
            <div class="alert-content">
                <h4 class="alert-title">{{ session('status') }}</h4>

            </div>
        </div>
    @endif

    @if(session()->has('warning'))
        <div class="alert alert-warning flex mrg25B">
            <div class="bg-orange alert-icon">
                <i class="glyph-icon icon-warning"></i>
            </div>
            <div class="alert-content">
                <h4 class="alert-title">{{ session('warning') }}</h4>
            </div>
        </div>
    @endif

    <script>
        {{--$(document).ready(function () {--}}
            {{--// Display project is user is part of the project--}}
            {{--@if( !$project->users()->where('id', \Auth::user()->id)->get()->isEmpty())--}}
                    {{--@if($project->status == 'ongoing')--}}
                        {{--boardId = '{{ $project->trello_board_id  }}';--}}
                        {{--displayProject(boardId);--}}

                    {{--@elseif($project->status == 'retained')--}}
                        {{--boardId = '{{ $project->trello_support_board_id  }}';--}}
                        {{--displayProject(boardId);--}}

                    {{--@endif--}}
            {{--@endif--}}
        {{--})--}}
    </script>

        <div class="row">
            <div class="col-md-12">
                @if(\Auth::user()->isProjectManager() && !$project->users()->where('id', \Auth::user()->id)->get()->isEmpty())

                    <button data-toggle="modal" data-target="#modal{{ $project->id }}" class="btn btn-danger float-right mrg25B mrg20L"><i class="glyph-icon icon-elusive-cancel-circled"></i>&nbsp;Delete<div class="ripple-wrapper"></div></button>
                    <div class="modal fade" id="modal{{ $project->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h4 class="modal-title">Delete project</h4>
                                </div>
                                <div class="modal-body">
                                    <p>Are you sure that you want to delete the project {{ $project->name }}?</p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <a href="/projects/{{ $project->slug }}/delete" class="btn btn-danger">Delete</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <a href="/projects/{{ $project->slug }}/edit" class="btn btn-success float-right mrg25B mrg20L"><i class="glyph-icon icon-edit"></i>&nbsp;Edit<div class="ripple-wrapper"></div></a>
                    @if($project->status == 'ongoing')
                        <div class="btn-group float-right">
                            <button type="button" class="btn btn-azure dropdown-toggle" data-toggle="dropdown" aria-expanded="false">{{ $project->status }} <span class="caret"></span><div class="ripple-wrapper"></div></button>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="/projects/{{ $project->slug }}/retain">Retain</a></li>
                                <li><a href="/projects/{{ $project->slug }}/close">Close</a></li>
                            </ul>
                        </div>
                    @elseif($project->status == 'retained')
                        <div class="btn-group float-right">
                            <button type="button" class="btn btn-yellow dropdown-toggle" data-toggle="dropdown" aria-expanded="false">{{ $project->status }} <span class="caret"></span><div class="ripple-wrapper"></div></button>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="/projects/{{ $project->slug }}/open">Open</a></li>
                                <li><a href="/projects/{{ $project->slug }}/close">Close</a></li>
                            </ul>
                        </div>
                    @else
                        <div class="btn-group float-right">
                            <button type="button" class="btn btn-black dropdown-toggle" data-toggle="dropdown" aria-expanded="false">{{ $project->status }} <span class="caret"></span><div class="ripple-wrapper"></div></button>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="/projects/{{ $project->slug }}/open">Open</a></li>
                                <li><a href="/projects/{{ $project->slug }}/retain">Retain</a></li>
                            </ul>
                        </div>
                    @endif
                @else
                    @if($project->status == 'ongoing')
                        <a class="btn btn-azure float-right disabled" href="#" role="button">{{ $project->status }}</a>
                    @elseif($project->status == 'retained')
                        <a class="btn btn-yellow float-right disabled" href="#" role="button">{{ $project->status }}</a>
                    @else
                        <a class="btn btn-black float-right disabled" href="#" role="button">{{ $project->status }}</a>
                    @endif

                @endif
            </div>
        </div>
    <div class="col-md-12">
        <div class="example-box-wrapper">
            <ul class="list-group row">
                <li class="col-md-2 {{ session()->has('delete_file') ? '' : 'active' }}">
                    <a href="#tab-home" data-toggle="tab" class="list-group-item">
                        <i class="glyph-icon icon-info"></i>
                        Details
                        <i class="glyph-icon icon-chevron-right"></i>
                    </a>
                </li>
                {{--@if($project->status != 'closed' && !$project->users()->where('id', \Auth::user()->id)->get()->isEmpty())--}}
                    {{--<li class="col-md-2">--}}
                        {{--<a href="#tab-trello" data-toggle="tab" class="list-group-item">--}}
                            {{--<i class="glyph-icon icon-trello"></i>--}}
                            {{--Trello--}}
                            {{--<i class="glyph-icon icon-chevron-right"></i>--}}
                        {{--</a>--}}
                    {{--</li>--}}
                {{--@endif--}}
                @if($project->is_supported)
                    <li class="col-md-3">
                        <a href="#tab-retainer" data-toggle="tab" class="list-group-item">
                            <i class="glyph-icon icon-life-bouy"></i>
                            {{ $project->retainerType->name . " periods" }}
                            <i class="glyph-icon icon-chevron-right"></i>
                        </a>
                    </li>
                @endif
                @if(\Auth::user()->isAdmin())
                    <li class="col-md-3">
                        <a href="#tab-financial" data-toggle="tab" class="list-group-item">
                            <i class="glyph-icon icon-money"></i>
                            Financial reporting
                            <i class="glyph-icon icon-chevron-right"></i>
                        </a>
                    </li>
                @endif
                <li class="col-md-2 {{ session()->has('delete_file') ? 'active' : '' }}">
                    <a href="#tab-files" data-toggle="tab" class="list-group-item">
                        <i class="glyph-icon icon-file"></i>
                        Files
                        <i class="glyph-icon icon-chevron-right"></i>
                    </a>
                </li>
            </ul>
            <div class="tab-content">

                <div class="tab-pane pad0A fade {{ session()->has('delete_file') ? '' : 'active in' }}" id="tab-home">
                    <div class="row">
                        <div class="col-sm-4 col-md-4">
                            <div class="panel-group no-hover" id="panel-info">
                                <div class="panel">
                                    <div class="panel-heading">
                                        <a data-toggle="collapse" data-parent="#panel-info" href="#project-info" class="collapsed toggle">
                                            <h4 class="panel-title">
                                                <i class="glyph-icon icon-info-circle"></i>&nbsp;
                                                Project Information
                                                <span id="ftp-span" class="ui-accordion-header-icon ui-icon ui-icon-triangle-1-s"></span>
                                            </h4>
                                        </a>
                                    </div>
                                    <div id="project-info" class="panel-collapse collapse in">
                                        <div class="panel-body">
                                            <p><b>Type: </b>@foreach($project->types as $type) {{ $type->name.' ' }}@endforeach</p>
                                            <p><b>Company: </b><span id="company-name">{{ $project->company }}</span></p>
                                            @if($project->website_url !== '')<p><b>Website: </b><a href="{{ $project->website_url }}" target="_blank">{{ $project->website_url }}</a></p>@endif
                                            @if($project->cpanel_url !== '')<p class="flex"><b>CPanel: </b>&nbsp;&nbsp;&nbsp;<a href="{{ $project->cpanel_url }}" target="_blank"><img class="cpanel" src="/images/cpanel.jpg"> </a> </p>@endif
                                            @if($project->description !== '') <p><b>Description: </b>{{ $project->description }}</p>@endif
                                            @if($project->sitemap_url !== '') <p><b>Sitemap: </b><a href="{{ $project->sitemap_url }}" target="_blank">{{ $project->sitemap_url }}</a></p>@endif
                                            @if($project->invision_url !== '') <p><b>Inivision: </b><a href="{{ $project->invision_url }}" target="_blank">{{ $project->invision_url }}</a></p>@endif
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-sm-4 col-md-4">
                            <div class="panel-group no-hover" id="panel-people">
                                <div class="panel">
                                    <div class="panel-heading">
                                        <a data-toggle="collapse" data-parent="#panel-people" href="#project-people" class="collapsed toggle">
                                            <h4 class="panel-title">
                                                <i class="glyph-icon icon-elusive-group"></i>&nbsp;
                                                People
                                                <span id="ftp-span" class="ui-accordion-header-icon ui-icon ui-icon-triangle-1-s"></span>
                                            </h4>
                                        </a>
                                    </div>
                                    <div id="project-people" class="panel-collapse collapse in">
                                        <div class="panel-body">
                                            @if(isset($project->client))
                                            <div class="row flex">
                                                <div class="col-md-3"><p><b>Client: </b></p></div>
                                                <div class="col-md-9">
                                                    <a href="{{ url('/clients/'.$project->client->slug) }}" class="people-logo" data-toggle="tooltip" title="{{ $project->client->firstname }} {{ $project->client->lastname }}">
                                                        <img class="img-circle" width="40" height="40" src="{{ $project->client->picture }}" alt="{{ $project->client->firstname }} {{ $project->client->lastname }}">
                                                    </a>
                                                </div>
                                            </div>
                                            @endif
                                            @if(count($project->contractors()->get()) > 0)
                                                <div class="row flex">
                                                    <div class="col-md-3"><p><b>Contractors: </b></p></div>
                                                    <div class="col-md-9">
                                                        @foreach($project->contractors()->get() as $contractor)
                                                            <a href="{{ url('/contractors/'.$contractor->slug) }}" class="people-logo" data-toggle="tooltip" title="{{ $contractor->firstname }} {{ $contractor->lastname }}"><img class="img-circle" width="40" height="40" src="{{ $contractor->picture }}" alt="{{ $contractor->firstname }} {{ $contractor->lastname }}"></a>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            @endif
                                            @if(count($project->users()->get()) > 0)
                                                <div class="row flex">
                                                    <div class="col-md-3"><p><b>Baboons: </b></p></div>
                                                    <div class="col-md-9">
                                                        @foreach($project->users()->get() as $baboon)
                                                            <a href="{{ url('/baboons/'.$baboon->slug) }}" class="people-logo" data-toggle="tooltip" title="{{ $baboon->firstname }} {{ $baboon->lastname }}"><img class="img-circle" width="40" height="40" src="{{ $baboon->picture }}" alt="{{ $baboon->firstname }} {{ $baboon->lastname }}"></a>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-sm-4 col-md-4">
                            @if($project->cms_url !== '')
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="panel-group no-hover" id="panel-cms">
                                            <div class="panel">
                                                <div class="panel-heading">
                                                    <a data-toggle="collapse" data-parent="#panel-cms" href="#project-cms" class="collapsed toggle">
                                                        <h4 class="panel-title">
                                                            <i class="glyph-icon icon-elusive-wordpress"></i>&nbsp;
                                                            CMS Details
                                                            <span id="ftp-span" class="ui-accordion-header-icon ui-icon ui-icon-triangle-1-s"></span>
                                                        </h4>
                                                    </a>
                                                </div>
                                                <div id="project-cms" class="panel-collapse collapse in">
                                                    <div class="panel-body">
                                                        <p><b>URL: </b><a href="{{ $project->cms_url }}" target="_blank">{{ $project->cms_url }}</a></p>
                                                        @foreach($project->cmsUsers as $user)
                                                            <p><b>User: </b>{{ $user->user }} &nbsp;&nbsp;&nbsp;<b>Password: </b>{{ \Crypt::decrypt($user->password) }}</p>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif

                                @if($project->ftp_host !== '')
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="panel-group no-hover" id="panel-ftp">
                                                <div class="panel">
                                                    <div class="panel-heading">
                                                        <a data-toggle="collapse" data-parent="#panel-ftp" href="#project-ftp" class="collapsed toggle">
                                                            <h4 class="panel-title">
                                                                <i class="glyph-icon icon-upload"></i>&nbsp;
                                                                FTP Details
                                                                <span id="ftp-span" class="ui-accordion-header-icon ui-icon ui-icon-triangle-1-s"></span>
                                                            </h4>
                                                        </a>
                                                    </div>
                                                    <div id="project-ftp" class="panel-collapse collapse in">
                                                        <div class="panel-body">
                                                            <p class="text-transform-upr"><b>Type: </b>{{ $project->ftp_type }}</p>
                                                            <p><b>Host: </b>{{ $project->ftp_host }}</p>
                                                            <p><b>Port: </b>{{ $project->ftp_port }}</p>
                                                            @foreach($project->ftpUsers as $user)
                                                                <p><b>User: </b>{{ $user->user }} &nbsp;&nbsp;&nbsp;<b>Password: </b>{{ \Crypt::decrypt($user->password) }}</p>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif

                                @if($project->domain_url !== '')
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="panel-group no-hover" id="panel-domain">
                                                <div class="panel">
                                                    <div class="panel-heading">
                                                        <a data-toggle="collapse" data-parent="#panel-domain" href="#project-domain" class="collapsed toggle">
                                                            <h4 class="panel-title">
                                                                <i class="glyph-icon icon-elusive-globe"></i>&nbsp;
                                                                Panel login
                                                                <span id="ftp-span" class="ui-accordion-header-icon ui-icon ui-icon-triangle-1-s"></span>
                                                            </h4>
                                                        </a>
                                                    </div>
                                                    <div id="project-domain" class="panel-collapse collapse in">
                                                        <div class="panel-body">
                                                            <p><b>URL: </b>{{ $project->domain_url }}</p>
                                                            @if($project->domain_user !== '')<p><b>User: </b>{{ $project->domain_user }}</p>@endif
                                                            @if($project->domain_password !== '')<p><b>Password: </b>{{ \Crypt::decrypt($project->domain_password) }}</p>@endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                        </div>

                    </div>
                </div>

                {{--@if($project->status != 'closed')--}}
                    {{--<div class="tab-pane fade" id="tab-trello">--}}
                        {{--<div class="row">--}}
                            {{--<div class="col-md-12">--}}
                                {{--<div class="content-box mrg15B">--}}
                                    {{--<h3 class="content-box-header text-left" style="border-bottom-color: rgba(158, 173, 195, 0.16);">--}}
                                        {{--<i class="glyph-icon icon-trello"></i>--}}
                                        {{--@if($project->status == 'ongoing')--}}
                                            {{--{{ $project->name }}--}}
                                            {{--<button id="refresh-button" class="btn btn-azure float-right mrg20L" onclick="displayProject('{{ $project->trello_board_id  }}')"><i class="remove-border glyph-icon demo-icon icon-spin-3 font-white font-size-16"></i>&nbsp;Refresh<div class="ripple-wrapper"></div></button>--}}
                                            {{--<a href="https://trello.com/b/{{ $project->trello_board_id }}" target="_blank" class="btn btn-info float-right mrg20L"><i class="glyph-icon icon-trello"></i>&nbsp;&nbsp;Go to Trello</a>--}}

                                        {{--@else--}}
                                            {{--{{ $project->name.' - '.$project->retainerType->name }}--}}
                                            {{--<button id="refresh-button" class="btn btn-azure float-right mrg20L" onclick="displayProject('{{ $project->trello_support_board_id  }}')"><i class="remove-border glyph-icon demo-icon icon-spin-3 font-white font-size-16"></i>&nbsp;Refresh<div class="ripple-wrapper"></div></button>--}}
                                            {{--<a href="https://trello.com/b/{{ $project->trello_support_board_id }}" target="_blank" class="btn btn-info float-right mrg20L"><i class="glyph-icon icon-trello"></i>&nbsp;&nbsp;Go to Trello</a>--}}
                                        {{--@endif--}}
                                    {{--</h3>--}}
                                    {{--<div class="content-box-wrapper">--}}
                                        {{--<div id="form-wizard-3" class="form-wizard">--}}
                                            {{--<ul></ul>--}}
                                            {{--<div class="tab-content">--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--@endif--}}

                @if($project->is_supported)
                    <div class="tab-pane fade" id="tab-retainer">
                        <div class="row">
                            <div class="col-md-12">
                                @if(\Auth::user()->isProjectManager() && !$project->users()->where('id', \Auth::user()->id)->get()->isEmpty())
                                    <button id="trigger-delete-modal" data-toggle="modal" data-target="#modal-delete" class="btn btn-danger float-right mrg15B"><i class="glyph-icon icon-elusive-cancel-circled"></i>&nbsp;Delete retainer<div class="ripple-wrapper"></div></button>
                                    <div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    <h4 class="modal-title">Delete retainer project</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <p>Are you sure that you want to delete the retainer project?</p>
                                                    <p>The project will be closed and the retainer board and retainer toggle project will be deleted.</p>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                    <a href="/retainers/{{ $project->slug }}/delete" class="btn btn-danger">Delete</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                <a href='https://toggl.com/app/reports/detailed/{{ config('toggle.workspace_id') }}/period/thisYear/projects/{{ $project->toggle_support_project_id }}/billable/both' class='btn btn-success float-right mrg15R' target='_blank'><i class='glyph-icon icon-bar-chart'></i>&nbsp;Full report</a>
                                <table id="datatable-reorder" class="table table-striped table-bordered dataTable table-center" cellspacing="0" width="100%" role="grid" aria-describedby="datatable-reorder_info" style="width: 100%;">
                                    <thead>
                                    <tr role="row">
                                        <th>Status</th>
                                        <th class="width-240">Period</th>
                                        <th>Monthly time</th>
                                        <th>Time due</th>
                                        <th>Time spent</th>
                                        <th>Time left</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    {{-- Active period --}}
                                    @if($activePeriod)
                                        <tr role="row" class="active-period">
                                            <td>{{ $activePeriod->status }}</td>
                                            <td>From {{ date('F d, Y', strtotime($activePeriod->from)) }} to {{ date('F d, Y', strtotime($activePeriod->to)) }}</td>
                                            <td>{{ $activePeriod->monthly_time }}H</td>
                                            <td>{{ $activePeriod->time_due }}H</td>
                                            <td>{{ $activePeriod->time_spent }}H</td>
                                            <td>{{ $activePeriod->time_left }}H</td>
                                            <td><a href='https://toggl.com/app/reports/detailed/{{ config('toggle.workspace_id') }}/from/{{ $activePeriod->from }}/to/{{ $activePeriod->to }}/projects/{{ $project->toggle_support_project_id }}/billable/both' class='btn btn-success'  target='_blank'><i class='glyph-icon icon-bar-chart'></i>&nbsp;&nbsp;Period report</a></td>
                                        </tr>
                                    @endif

                                    <?php $i = 0; ?>
                                    @foreach($project->retainerPeriods as $period)
                                        @if($period->status != 'active')
                                            <tr role="row" class="{{ $i%2 == 0 ? 'odd' : 'even' }}">
                                                <td>{{ $period->status }}</td>
                                                <td>From {{ date('F d, Y', strtotime($period->from)) }} to {{ date('F d, Y', strtotime($period->to)) }}</td>
                                                <td>{{ $period->monthly_time }}H</td>
                                                <td>{{ $period->time_due }}H</td>
                                                <td>{{ $period->time_spent }}H</td>
                                                <td>{{ $period->time_left }}H</td>
                                                <td><a href='https://toggl.com/app/reports/detailed/{{ config('toggle.workspace_id') }}/from/{{ $period->from }}/to/{{ $period->to }}/projects/{{ $project->toggle_support_project_id }}/billable/both' class='btn btn-success'  target='_blank'><i class='glyph-icon icon-bar-chart'></i>&nbsp;&nbsp;Period report</a></td>
                                            </tr>
                                        @endif
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                @endif

                @if(\Auth::user()->isAdmin())
                    <script src="/js/financial-report.js"></script>
                    <input type="hidden" id="project-id" value="{{ $project->id }}">
                    <div class="tab-pane fade" id="tab-financial">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="content-box mrg15B">
                                    <h3 class="content-box-header text-left" style="border-bottom-color: rgba(158, 173, 195, 0.16);">
                                        <i class="glyph-icon icon-money"></i>
                                        Financial reporting
                                    </h3>
                                    <div class="content-box-wrapper">

                                        <table class="table table-condensed">
                                            <thead>
                                            <tr>
                                                <th width="400">Baboons</th>
                                                <th width="280">Rate (£/h)</th>
                                                <th width="100">Time spent</th>
                                                <th width="100">Cost</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($project->users as $user)
                                                <tr class="row-user">
                                                    <td>{{ $user->firstname . " ". $user->lastname }}</td>
                                                    <td>
                                                        <div class="row flex">
                                                            <div class="col-md-8">
                                                                <div class="slider"></div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <input type="number" name="rate" class="rate form-control" value="{{ $user->getRateByProject($project->id)[0]->rate }}" data-user-id="{{ $user->id }}">
                                                            </div>
                                                        </div>
                                                    </td>
                                                    @if($project->is_supported)
                                                        <td><span class="time">{{ $user->getTimeSpentPerProject($project->toggle_support_project_id) }}</span> h</td>
                                                    @else
                                                        <td><span class="time">{{ $user->getTimeSpentPerProject($project->toggle_project_id) }}</span> h</td>
                                                    @endif
                                                    <td class="text-center">£<span class="cost"></span></td>
                                                </tr>
                                            @endforeach
                                            <tr>
                                                <td colspan="4" height="35"></td>
                                            </tr>
                                            </tbody>

                                            <thead>
                                            <tr>
                                                <td class="no-border">&nbsp;</td>
                                                <td class="no-border">&nbsp;</td>
                                                <th class="border-bot-1">Total cost</th>
                                                <td class="text-center">£<span class="total-cost"></span></td>
                                            </tr>
                                            @if( !$project->is_supported)
                                                <tr>
                                                    <td class="no-border">&nbsp;</td>
                                                    <td class="no-border">&nbsp;</td>
                                                    <th class="border-bot-1">Budget</th>
                                                    <td class="text-center"><input type="hidden" id="budget" value="{{ $project->budget }}">£<span class="budget">{{ number_format($project->budget,0,'.', ' ') }}</span></td>
                                                </tr>
                                                <tr>
                                                    <td class="no-border">&nbsp;</td>
                                                    <td class="no-border">&nbsp;</td>
                                                    <th class="border-bot-1">Profit</th>
                                                    <td class="border-bot-1 td-profit text-center">£<span class="profit"></span></td>
                                                </tr>
                                            @endif
                                            </thead>

                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif

                <div class="tab-pane pad0A fade {{ session()->has('delete_file') ? 'active in' : '' }}" id="tab-files">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="content-box mrg15B">
                                <h3 class="content-box-header text-left" style="border-bottom-color: rgba(158, 173, 195, 0.16);">
                                    <i class="glyph-icon icon-file"></i>
                                    Files
                                </h3>
                                <div class="content-box-wrapper">
                                    <div class="row" id="dropzone-example">
                                        <div class="col-md-12">
                                            <form action="/projects/{{ $project->slug }}/file-upload" class="dropzone bg-gray col-md-12 center-margin flex" id="demo-upload" enctype="multipart/form-data">
                                                {!! csrf_field() !!}
                                            </form>
                                        </div>
                                    </div>
                                    <div class="row mrg20T">
                                        @foreach($project->files as $file)
                                            <div class="col-md-2">
                                                <div class="thumbnail-box-wrapper">
                                                    <div class="thumbnail-box text-center img-prev">
                                                        <div class="thumb-content">
                                                            <div class="center-vertical">
                                                                <div class="center-content">
                                                                    <div class="thumb-btn animated zoomIn">
                                                                        <a href="{{ url('/projects/'.$project->slug.'/file/'.$file->id) }}" class="btn btn-lg btn-round btn-info file-download" title="Downdload"><i class="glyph-icon icon-elusive-download-alt"></i></a>
                                                                        <button data-toggle="modal" data-target="#modal{{ $file->id }}" class="btn btn-lg btn-round btn-danger" title="Remove"><i class="glyph-icon icon-remove"></i></button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="thumb-overlay bg-gray"></div>
                                                        <img class="imgPreview" src="" alt="{{ $file->name }}">
                                                    </div>
                                                    <div class="thumb-pane">
                                                        <h3 class="thumb-heading">
                                                            <a href="{{ url('/projects/'.$project->slug.'/file/'.$file->id) }}" title="">
                                                                <small>{{ $file->name }}</small>
                                                            </a>

                                                        </h3>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal fade" id="modal{{ $file->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                            <h4 class="modal-title">Delete file</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <p>Are you sure that you want to delete the file {{ $file->name }}?</p>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                            <a href="{{ url('/projects/'.$project->slug.'/file/'.$file->id.'/delete') }}" class="btn btn-danger">Delete</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
