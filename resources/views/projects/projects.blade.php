@extends('template')
@section('body')

    @include('breadcrumbs', array('breadcrumb_title' => 'Projects', 'breadcrumb_list' => [['Dashboard','/'], ['Projects','/projects']]) )

    @if(session()->has('status'))
        <div class="alert alert-success flex mrg25B">
            <div class="bg-green alert-icon">
                <i class="glyph-icon icon-check"></i>
            </div>
            <div class="alert-content">
                <h4 class="alert-title">{{ session('status') }}</h4>
            </div>
        </div>
    @endif

    @if(session()->has('warning'))
        <div class="alert alert-warning flex mrg25B">
            <div class="bg-orange alert-icon">
                <i class="glyph-icon icon-warning"></i>
            </div>
            <div class="alert-content">
                <h4 class="alert-title">{{ session('warning') }}</h4>
            </div>
        </div>
    @endif

    @if(session()->has('project'))
        <script>
            var boardId;
            @if(session()->get('project')->status == 'ongoing')
                boardId = "{{ session()->get('project')->trello_board_id }}";
            @elseif(session()->get('project')->status == 'retained')
                boardId = "{{ session()->get('project')->trello_support_board_id }}";
            @endif

            $(document).ready(function () {

                @foreach(session()->get('project')->users as $baboon)
                    @if($baboon->isProjectManager())
                        type = 'admin';
                    @else
                        type = 'normal';
                    @endif

                    addMemberToOrganization("{{ session()->get('project')->trello_organization_id }}", "{{ $baboon->trello_user_id }}", type);
                    addMemberToBoard(boardId, "{{ $baboon->trello_user_id }}", type);
                @endforeach

            });
        </script>
    @endif

    <div class="panel">
        <div class="panel-body">
            <h3 class="title-hero">
                Projects
            </h3>
            <div class="example-box-wrapper">
                <div id="datatable-reorder_wrapper" class="dataTables_wrapper form-inline">
                    <div id="datatable-reorder_filter" class="dataTables_filter" style="display:block;float: left">
                        <label>
                            <input type="search" class="form-control" placeholder="Search..." aria-controls="datatable-reorder">
                        </label>
                    </div>
                    <table id="datatable-reorder" class="table table-striped table-bordered dataTable" cellspacing="0" width="100%" role="grid" aria-describedby="datatable-reorder_info" style="width: 100%;">
                        <thead>
                            <tr role="row">
                                <th>Name</th>
                                <th>Type</th>
                                <th>Company</th>
                                <th>Website</th>
                                <th class="width-20">CPanel</th>
                                <th>Status&nbsp;
                                    <a href="#" class="font-size-14" data-toggle="tooltip" data-placement="top" title=
                                    "The ongoing status means that the project is still in development. Only the main trello board and the main toggle project is active.
                                    The retained status means that the project is either retained or supported. Only the retainer trello board and the retainer toggle project is active.
                                    The closed status means the project is over and we are not working on it anymore. All trello boards and toggle projects are closed.
                                    ">
                                        <i class="glyph-icon icon-info-circle"></i>
                                    </a>
                                </th>
                                <th>Action</th>
                            </tr>
                        </thead>

                        <tbody>
                            <?php $i = 0; ?>
                            @foreach($projects as $project)
                                <tr role="row" class="{{ $i%2 == 0 ? 'odd' : 'even' }}">
                                    <td>{{ $project->name }}</td>
                                    <td>@foreach($project->types as $type ){{ $type === $project->types->last() ? $type->name : $type->name . ', '}}@endforeach</td>
                                    <td>{{ $project->company }}</td>
                                    <td><a href="{{ $project->website_url }}" target="_blank">{{ $project->website_url }}</a></td>
                                    <td class="text-center">@if($project->cpanel_url != '')<a href="{{ $project->cpanel_url }}" target="_blank"><img class="cpanel" src="/images/cpanel.jpg"> </a>@endif </td>
                                    <td class="text-center">
                                        @if(\Auth::user()->isProjectManager() && !$project->users()->where('id', \Auth::user()->id)->get()->isEmpty())
                                            @if($project->status == 'ongoing')
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-azure dropdown-toggle" data-toggle="dropdown" aria-expanded="false">{{ $project->status }} <span class="caret"></span><div class="ripple-wrapper"></div></button>
                                                    <ul class="dropdown-menu" role="menu">
                                                        <li><a href="/projects/{{ $project->slug }}/retain">Retain</a></li>
                                                        <li><a href="/projects/{{ $project->slug }}/close">Close</a></li>
                                                    </ul>
                                                </div>
                                            @elseif($project->status == 'retained')
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-yellow dropdown-toggle" data-toggle="dropdown" aria-expanded="false">{{ $project->status }} <span class="caret"></span><div class="ripple-wrapper"></div></button>
                                                    <ul class="dropdown-menu" role="menu">
                                                        <li><a href="/projects/{{ $project->slug }}/open">Open</a></li>
                                                        <li><a href="/projects/{{ $project->slug }}/close">Close</a></li>
                                                    </ul>
                                                </div>
                                            @else
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-black dropdown-toggle" data-toggle="dropdown" aria-expanded="false">{{ $project->status }} <span class="caret"></span><div class="ripple-wrapper"></div></button>
                                                    <ul class="dropdown-menu" role="menu">
                                                        <li><a href="/projects/{{ $project->slug }}/open">Open</a></li>
                                                        <li><a href="/projects/{{ $project->slug }}/retain">Retain</a></li>
                                                    </ul>
                                                </div>
                                            @endif
                                        @else
                                            @if($project->status == 'ongoing')
                                                <a class="btn btn-azure disabled" href="#" role="button">{{ $project->status }}</a>
                                            @elseif($project->status == 'retained')
                                                <a class="btn btn-yellow disabled" href="#" role="button">{{ $project->status }}</a>
                                            @else
                                                <a class="btn btn-black disabled" href="#" role="button">{{ $project->status }}</a>
                                            @endif

                                        @endif

                                    </td>
                                    <td class="text-center">
                                        <a href="/projects/{{ $project->slug }}" class="btn btn-info"><i class="glyph-icon icon-eye"></i>&nbsp;View<div class="ripple-wrapper"></div></a>
                                        @if(\Auth::user()->isProjectManager() && !$project->users()->where('id', \Auth::user()->id)->get()->isEmpty())
                                            <a href="/projects/{{ $project->slug }}/edit" class="btn btn-success"><i class="glyph-icon icon-edit"></i>&nbsp;Edit<div class="ripple-wrapper"></div></a>
                                            <button data-toggle="modal" data-target="#modal{{ $project->id }}" class="btn btn-danger"><i class="glyph-icon icon-elusive-cancel-circled"></i>&nbsp;Delete<div class="ripple-wrapper"></div></button>
                                            <div class="modal fade" id="modal{{ $project->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                            <h4 class="modal-title">Delete project</h4>
                                                        </div>
                                                        <div class="modal-body text-left">
                                                            <p>Are you sure that you want to delete the project {{ $project->name }}?</p>
                                                            <p>It will delete all associated files and retainer periods as well.</p>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                            <a href="/projects/{{ $project->slug }}/delete" class="btn btn-danger">Delete</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop
